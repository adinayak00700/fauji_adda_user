package com.user.foujiadda.callbacks;

public interface NetworkStateReceiverListener {

    void onInternetConnectionEstablished();

    void onInternetConnectionNotFound();


}
