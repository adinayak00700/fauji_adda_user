package com.user.foujiadda.callbacks;

public interface PaginationCallback{
    public void loadNextPage();
}