package com.user.foujiadda.callbacks;

public interface BaseCallBacks {



    void showLoader();

    void dismissLoader();

    void onFragmentDetach(String fragmentTag);

}
