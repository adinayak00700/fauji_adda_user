package com.user.foujiadda.activity

import NotificationAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.adapter.RateAndReviewAdapter
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityNotificationBinding
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.dialog.ProgressDialog
import com.user.foujiadda.factory.NotificationFactory
import com.user.foujiadda.factory.RateAndReviewFactory
import com.user.foujiadda.models.ModelNotification
import com.user.foujiadda.models.ModelRateAndReview
import com.user.foujiadda.models.NotificationData
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.models.viewmodel.NotificationViewModel
import com.user.foujiadda.models.viewmodel.ViewModelRateAndReview
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : BaseActivity() , NotificationAdapter.NotifictionDeleteBack {

    private lateinit var binding : ActivityNotificationBinding
    private lateinit var viewModel: NotificationViewModel
    private lateinit var NotificationAdapter: NotificationAdapter
    private lateinit var modelNotification: ModelNotification
    private var id : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification)

        binding.toolbar.tvhead.text = "Notification"


        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
            finish()
        }

        binding.toolbar.clearAll.setOnClickListener {
            getNotificationDelete()
            id = ""
        }


        if ( PrefManager.getInstance(this@NotificationActivity)!!.userDetail.token=="skip"){


            val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(this@NotificationActivity))
            val dialog = MaterialAlertDialogBuilder(this@NotificationActivity, R.style.MyThemeOverlayAlertDialog).create()
            dialog.setCancelable(false)
            dialog.setView(dialogBinding.root)

            dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                val intent = Intent(this@NotificationActivity, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this@NotificationActivity.startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
            }

            dialogBinding.signup.setOnClickListener {
                val intent = Intent(this@NotificationActivity, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this@NotificationActivity.startActivity(intent)
            }
            dialog.show()

        } else{

            setupadapter()
            getNotification()

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    override fun onClick(viewId: Int, view: View?) {
        TODO("Not yet implemented")
    }


    private fun setupadapter() {

        val factory = NotificationFactory()
        binding.rvnotification.layoutManager = LinearLayoutManager(applicationContext)
        viewModel = ViewModelProviders.of(this, factory).get(NotificationViewModel::class.java)
        NotificationAdapter = NotificationAdapter(viewModel, applicationContext ,this )
        binding.rvnotification.adapter = NotificationAdapter
        viewModel.livedatalist.observe(this , Observer {
            NotificationAdapter.submitList(it)
        })
    }


    fun getNotification() {
        val hashMap = HashMap<String, String>()
        NotificationAdapter.notifyDataSetChanged()

        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)

        RestClient.getInst().getNotification(hashMap).enqueue(object :
            Callback<ModelNotification> {

            override fun onResponse(
                call: Call<ModelNotification>,
                response: Response<ModelNotification>
            ) {
                if (response.body()!!.result) {


                        viewModel.add(response.body()!!.data as ArrayList<NotificationData>)
                        if (response.body()!!.data.isEmpty()){
                            binding.noDataFound.visibility = View.VISIBLE
                            binding.toolbar.clearAll.visibility = View.GONE
                        }

                }
            }

            override fun onFailure(call: Call<ModelNotification>, t: Throwable) {

                makeLongToast(t.message)
            }
        })
    }

    fun getNotificationDelete() {
        val hashMap = HashMap<String, String>()
        NotificationAdapter.notifyDataSetChanged()

        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)
        hashMap.put("id", id)

        RestClient.getInst().getNotificationDelete(hashMap).enqueue(object :
            Callback<ModelNotification> {

            override fun onResponse(
                call: Call<ModelNotification>,
                response: Response<ModelNotification>
            ) {
                if (response.body()!!.result) {

                    getNotification()

                }
            }

            override fun onFailure(call: Call<ModelNotification>, t: Throwable) {

                makeLongToast(t.message)
            }
        })
    }

    override fun onNotificationDeleteClick(notification: NotificationData) {
        id = notification.id
        NotificationAdapter.notifyItemRemoved(id.toInt())
        getNotificationDelete()
    }

    override fun onClickNotification(notification: NotificationData) {
        if (notification.service=="OFFERS"){
            startActivity(IntentHelper.getOfferScreen(this))
        }else if (notification.service=="CSD"){
            startActivity(IntentHelper.getCSDScreen(this))
        }else if (notification.service=="NEWS"){
            startActivity(IntentHelper.getNewsActivity(this))
        }else if (notification.service == "ECHS"){
            startActivity(IntentHelper.getECHSScreen(this))
        }else if (notification.service == "TRAVEL"){
            startActivity(IntentHelper.getTravelScreen(this))
        }else if (notification.service == "JOBS"){
            startActivity(IntentHelper.getJobScreen(this))
        }else{
            startActivity(IntentHelper.getDashboardActivity(this))
        }
    }
}