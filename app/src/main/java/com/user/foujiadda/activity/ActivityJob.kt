package com.user.foujiadda.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.mechanicforyou.user.utilities.FilterDialog
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityJobBinding
import com.user.foujiadda.fragment.FragmentJobDescription
import com.user.foujiadda.fragment.FragmentJobFilter
import com.user.foujiadda.fragment.FragmentJobList
import com.user.foujiadda.models.JobData


class ActivityJob : AppCompatActivity(), FragmentJobList.Callbackk, FilterDialog.Callbackss {
    private lateinit var binding: ActivityJobBinding

    var state : String = ""
    var city : String = ""
    var educations : String = ""
    var salarys : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job)
        binding.toolbar.topAppBar.inflateMenu(R.menu.filter_menu)
        loadFragment(FragmentJobList(getString(R.string.govt),this, state, city, educations, salarys))
        binding.toolbar.tvhead.text = getString(R.string.jobs)
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }

        binding.toolbar.topAppBar.menu!!.getItem(0).setOnMenuItemClickListener {
            /*loadFragment(FragmentJobFilter())*/
            var dialogfilter=FilterDialog(context = this,this)
            dialogfilter.show()
           // var dialogFilter=DialogFil

            true
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position
                if (position == 0) {
                        loadFragment(FragmentJobList(getString(R.string.govt),this@ActivityJob, state, city, educations, salarys  ))
                } else if (position == 1) {
                    loadFragment(FragmentJobList(getString(R.string.pvt),this@ActivityJob, state, city, educations, salarys))
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
    }

    var backStateName = ""

    private fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val manager: FragmentManager = getSupportFragmentManager()
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) { //fragment not in back stack, create it.
            val ft = manager.beginTransaction()
         //   ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right)
            ft.replace(binding.frame.id, fragment, backStateName)
          //  ft.addToBackStack(backStateName);
            ft.commit()
        }
    }

    override fun onClickOnJob(job: JobData) {
        loadFragment(FragmentJobDescription(job))
    }

    override fun onCLickONApplyButton(
        selecteState: String,
        selecteCity: String,
        education: String,
        salary: String
    ) {
        state = selecteState
        city = selecteCity
        educations = education
        salarys = salary

      var fragment=  supportFragmentManager.findFragmentById(R.id.frame)
        if (fragment is FragmentJobList){
           fragment.onCLickONApplyButton(selecteState, selecteCity, education, salary)
        }
    }
}