package com.user.foujiadda.activity

import SliderAdapterExample
import VendorReviewsAdapter
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.dialog.DialogRatenReview
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityVendorDescriptionBinding
import com.user.foujiadda.factory.VendorReviewModelFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.viewmodel.VendorReviewViewModel
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ActivityVendorDescription : AppCompatActivity(), VendorReviewsAdapter.Callbackk {
    var type = ""
    var appointmentDate = ""
    lateinit var vendor: Vendor
    var vendorId = ""
    private lateinit var binding: ActivityVendorDescriptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_vendor_description)
        initFeedAdapter()
        vendorId = intent.getStringExtra("id").toString()

        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
        }



        if (intent.getStringExtra("type") != null) {
            type = intent.getStringExtra("type").toString()

            if (type =="guest_house"){
                binding.toolbar.tvhead.text = "Guest House"
            }

            if (type== "csd_dealers")
            {
                binding.toolbar.tvhead.text = "CSD Dealer"
            }


            if (type== "csd_store")
            {
                binding.toolbar.tvhead.text = "CSD Stores"
            }

            if (type == "echs_empanelled"){
                binding.toolbar.tvhead.text = "ECHS Empanelled Hospitals"
            }

            if (type == "echs_polyclinic"){
                binding.toolbar.tvhead.text = "ECHS Polyclinic"
            }




        }

        if (intent.getStringExtra("date") != null) {
            appointmentDate = intent.getStringExtra("date").toString()
        }


        getVendorDetails()
    }


    fun getVendorDetails() {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)
        hashMap.put("vendor_id", vendorId)
        hashMap.put("type", type)

        RestClient.getInst().getVendorDetails(hashMap)
            .enqueue(object : Callback<ModelVendorDetails> {
                override fun onResponse(
                    call: Call<ModelVendorDetails>,
                    response: Response<ModelVendorDetails>
                ) {
                    if (response.body()!!.result) {
                        vendor = response.body()!!.data


                        getReviews()
                        binding.tvVendorName.text = response.body()!!.data.name
                        binding.tvAddress.text = response.body()!!.data.address
                        /*binding.tvPhoneNumber.text = response.body()!!.data.mobile*/
                        binding.ratingBar.rating = vendor.avg_rating.toFloat()
                        binding.tvcommentCount.text = vendor.total_rating.toString()
                        binding.facillitText.text = vendor.facility

                        if (type == "guest_house") {
                            binding.layoutGuestDetails.visibility = View.VISIBLE
                            binding.tvOfficerValue.text = vendor.rooms.officers
                            binding.tvDormitoryValue.text = vendor.rooms.dormitory
                            binding.tvJcosValue.text = vendor.rooms.jcos
                            binding.tvOrsvalue.text = vendor.rooms.ors
                        }
                        if (type == "echs_empanelled") {
                            binding.layoutFacility.visibility = View.VISIBLE

                        }

                        binding.tvPhoneNumber.setOnClickListener {
                            // Use format with "tel:" and phoneNumber created is
                            // stored in u.
                            // Use format with "tel:" and phoneNumber created is
                            // stored in u.
                            val u: Uri = Uri.parse("tel:" + response.body()!!.data.mobile)

                            // Create the intent and set the data for the
                            // intent as the phone number.

                            // Create the intent and set the data for the
                            // intent as the phone number.
                            val i = Intent(Intent.ACTION_DIAL, u)

                            try {
                                // Launch the Phone app's dialer with a phone
                                // number to dial a call.
                                startActivity(i)
                            } catch (s: SecurityException) {
                                // show() method display the toast with
                                // exception message.
                                Toast.makeText(
                                    applicationContext,
                                    "An error occurred",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                        val urls: MutableList<String> = ArrayList()


                        var slider = SliderAdapterExample(this@ActivityVendorDescription)


                        urls.add(response.body()!!.data.image)

                        slider.addItem(urls as ArrayList<String>)
                        binding.imageSlider.setSliderAdapter(slider);
                        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        binding.imageSlider.startAutoCycle();


                        if (vendor.vendorType == "echs_empanelled") {
                            addLead("")
                        }

                        if (vendor.vendorType == "csd_dealers") {
                            addLead("")
                        }

                        if (vendor.vendorType == "guest_house") {
                            addLead(vendor.state_id)
                        }

                        binding.tvRating.setOnClickListener {
                            var dialog = DialogRatenReview(
                                this@ActivityVendorDescription,
                                vendor,
                                object : DialogRatenReview.Callbackk {
                                    override fun onClickOnAddRateReviewSuccessful() {
                                        getReviews()
                                        getVendorDetails()
                                    }
                                },
                                userRating.rating,
                                userRating.review,false
                            )
                            dialog.show()
                        }

                        binding.buttonDirection.setOnClickListener {


/*
                            if (vendor.lat.isNotEmpty() || vendor.lng.isNotEmpty() || vendor.google_map.isNotEmpty() ) {

//                                val location: String = vendor.lat.toString() + "," + vendor.lng
//                                val gmmIntentUri: Uri = Uri.parse("google.navigation:q=$location")
                                val gmmIntentUri: Uri = Uri.parse("google.navigation:q=${vendor.google_map}")
                                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                                mapIntent.setPackage("com.google.android.apps.maps")
                                startActivity(mapIntent)

                            }else {*/

                                val location: String = vendor.lat.toString() + "," + vendor.lng
                                val gmmIntentUri: Uri = Uri.parse("google.navigation:q=$location")
                                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                                    mapIntent.setPackage("com.google.android.apps.maps")
                                    startActivity(mapIntent)



                        }


                    } else {
                        // makeToast(response.body()!!.message)
                    }
                }


                override fun onFailure(call: Call<ModelVendorDetails>, t: Throwable) {

                }
            })
    }

    fun addLead(stateId : String) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)
        hashMap.put("type", vendor.vendorType)
        hashMap.put("type_id", vendor.id)
        hashMap.put("city_id", vendor.city_id)
        hashMap.put("appointment_date", appointmentDate)
        hashMap.put("state_id", stateId)


        RestClient.getInst().addLead(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                } else {
                    // makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }


    private lateinit var viewModel: VendorReviewViewModel
    private lateinit var feedAdapter: VendorReviewsAdapter
    fun initFeedAdapter() {
        binding.rvReviews.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val factory = VendorReviewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(VendorReviewViewModel::class.java)
        feedAdapter = VendorReviewsAdapter(viewModel, this, this)
        binding.rvReviews.adapter = feedAdapter
        viewModel.livedatalist.observe(this, Observer {
            feedAdapter.submitList(it)
        })
    }

    private lateinit var userRating: UserRating
    fun getReviews() {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)
        hashMap.put("type", vendor.vendorType)
        hashMap.put("type_id", vendor.id)

        RestClient.getInst().getReviews(hashMap).enqueue(object : Callback<ModelVendorReviews> {
            override fun onResponse(
                call: Call<ModelVendorReviews>,
                response: Response<ModelVendorReviews>
            ) {
                if (response.body()!!.result) {

                    userRating = response.body()!!.userRating
                    viewModel.add(response.body()!!.data as ArrayList<VendorReview>)

                    if (response.body()!!.data.isEmpty()){
                        binding.layoutNoData.visibility=View.VISIBLE
                    }else{
                        binding.layoutNoData.visibility=View.GONE
                    }


                } else {


                    // makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelVendorReviews>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }

    override fun onClickONCommentLayout(postId: Int) {

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}