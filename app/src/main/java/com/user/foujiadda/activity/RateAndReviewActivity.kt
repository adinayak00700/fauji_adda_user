package com.user.foujiadda.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.dialog.DialogRatenReview
import com.user.foujiadda.R
import com.user.foujiadda.adapter.RateAndReviewAdapter
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityRateAndReviewBinding
import com.user.foujiadda.factory.RateAndReviewFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.viewmodel.ViewModelRateAndReview
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateAndReviewActivity : BaseActivity() , RateAndReviewAdapter.RateCallBAck {

    private lateinit var EditAndReviewAdapter: RateAndReviewAdapter
    private lateinit var binding: ActivityRateAndReviewBinding
    private lateinit var viewModel: ViewModelRateAndReview
    lateinit var vendor: Vendor
    var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rate_and_review)

        binding.toolbar.tvhead.text = "Rate And Review"

        setupadapter()

        getVendors()

        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
            finish()
        }

    }

    private fun setupadapter() {

        val factory = RateAndReviewFactory()
        binding.rvRateAndReview.layoutManager = LinearLayoutManager(applicationContext)
        viewModel = ViewModelProviders.of(this, factory).get(ViewModelRateAndReview::class.java)
        EditAndReviewAdapter = RateAndReviewAdapter(viewModel, applicationContext,this )
        binding.rvRateAndReview.adapter = EditAndReviewAdapter
        viewModel.livedatalist.observe(this , Observer {
            EditAndReviewAdapter.submitList(it)
        })


    }


    override fun onClick(viewId: Int, view: View?) {

    }

   /* override fun onClickOnVendor(vendor: Vendor) {
        TODO("Not yet implemented")
    }*/
   private lateinit var userRating: UserRating

    override fun onCickEditReview(vendorr: Vendor, edit : Boolean) {
        vendor=vendorr
        userRating=UserRating("","","",vendor.rating,vendor.review,"","","","","")
        if (edit) {
            var dialog = DialogRatenReview(
                this,
                vendor,
                object : DialogRatenReview.Callbackk {
                    override fun onClickOnAddRateReviewSuccessful() {

                        getVendors()

                    }
                },
                userRating.rating,
                userRating.review,true
            )
            dialog.show()
        }

    }


    fun getVendors() {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(this)!!.userDetail.token)

        RestClient.getInst().getRateAndReview(hashMap).enqueue(object : Callback<ModelRateAndReview> {

            override fun onResponse(
                call: Call<ModelRateAndReview>,
                response: Response<ModelRateAndReview>
            ) {
                if (response.body()!!.result) {

                    if (response.body()!!.data.isEmpty()){
                        binding.noDataFound.visibility = View.VISIBLE
                    }
                    viewModel.add(response.body()!!.data as ArrayList<Vendor>)



                } else {
                    // makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelRateAndReview>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

}