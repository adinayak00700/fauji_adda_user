package com.user.foujiadda.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityOtherUserProfileBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelOtheruserProfile
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.HashMap

class ActivityOtherUserProfile : AppCompatActivity() {
    lateinit var binding: ActivityOtherUserProfileBinding

    private var userId : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_other_user_profile)
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
        userId = intent.getStringExtra("userId").toString()
        getProfile()

        binding.toolbar.tvhead.text = "User Profile"


        if (userId == "Yes"){
            binding.tvButtonFollow.visibility = View.GONE
            binding.tvAbout.visibility = View.VISIBLE
            binding.tvEmail.visibility = View.VISIBLE
            binding.tvNumber.visibility = View.VISIBLE
            binding.tvDob.visibility = View.VISIBLE
            binding.tvCity.visibility = View.VISIBLE
            binding.tvState.visibility = View.VISIBLE
        }

    }


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["postUserID"] = intent.getStringExtra("id").toString()

        RestClient.getInst().getOtheruserProfile(hashMap)
            .enqueue(object : Callback<ModelOtheruserProfile> {
                override fun onResponse(
                    call: Call<ModelOtheruserProfile>,
                    response: Response<ModelOtheruserProfile>
                ) {

                    try {
                        if (response.body()!!.result) {


                            if (response.body()!!.data.email.isEmpty()){
                                binding.tvEmail.visibility = View.GONE
                            }

                            if (response.body()!!.data.mobile.isEmpty()){
                                binding.tvNumber.visibility = View.GONE
                            }

                            if (response.body()!!.data.dob.isEmpty()){
                                binding.tvDob.visibility = View.GONE
                            }

                            if (response.body()!!.data.city.isEmpty()){
                                binding.tvCity.visibility = View.GONE

                            }

                            if ( response.body()!!.data.state.isEmpty()){
                                binding.tvState.visibility = View.GONE
                            }

                            if (response.body()!!.data.city.isEmpty() && response.body()!!.data.state.isEmpty()){
                                binding.layouytCity.visibility = View.GONE
                            }

                            if (response.body()!!.data.mobile.isEmpty() && response.body()!!.data.dob.isEmpty()){
                                binding.layouytNumber.visibility = View.GONE
                            }

                            binding.tvUserName.text= response.body()!!.data.name
                            binding.tvFollowerValue.text = response.body()!!.data.followers
                            binding.tvFollowingValue.text = response.body()!!.data.following
                            binding.tvEmail.text = response.body()!!.data.email

                            binding.tvNumber.text = response.body()!!.data.mobile
                            binding.tvDob.text = response.body()!!.data.dob
                            binding.tvCity.text = response.body()!!.data.city
                            binding.tvState.text = response.body()!!.data.state

                            binding.tvAbout.text = response.body()!!.data.about

                            if (response.body()!!.data.is_follow == "No") {
                                binding.tvButtonFollow.text = "Follow"
                            } else {
                                binding.tvButtonFollow.text = "Followed"
                            }



                            if (response.body()!!.data.privacy == "0") {
                                binding.tvAbout.visibility = View.VISIBLE
                                binding.tvEmail.visibility = View.VISIBLE
                                binding.tvNumber.visibility = View.VISIBLE
                                binding.tvDob.visibility = View.VISIBLE
                                binding.tvCity.visibility = View.VISIBLE
                                binding.tvState.visibility = View.VISIBLE

                            } else {
                                binding.tvAbout.visibility = View.VISIBLE
                                binding.tvEmail.visibility = View.GONE
                                binding.tvNumber.visibility = View.GONE
                                binding.tvDob.visibility = View.GONE
                                binding.tvCity.visibility = View.GONE
                                binding.tvState.visibility = View.GONE
                            }

                            binding.tvButtonFollow.setOnClickListener {

                                if (response.body()!!.data.is_follow == "No") {
                                    followUnfollowApi("Yes")
                                } else {
                                    followUnfollowApi("No")
                                }

                            }

                            Glide.with(this@ActivityOtherUserProfile).load(response.body()!!.data.image).circleCrop().into(binding.shapeableImageView)


                        } else {

                        }
                    } catch (e: Exception) {

                    }
                }

                override fun onFailure(call: Call<ModelOtheruserProfile>, t: Throwable) {

                }
            })
    }


    private fun followUnfollowApi(
        is_follow: String
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] =
            PrefManager.getInstance(this)!!.userDetail.token
        hashMap["postUserID"] = intent.getStringExtra("id").toString()
        hashMap["follow"] = is_follow
        RestClient.getInst().followUnfollowUser(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    getProfile()
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }


}