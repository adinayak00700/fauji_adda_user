package com.user.foujiadda.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.datingapp.utilities.Constants
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityVendorBinding
import com.user.foujiadda.fragment.FragmentVendorLocationListWithMap
import com.user.foujiadda.models.ModelSelectedFilter


class ActivityVendor : BaseActivity() {
    private lateinit var binding: ActivityVendorBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_vendor)

        val selectedFilter: ModelSelectedFilter? =
            intent.getSerializableExtra("data") as ModelSelectedFilter?

        loadFragment(FragmentVendorLocationListWithMap(selectedFilter))

        binding.toolbar.tvhead.text="Vendors"
        if (selectedFilter!!.filterType == "csd_dealers") {
            binding.toolbar.tvhead.text = "Dealers"
        }else   if (selectedFilter.filterType == "csd_store") {
            binding.toolbar.tvhead.text = "CSD Stores"
        }
        else   if (selectedFilter.filterType == "echs_empanelled") {
            binding.toolbar.tvhead.text = "ECHS Hospitals"
        }else   if (selectedFilter.filterType == "echs_polyclinic") {
            binding.toolbar.tvhead.text = "ECHS Polyclinic"
        }
        else   if (selectedFilter.filterType == "guest_house") {
            binding.toolbar.tvhead.text = "Guest Houses"
        }

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onClick(viewId: Int, view: View?) {

    }

    var backStateName = ""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp: Fragment? = mFragmentManager.findFragmentByTag(backStateName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(binding.frame.id, (fragmentTemp)!!, backStateName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }
}