package com.user.foujiadda.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.PermissionChecker
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityItemDetailsShowBinding
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.models.ModelNews
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.News
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class Item_Details_Show : BaseActivity() {
    var type = ""
    var newsId = ""
    var bitmaps : Bitmap? = null
    private lateinit var binding: ActivityItemDetailsShowBinding
    var WRITE_REQUEST_CODE = 100
    var permissiona = ""
    var permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemDetailsShowBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        newsId = intent.getStringExtra("id").toString()
        getNews(newsId)
        binding.toolbar.tvhead.text = "News"
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(viewId: Int, view: View?) {

    }


    fun getNews(newsId: String) {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["news_categoryID"] = ""
        hashMap["type"] = ""
        hashMap["news_id"] = newsId
        RestClient.getInst().getNews(hashMap).enqueue(object : Callback<ModelNews> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(call: Call<ModelNews>, response: Response<ModelNews>) {
                if (response.body()!!.result) {

                    var news = response.body()!!.data[0]

                    Glide.with(this@Item_Details_Show)
                        .load(news.image)
                        .into(binding.NewsBanner)

                    binding.newsTitle.text = news.title
//                    binding.description.text = news.description
//                    binding.description.setText(Html.fromHtml(news.description, Html.FROM_HTML_MODE_COMPACT));

                    binding.description.setOnLongClickListener {
                        true
                    }

                    binding.description.loadDataWithBaseURL(null, news.description, "text/html", "utf-8", null)
//                    binding.description.setText(Html.fromHtml(news.description, Html.FROM_HTML_MODE_COMPACT));


                    binding.description.loadDataWithBaseURL(
                        null,
                        news.description,
                        "text/html",
                        "utf-8",
                        null
                    )
                    binding.description.setOnLongClickListener(OnLongClickListener { true })
                    binding.description.setLongClickable(false)
                    //   binding.description.setText(Html.fromHtml(news.description, Html.FROM_HTML_MODE_COMPACT));


                    /*binding.description.setText(Html.fromHtml(news.description))*/
                    binding.tvLikeCount.text = news.likeCount


                    if (news.is_like == 1) {
                        binding.imageView12.setColorFilter(
                            getResources().getColor(R.color.colorPrimary)
                        );
                    } else {
                        binding.imageView12.setColorFilter(
                            getResources().getColor(R.color.colorGray)
                        );
                    }

                    binding.layoutShare.setOnClickListener {




                        val requiredPermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
                        val checkVal: Int = checkCallingOrSelfPermission(requiredPermission)
                        
                        requestPermissions(permissions, WRITE_REQUEST_CODE);

                        if (checkVal==PackageManager.PERMISSION_GRANTED) {
                            AsyncTask.execute {
                                try {
                                    val url = URL(news.image)
                                    val connection: HttpURLConnection? = url.openConnection() as HttpURLConnection?
                                    connection!!.connect()
                                    var inputStream: InputStream? = null
                                    inputStream = connection.inputStream
                                    val myBitmap = BitmapFactory.decodeStream(inputStream)
                                    val share = Intent(Intent.ACTION_SEND)
                                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                    share.type = "Image/jpeg"
                                    share.type = "text/html"
                                    share.putExtra(Intent.EXTRA_TEXT,
                                        news.title + "\n" + "https://www.foujiaddanew.com/appplay?" + "newsId=" + news.id
                                                + "\n" + "To download best defence news app Fouji Adda, Click the below link." +
                                                " " + "\n" + "https://play.google.com/store/apps/details?id=com.user.foujiadda"
                                    )
                                    val bytes = ByteArrayOutputStream()
                                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                                    val path = MediaStore.Images.Media.insertImage(
                                        getContentResolver(),
                                        myBitmap,
                                        "Title",
                                        null
                                    )


                                    //  val imageUri = Uri.parse(path)
                                    val imageUri=RestClient.getImageUri(this@Item_Details_Show,myBitmap)
                                    share.putExtra(Intent.EXTRA_STREAM, imageUri)
                                    startActivity(Intent.createChooser(share, "Select"))
                                } catch (e: Exception) {
                                    Toast.makeText(this@Item_Details_Show, e.message, Toast.LENGTH_LONG).show()
                                }
                                Log.d("vcsdvcvvc", "cjhwjecjwjh")

                            }
                        }else{
                            Toast.makeText(this@Item_Details_Show,"Please enable Write permission from Setting",Toast.LENGTH_SHORT).show()
                        }

                        Log.d("vcsdvcvvc", "cjhwjesddcjwjh")

                    }

                    binding.layoutLike.setOnClickListener {

                            if (PrefManager.getInstance(applicationContext)!!.userDetail.token == "skip") {

                                val dialogBinding = EmptyLayoutBinding.inflate(
                                    LayoutInflater.from(applicationContext)
                                )
                                val dialog = MaterialAlertDialogBuilder(
                                    this@Item_Details_Show,
                                    R.style.MyThemeOverlayAlertDialog
                                ).create()
                                dialog.setCancelable(false)
                                dialog.setView(dialogBinding.root)

                                dialogBinding.login.setOnClickListener {
                                    val intent =
                                        Intent(applicationContext, ActivityLogin::class.java)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                }

                                dialogBinding.signup.setOnClickListener {
                                    val intent =
                                        Intent(applicationContext, ActivityLogin::class.java)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                }

                                dialog.show()

                            } else {
                                likeUnlikeApi(news)
                            }

                    }
                }
            }

            override fun onFailure(call: Call<ModelNews>, t: Throwable) {
             makeToast(t.message)
            }
        })
    }



  /*  override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            WRITE_REQUEST_CODE -> if (grantResults[0] === PackageManager.PERMISSION_GRANTED) {
                permissiona = "Access"
            } else {
                //Denied.
            }
        }
    }*/




    private fun likeUnlikeApi(feed: News) {

        val hashMap = java.util.HashMap<String, String>()
            hashMap["token"] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["news_id"] = feed.id

            if (feed.is_like == 0) {
                hashMap["like"] = "1"
            } else {
                hashMap["like"] = "0"
            }
            RestClient.getInst().likeUnlikeNews(hashMap).enqueue(object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                        getNews(newsId)
                    }
                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                    makeToast(t.message)
                }
            })
        }

}
