package com.user.foujiadda.activity


import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.ValidationHelper


class ActivitySplash : BaseActivity() {

    lateinit var context: ActivitySplash
    lateinit var prefManager: PrefManager
    val SPLASH_DURATION: Long = 2000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        context = this
        prefManager = PrefManager(context)
        var newsId: String=""
        var postId: String=""
        if(intent.data!=null){
            val intent = intent
            val uri: Uri? = intent.data
           if ( uri!!.getQueryParameter("newsId")!=null){
               newsId = uri.getQueryParameter("newsId")!!
           }else if (uri.getQueryParameter("postId")!=null)
            postId = uri.getQueryParameter("postId")!!
        }

        var id="";

        var type="";
        if (!ValidationHelper.isNull(newsId)){
            id=newsId
            type="news"
        }else{
            id=postId
            type="post"
        }

        Log.d("asdasdasd",type);
        Log.d("asdasdasd",id);


        Handler(Looper.getMainLooper()).postDelayed({
            if (prefManager.keyIsLoggedIn) {
                startActivity(IntentHelper.getDashboardActivity(context).putExtra("type",type).putExtra("id",id))
            } else {
                startActivity(IntentHelper.getOnboardingScreen(context))
            }
            finish()
        }, SPLASH_DURATION)
    }

    override fun onClick(viewId: Int, view: View?) {

    }
}