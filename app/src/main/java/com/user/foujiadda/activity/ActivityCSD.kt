package com.user.foujiadda.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityCsdBinding
import com.user.foujiadda.fragment.csd.FragmentCSDBase
import com.user.foujiadda.fragment.csd.FragmentCSDForm
import com.user.foujiadda.fragment.csd.FragmentVehicleResult
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.utilities.IntentHelper

class ActivityCSD : AppCompatActivity(), FragmentCSDForm.Callbackk, FragmentCSDBase.Callbackk {
    private lateinit var binding:ActivityCsdBinding
    var type=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_csd)

        loadFragment(FragmentCSDBase(this))
        binding.toolbar.tvhead.text=type

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            onBackPressed()
            }
    }

    var backStateName=""
    fun loadFragment(fragment: Fragment) {

        backStateName = fragment.javaClass.simpleName
        Log.d("asdassdasd",backStateName)
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        fragmentTransaction.add(binding.frame.id, (fragment), backStateName)
        fragmentTransaction.addToBackStack(backStateName)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount==1){
            finish()
        }else{
            supportFragmentManager.popBackStackImmediate()
        }


    }

    override fun onClickOnGetFormResult(selectedFilter: ModelSelectedFilter, cityName: String) {
        if (selectedFilter.type==getString(R.string.car_price) || selectedFilter.type==getString(R.string.bike_price)){
            loadFragment(FragmentVehicleResult(selectedFilter,cityName))
        }else{
            startActivity(IntentHelper.getVendorActivity(this).putExtra("data",selectedFilter))
        }
    }
}