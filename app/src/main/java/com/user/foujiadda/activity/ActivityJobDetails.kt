package com.user.foujiadda.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityJobDetailsBinding
import com.user.foujiadda.fragment.FragmentECHSForm
import com.user.foujiadda.fragment.FragmentJobDescription
import com.user.foujiadda.models.JobData
import com.user.foujiadda.models.ModelSelectedFilter

class ActivityJobDetails : AppCompatActivity() {
    private lateinit var binding: ActivityJobDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_details)
        binding.toolbar.tvhead.text = getString(R.string.job_details)
        val job: JobData? = intent.getSerializableExtra(getString(R.string.data)) as JobData?
        loadFragment(FragmentJobDescription(job!!))
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
    }

    var backStateName = ""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = getSupportFragmentManager()
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        fragmentTransaction.replace(binding.frame.id, (fragment), backStateName)
        fragmentTransaction.commit()
    }


}