package com.user.foujiadda.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityTravelBinding
import com.user.foujiadda.fragment.FragmentCheckPNR
import com.user.foujiadda.fragment.FragmentMCQuota
import com.user.foujiadda.fragment.FragmentMCQuotaDetails
import com.user.foujiadda.fragment.csd.FragmentGuestHouseSearchForm
import com.user.foujiadda.fragment.csd.FragmentMCQBase
import com.user.foujiadda.models.Train

class ActivityTravel : BaseActivity(), FragmentMCQuota.Callbackk, FragmentMCQBase.Callbackk {
    private lateinit var binding:ActivityTravelBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil. setContentView(this,R.layout.activity_travel)
        loadFragment(FragmentMCQBase(this))

        binding.toolbar.tvhead.text="Travel"
   
        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onClick(viewId: Int, view: View?) {

    }

    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        Log.d("asdassdasd",backStateName)
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.frame.id, (fragment), backStateName)
        fragmentTransaction.addToBackStack(backStateName)
        fragmentTransaction.commit()
    }


    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount==1){
            finish()
        }else{
            supportFragmentManager.popBackStackImmediate()
        }

    }

    override fun onClickOnSearchTrain(train: Train) {
        loadFragment(FragmentMCQuotaDetails(train))
    }

}