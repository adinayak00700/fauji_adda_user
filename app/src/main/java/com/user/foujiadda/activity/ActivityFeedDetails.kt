package com.user.foujiadda.activity

import SliderAdapterExample
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityFeedDetailsBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelFeeds
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ActivityFeedDetails : AppCompatActivity() {
    lateinit var binding:ActivityFeedDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      binding=DataBindingUtil.  setContentView(this,R.layout.activity_feed_details)
        getFeeds()

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }

        binding.toolbar.tvhead.text=getString(R.string.post_details)
    }



    fun getFeeds() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["post_id"] = intent.getStringExtra("id").toString()
        RestClient.getInst().getFeeds(hashMap).enqueue(object : Callback<ModelFeeds> {
            override fun onResponse(call: Call<ModelFeeds>, response: Response<ModelFeeds>) {
                try {
                    if (response.body()!!.result) {
                        setupdata(response.body()!!.data[0])
                    } else {

                    }


                } catch (e: Exception) {
                    startActivity(IntentHelper.getLoginScreen(this@ActivityFeedDetails))
                }


            }

            override fun onFailure(call: Call<ModelFeeds>, t: Throwable) {

            }
        })
    }



    private fun likeUnlikeApi(
       feed: Feed
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] =
            PrefManager.getInstance(this)!!.userDetail.token
        hashMap["post_id"] = feed.id
        if (feed.like == 0) {
            hashMap["like"] = getString(R.string.like_1)
        } else {
            hashMap["like"] = getString(R.string.like_0)
        }
        RestClient.getInst().likeUnlikeFeed(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    if (feed.like == 0) {
                        feed.like = 1
                        feed.likeCount = (feed.likeCount.toInt() + 1).toString()
                    } else {
                        feed.like = 0
                        feed.likeCount = (feed.likeCount.toInt() - 1).toString()
                    }
          getFeeds()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }


    private fun followUnfollowApi(
 feed: Feed
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] =
            PrefManager.getInstance(this)!!.userDetail.token
        hashMap["postUserID"] = feed.user_id
        if (feed.already_follow == getString(R.string.no)) {
            hashMap["follow"] = getString(R.string.yes)
        } else {
            hashMap["follow"] =getString(R.string.no)
        }
        RestClient.getInst().followUnfollowUser(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    if (feed.already_follow == getString(R.string.no)) {
                        feed.already_follow = getString(R.string.yes)
                    } else {
                        feed.already_follow = getString(R.string.no)
                    }
                  getFeeds()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }



    fun setupdata(feed: Feed) {
        if (feed.images.size == 0) {
            binding.clBanner.visibility = View.GONE
        } else {
            binding.clBanner.visibility = View.VISIBLE

            val urls: MutableList<String> = ArrayList()
            var slider = SliderAdapterExample(this)

            for (i in feed.images.indices) {
                urls.add(feed.images[i].image)
            }
            slider.addItem(urls as ArrayList<String>)
            binding.imageSlider.setSliderAdapter(slider);
            binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
            binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
            binding.imageSlider.startAutoCycle()
        }

        if (feed.commentCount != R.string.like_0.toString()) {
            binding.tvCommentCountasd.text = "Comment(" + feed.commentCount + ")"
        } else {
            binding.tvCommentCountasd.text = getString(R.string.comment)
        }
        Glide.with(this).load(feed.userProfile).circleCrop().into(binding.shapeableImageView)
        binding.tvTextDescriptioon.text = feed.description
        binding.tvLikeCount.text = feed.likeCount

        var timesAg = TimesAgo2.covertTimeToText(feed.created_at, true)
        binding.tvTime.text = timesAg
        binding.tvUserName.text = feed.user_name;
        //    binding.tvUserName.text=feed.name;
        if (feed.like == 1) {
            binding.imageView12.setColorFilter(
                getResources().getColor(R.color.colorPrimary)
            );
        } else {
            binding.imageView12.setColorFilter(
                getResources().getColor(R.color.colorGray)
            );
        }


        if (feed.already_follow == R.string.yes.toString()) {
            binding.tvFollow.setTextColor(
               getResources().getColor(R.color.colorPrimary)
            )
        } else {
            binding.tvFollow.setTextColor(
                getResources().getColor(R.color.colorGray)
            );
        }



        binding.layoutShare.setOnClickListener {
            var data= "http://foujiaddanew.com/appplay?"+"postId="+feed.id
            IntentHelper.shareAPPDeeplink(this, data)
        }



        binding.layoutLike.setOnClickListener {
            likeUnlikeApi( feed)
        }



        if (feed.already_follow == R.string.yes.toString()) {
            binding.tvFollow.text = getString(R.string.followed)
        } else {
            binding.tvFollow.text = getString(R.string.follow)
        }


        binding.layoutUserDetails.setOnClickListener {


            if (feed.own_post == R.string.no.toString()) {
               startActivity(
                    IntentHelper.getOtherUserProfile(this).putExtra("id", feed.user_id)
                )
            } else {

            }


        }



        binding.shapeableImageView.setOnClickListener {

        }


        if (feed.own_post == R.string.no.toString()) {
            binding.tvFollow.visibility = View.VISIBLE
        } else {
            binding.tvFollow.visibility = View.GONE
        }
        binding.tvFollow.setOnClickListener {
            followUnfollowApi(feed)
        }


    }


}