package com.user.foujiadda.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.mechanic4u.ui.adapter.OnBoardingViewPagerAdapter
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivitySliderBinding
import com.user.foujiadda.databinding.ActivitySliderNewBinding
import com.user.foujiadda.models.OnBoardingModel
import com.user.foujiadda.models.UserDetails
import com.user.foujiadda.utilities.IntentHelper
import okhttp3.internal.wait


class SliderActivity : AppCompatActivity(), View.OnClickListener {

    private val numberOfPages = 3
    private lateinit var onBoardingViewPagerAdapter: OnBoardingViewPagerAdapter

    private var position = 0
    private lateinit var binding: ActivitySliderNewBinding
    private val onBoardingData: MutableList<OnBoardingModel> = ArrayList()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySliderNewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        onBoardingData.add(OnBoardingModel(resources.getString(R.string.sliderFirstTitle), resources.getString(R.string.sliderFirstDescription), R.drawable.oneee))
        onBoardingData.add(OnBoardingModel(resources.getString(R.string.sliderSecondTitle), resources.getString(R.string.sliderSecondDescription), R.drawable.threee))
        onBoardingData.add(OnBoardingModel(resources.getString(R.string.sliderThirdTitle), resources.getString(R.string.sliderThirdDescription), R.drawable.fourrr))
        setOnBoardingViewPager(onBoardingData)
        position = binding.slider.currentItem
        binding.dotsIndicator.setViewPager(binding.slider)
        addSlideChangeListener()
        binding.slider.offscreenPageLimit=3

        if (binding.slider.currentItem == 2)
        {
            binding.tvSkip.visibility = View.VISIBLE
        }


        binding.startBtn.setOnClickListener {
            startActivity(Intent(applicationContext, ActivityLogin::class.java))
            finish()
        }
        binding.tvSkip.setOnClickListener {
            startActivity(Intent(applicationContext, ActivityLogin::class.java))
            finish()
        }
    }



    private fun getItem(i: Int): Int {
        return binding.slider.currentItem + i
    }

    private fun addSlideChangeListener() {


        binding.slider.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if (position==2) {
                    binding.tvSkip.visibility = View.VISIBLE
                }
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
                if (position==2) {
                    binding.tvSkip.visibility = View.VISIBLE
                }
            }
        })
    }


    private fun setOnBoardingViewPager(onBoardingModel: List<OnBoardingModel>) {
        onBoardingViewPagerAdapter = OnBoardingViewPagerAdapter(this, onBoardingModel)
        binding.slider.adapter = onBoardingViewPagerAdapter
    }

    override fun onClick(view: View?) {
        when (view!!.id) {



     /*       R.id.nextBtn -> {
                val current = getItem(+1)
                if (current < onBoardingData.size) {
                    binding.slider.currentItem = current
                } else {
                    startActivity(Intent(applicationContext, ActivityLogin::class.java))
                    finish()
                }
            }
            R.id.skipBtn -> {
                startActivity(Intent(applicationContext, ActivityLogin::class.java))
                finish()
            }*/

        }

    }
}