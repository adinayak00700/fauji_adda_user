package com.user.foujiadda.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import com.example.datingapp.application.App
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import com.user.foujiadda.R

import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivitySrttingsBinding
import com.user.foujiadda.databinding.EmptyLayoutBinding

import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.models.UpdateSettings
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.NetworkUtils
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ActivitySrttings : BaseActivity() {
    lateinit var binding: ActivitySrttingsBinding

    var state1 = ""
    var notification  = ""
    var notificationstatskey  = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this, R.layout.activity_srttings)

        binding.toolbarCommon.tvhead.text="Settings"

        if (PrefManager.getInstance(this)!!.userDetail.token=="skip"){

            binding.setting.visibility = View.GONE
            val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(this))
            val dialog = MaterialAlertDialogBuilder(this, R.style.MyThemeOverlayAlertDialog).create()
            dialog.setCancelable(false)
            dialog.setView(dialogBinding.root)

            dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                val intent = Intent(this, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this.startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
            }

            dialogBinding.signup.setOnClickListener {
                val intent = Intent(this, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this.startActivity(intent)
            }
            dialog.show()

        } else{

            binding.switch2.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                // do something, the isChecked will be
                // true if the switch is in the On position


        binding.toolbarCommon.topAppBar.setNavigationOnClickListener {
            finish()
        }
        binding.toolbarCommon.tvhead.text="Settings"
        getProfile()
                if (isChecked){
                    notification = "notification"
                    notificationstatskey = "1"
                    notificationPrevent()

                }else{
                    notification = "notification"
                    notificationstatskey = "0"
                    notificationPrevent()
                }

            })

            binding.switch3.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                // do something, the isChecked will be
                // true if the switch is in the On position

                if (isChecked){
                    notification = "privacy"
                    notificationstatskey = "1"
                    notificationPrevent()
                }else{
                    notification = "privacy"
                    notificationstatskey = "0"
                    notificationPrevent()
                }
            })

            binding.toolbarCommon.topAppBar.setNavigationOnClickListener {
                finish()

            }


            getProfile()
        }



    }



    override fun onClick(viewId: Int, view: View?) {
        TODO("Not yet implemented")
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(this)!!.userDetail.token
        if (isNetworkAvailable()) {
            RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
                override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                    dismissLoader()
                    if (response.body()!!.result) {

                        if (response.body()!!.data.token == "skip") {

                            PrefManager.getInstance(this@ActivitySrttings)!!.userDetail =
                                response.body()!!.data
                            state1 = ""

                        } else {
                            PrefManager.getInstance(this@ActivitySrttings)!!.userDetail =
                                response.body()!!.data

                            if (response.body()!!.data.notification == "1") {
                                binding.switch2.setChecked(true)
                            } else {
                                binding.switch2.setChecked(false)
                            }

                            if (response.body()!!.data.privacy == "1") {
                                binding.switch3.setChecked(true)
                            } else {
                                binding.switch3.setChecked(false)
                            }
                            /*state1 =  response.body()!!.data.city_id*/
                        }

                    } else {
                        makeToast(response.body()!!.message)
                    }
                }

                override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                    dismissLoader()
                   /* makeToast(getString(R.string.went_wrong_msg))
                    makeToast(t.message)*/
                }
            })
        }else{
            Toast.makeText(this, "No internet Connection Found", Toast.LENGTH_LONG).show()
        }
    }


    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }
    fun notificationPrevent() {
        val hashMap = HashMap<String, String>()

        hashMap["token"] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["type"] =notification
        hashMap["status"] =notificationstatskey

        RestClient.getInst().updateSetting(hashMap).enqueue(object : Callback<UpdateSettings> {
            override fun onResponse(call: Call<UpdateSettings>, response: Response<UpdateSettings>) {

                if (response.body()!!.result) {

                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<UpdateSettings>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }
}