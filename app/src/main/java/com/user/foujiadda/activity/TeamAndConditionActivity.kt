package com.user.foujiadda.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import androidx.databinding.DataBindingUtil
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityAddPostBinding.inflate
import com.user.foujiadda.databinding.ActivityItemDetailsShowBinding
import com.user.foujiadda.databinding.ActivityTeamAndConditionBinding

class TeamAndConditionActivity : AppCompatActivity() {


    private lateinit var binding: ActivityTeamAndConditionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_team_and_condition)


    val web = findViewById<WebView>(R.id.teamcondition)


        web.loadUrl("https://foujiadda.in/admin/api/term_condition");

        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}