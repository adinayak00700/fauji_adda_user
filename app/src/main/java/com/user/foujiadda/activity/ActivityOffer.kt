package com.user.foujiadda.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityOfferBinding
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.fragment.FragmentJobDescription
import com.user.foujiadda.fragment.FragmentJobFilter
import com.user.foujiadda.fragment.FragmentJobList
import com.user.foujiadda.models.JobData
import com.user.foujiadda.utilities.IntentHelper
import user.faujiadda.fragment.FragmentOfferList


class ActivityOffer : AppCompatActivity(), FragmentJobList.Callbackk {
    private lateinit var binding: ActivityOfferBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer)
   /*     binding.toolbar.topAppBar.inflateMenu(R.menu.filter_menu)*/
        loadFragment(FragmentOfferList(""))
        binding.toolbar.tvhead.text = "Offers"
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }


/*        binding.toolbar.topAppBar.menu!!.getItem(0).setOnMenuItemClickListener {
            loadFragment(FragmentJobFilter())
            true
        }*/

        if (PrefManager.getInstance(this)!!.userDetail.token=="skip"){

            val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(this))
            val dialog = MaterialAlertDialogBuilder(this, R.style.MyThemeOverlayAlertDialog).create()
            dialog.setCancelable(false)
            dialog.setView(dialogBinding.root)

            dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                val intent = Intent(this, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this.startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
            }

            dialogBinding.signup.setOnClickListener {
                val intent = Intent(this, ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                this.startActivity(intent)
            }
            dialog.show()

        } else{

            binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    val position = tab!!.position
                    if (position == 0) {
                        loadFragment(FragmentOfferList(""))
                    } else if (position == 1) {
                        loadFragment(FragmentOfferList(""))
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }



    }

    var backStateName = ""



    private fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val manager: FragmentManager = getSupportFragmentManager()
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) { //fragment not in back stack, create it.
            val ft = manager.beginTransaction()
            //   ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right)
            ft.replace(binding.frame.id, fragment, backStateName)
            //  ft.addToBackStack(backStateName);
            ft.commit()
        }
    }






    override fun onClickOnJob(job: JobData) {
        loadFragment(FragmentJobDescription(job))
    }


}