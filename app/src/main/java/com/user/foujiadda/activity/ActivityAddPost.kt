package com.user.foujiadda.activity

import TopCategoryAdapter
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityAddPostBinding
import com.user.foujiadda.fragment.FragmentECHSForm
import com.user.foujiadda.fragment.csd.FragmentCSDBase
import com.user.foujiadda.fragment.csd.FragmentCSDForm
import com.user.foujiadda.fragment.csd.FragmentVehicleResult
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.models.ModelTopCategory
import com.wedguruphotographer.fragments.FragmentAddFeed

class ActivityAddPost : AppCompatActivity(), FragmentAddFeed.Callbackk {
    private lateinit var binding:ActivityAddPostBinding
    var type=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil. setContentView(this,R.layout.activity_add_post)


        loadFragment(FragmentAddFeed(this))
        binding.toolbar.tvhead.text=getString(R.string.add_post)

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
    }

    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.frame.id, (fragment)!!, backStateName)
        fragmentTransaction.commit()
    }

    override fun onPostAdded() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}