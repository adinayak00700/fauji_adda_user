package com.user.foujiadda.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivitySocialAppBinding
import com.user.foujiadda.fragment.FragmentFeeds
import com.wedguruphotographer.fragments.FragmentAddFeed

class ActivitySocialApp : AppCompatActivity() {
    lateinit var navController: NavController
    private lateinit var binding:ActivitySocialAppBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_social_app)
      // loadFragment(FragmentFeeds())
        binding.toolbar.tvhead.text="Feed"
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
        setUpNavigation()




    }

    fun setUpNavigation() {
        navController=findNavController(R.id.nav_host_fragment)


    }

    override fun onBackPressed() {

       Log.d("asdasdasd",supportFragmentManager.backStackEntryCount.toString())
    }

/*    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp: Fragment? = mFragmentManager.findFragmentByTag(backStateName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(binding.frame.id, (fragmentTemp)!!, backStateName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }*/
}