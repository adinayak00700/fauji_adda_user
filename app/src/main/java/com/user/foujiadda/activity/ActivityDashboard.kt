package com.user.foujiadda.activity

import DialogFragmentLoader
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.bumptech.glide.Glide
import com.example.datingapp.application.App
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.user.foujiadda.BuildConfig
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivitySrttings
import com.user.foujiadda.activity.Item_Details_Show
import com.user.foujiadda.adapter.DrawerAdapter
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityDashboardBinding
import com.user.foujiadda.databinding.DialogueupdateBinding
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.HeaderProfileBinding
import com.user.foujiadda.fragment.FragmentAboutUs
import com.user.foujiadda.fragment.FragmentFeeds
import com.user.foujiadda.fragment.FragmentFollowUs

import com.user.foujiadda.utilities.IntentHelper

import com.user.foujiadda.fragment.FragmentHelp
import com.user.foujiadda.models.ModelRateAndReview
import com.user.foujiadda.models.UpdateModelResponse
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import user.faujiadda.fragment.FlagmentHome
import user.faujiadda.fragment.FragmentProfileEdit
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ActivityDashboard : BaseActivity(), DrawerAdapter.Callback {

    lateinit var navController: NavController
    lateinit var binding: ActivityDashboardBinding
    var headerProfileBinding: HeaderProfileBinding? = null

    var skip = ""
    var CSD = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        setUpNavigation()
        //  getPIPmode()
        initDrawer()
        setupDrawer()
        init()


    }

    fun init() {

        setupProfile()
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            setupProfile()
            toggleDrawer()
        }

        binding.toolbar.topAppBar

        binding.toolbar.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.notification -> {
                   // navController.navigate(R.id.idFragmentNotification)
                      startActivity(IntentHelper.getNotificationActivity(this))
                    true
                }
                else -> false
            }
        }

        onNewIntent(intent)
        Log.d("cca", intent.type.toString())
    }


    override fun onNewIntent(intent: Intent?) {
        Log.d("ahbchjav", Gson().toJson(intent))
        super.onNewIntent(intent)
        if (intent!!.getStringExtra("service") != null) {
            intent!!.getStringExtra("service").toString().let {
                Log.d("ahbchjav", it)
            }

            if (intent.getStringExtra("service") == "CSD"){
                startActivity(IntentHelper.getCSDScreen(this))
            }
            if (intent.getStringExtra("service") =="ECHS"){
                startActivity(IntentHelper.getECHSScreen(this))
            }
            if (intent.getStringExtra("service") =="TRAVEL"){
                startActivity(IntentHelper.getTravelScreen(this))
            }
            if (intent.getStringExtra("service") =="NEWS"){
                startActivity(IntentHelper.getNewsActivity(this))
            }
            if (intent.getStringExtra("service") =="JOBS"){
                startActivity(IntentHelper.getJobScreen(this))
            }
            if (intent.getStringExtra("service") =="OFFERS"){
                startActivity(IntentHelper.getOfferScreen(this))
            }
        }

        //deepliking
        if (intent.getStringExtra("type")!=null){
        if (intent.getStringExtra("type").toString()=="news"){
            if (intent.getStringExtra("id")!=null && intent.getStringExtra("id").toString()!=""){
                val intentnew = Intent(this, Item_Details_Show::class.java)
                intentnew.putExtra("id",intent.getStringExtra("id").toString())
                startActivity(intentnew)
            }
        }else if (intent.getStringExtra("type").toString()=="post"){

                if (intent.getStringExtra("id")!=null && intent.getStringExtra("id").toString()!=""){
                    Log.d("jklhlsigybl","sklidjf;sdfghfgjdn")
                 startActivity(IntentHelper.getFeedDetails(this).putExtra("id",intent.getStringExtra("id").toString()))
                }
            }
        }

    }

    override fun onClick(viewId: Int, view: View?) {

    }

    private fun setupDrawer() {
        binding.drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //   binding.menuu.progress= slideOffset
            }

            override fun onDrawerOpened(drawerView: View) {

            }

            override fun onDrawerClosed(drawerView: View) {

            }

            override fun onDrawerStateChanged(newState: Int) {

            }
        })
    }

    fun initDrawer() {
        val header_layout = findViewById<ConstraintLayout>(R.id.header_profile)
        val headerView: View = LayoutInflater.from(this).inflate(R.layout.header_profile, header_layout, false)
        headerProfileBinding = DataBindingUtil.bind(headerView)
        binding.lvDrawer.addHeaderView(headerView)
        headerProfileBinding!!.root.setOnClickListener {
            toggleDrawer()
        }

        headerProfileBinding!!.root.setOnClickListener {
            toggleDrawer()
            binding.navView.menu[3].isChecked = true
            binding.navView.setSelectedItemId(R.id.idFragmentProfileEdit);
        }
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        val skip = PrefManager.getInstance(this)!!.userDetail.token

        val drawerAdapter = DrawerAdapter(this, this, skip)
        binding.lvDrawer.setAdapter(drawerAdapter)
    }

    lateinit var active: Fragment
    val fm: FragmentManager = supportFragmentManager
    val fragmentHome: Fragment = FlagmentHome()
    val fragment2: Fragment = FragmentFeeds()
    val fragment3: Fragment = FragmentHelp()
    val fragment4: Fragment = FragmentProfileEdit()


    fun setUpNavigation() {
        navController = findNavController(R.id.nav_host_fragment)
        setupWithNavController(
            binding.navView,
            navController
        )


        val navigation = findViewById<View>(R.id.nav_view) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment4, "4").hide(fragment4).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment3, "3").hide(fragment3).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment2, "2").hide(fragment2).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragmentHome, "1").hide(fragmentHome).commit()

        active = fragmentHome
    }

    val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.idFragmentHome -> {
                    fm.beginTransaction().hide(active).show(fragmentHome).commit()
                    active = fragmentHome
                    return@OnNavigationItemSelectedListener true
                }
                R.id.idFragmentCommunities -> {
                    fm.beginTransaction().hide(active).show(fragment2).commit()
                    active = fragment2
                    return@OnNavigationItemSelectedListener true
                }
                R.id.idFragmentCOntactUs -> {
                    var f= fragment4 as FragmentProfileEdit
                    f.getProfile()
                    fm.beginTransaction().hide(active).show(fragment3).commit()
                    active = fragment3
                    return@OnNavigationItemSelectedListener true
                }
                R.id.idFragmentProfileEdit -> {
                   var f= fragment4 as FragmentProfileEdit
                    f.getProfile()
                    fm.beginTransaction().hide(active).show(fragment4).commit()
                    active = fragment4
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onClickonDraweerItem(position: Int) {
        when (position) {

            0 ->  if (PrefManager.getInstance(this)!!.userDetail.token == "skip"){
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))
                } catch (e: ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))

                }
            }else {
                /*startActivity(IntentHelper.getSettingsActivity(this))*/
                val changePage = Intent(this, ActivitySrttings::class.java)
                // Error: "Please specify constructor invocation;
                // classifier 'Page2' does not have a companion object"
                startActivity(changePage)

            }

            1-> if (PrefManager.getInstance(this)!!.userDetail.token == "skip"){

                /*var data= "https://play.google.com/store/apps/details?id=com.user.faujiadda"
                IntentHelper.shareAPPDeeplink(this,data)*/


                /*var data= "https://play.google.com/store/apps/details?id=com.user.foujiadda"
                IntentHelper.shareAPPDeeplink(this, data)*/

                shareApp()

            }else{
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))
                } catch (e: ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))
                }


            }
            2->
                if (PrefManager.getInstance(this)!!.userDetail.token == "skip"){

                    startActivity(IntentHelper.getLoginScreen(this))

            }else{
               /* var data= "https://play.google.com/store/apps/details?id=com.user.foujiadda"
                IntentHelper.shareAPPDeeplink(this,data)*/
                    shareApp()
            }

            5 -> {

                if (PrefManager.getInstance(this)!!.userDetail.token == "skip"){
                    makeToast("You need to Login First")
                }else{
                    startActivity(IntentHelper.getRateANdReviewsActivity(this))
                }

            }
            3 ->   if (PrefManager.getInstance(this)!!.userDetail.token == "skip"){

                var custumDialogLoader=DialogFragmentLoader(FragmentFollowUs(),"Follow Us")
                custumDialogLoader.show(supportFragmentManager,"fdfgd")

            }else{

                var custumDialogLoader=DialogFragmentLoader(FragmentAboutUs(),"About Us")
                custumDialogLoader.show(supportFragmentManager,"fd")
            }


            4 -> {
                var custumDialogLoader=DialogFragmentLoader(FragmentFollowUs(),"Follow Us")
                custumDialogLoader.show(supportFragmentManager,"fdfgd")
            }


            6 -> {
                showLogoutDialog(this)
            }
        }
    }


    private fun shareApp(){


        AsyncTask.execute {
            try {
                val url = URL("https://foujiadda.com/wp-content/uploads/2022/07/Dark-Modern-Launch-App-Food-Delivery-Facebook-Ad-3.png")
                val connection: HttpURLConnection? = url.openConnection() as HttpURLConnection?
                connection!!.connect()
                var inputStream: InputStream? = null
                inputStream = connection.inputStream
                val myBitmap = BitmapFactory.decodeStream(inputStream)
                val share = Intent(Intent.ACTION_SEND)
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                share.type = "Image/jpeg"
                share.type = "text/html"
                share.putExtra(Intent.EXTRA_TEXT,"Download best defence news app from Play Store. Check CSD prices," +
                        " ECHS services, MCO quota, Sainik Guest House and ESM Job notification. Click the link to install now."
                        + "\n" +"https://play.google.com/store/apps/details?id=com.user.foujiadda")
                val bytes = ByteArrayOutputStream()
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                val path = MediaStore.Images.Media.insertImage(
                    getContentResolver(),
                    myBitmap,
                    "Title",
                    null
                )


                //  val imageUri = Uri.parse(path)
                val imageUri=RestClient.getImageUri(this,myBitmap)
                share.putExtra(Intent.EXTRA_STREAM, imageUri)
                startActivity(Intent.createChooser(share, "Select"))
            } catch (e: Exception) {
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            }
            Log.d("vcsdvcvvc", "cjhwjecjwjh")

        }






     /*   AsyncTask.execute {
            try {












                val url = URL("https://foujiadda.com/wp-content/uploads/2022/07/Dark-Modern-Launch-App-Food-Delivery-Facebook-Ad-3.png")
                var connection: HttpURLConnection? = null
                connection = url.openConnection() as HttpURLConnection?
                connection!!.connect()
                var inputStream: InputStream? = null
                inputStream = connection.inputStream
                val myBitmap = BitmapFactory.decodeStream(inputStream)
                val share = Intent(Intent.ACTION_SEND)
                share.type = "Image/jpeg"
                share.type = "text/html"
                share.putExtra(Intent.EXTRA_TEXT,"Download best defence news app from Play Store. Check CSD prices," +
                        " ECHS services, MCO quota, Sainik Guest House and ESM Job notification. Click the link to install now."
                        + "\n" +"https://play.google.com/store/apps/details?id=com.user.foujiadda")
                val bytes = ByteArrayOutputStream()
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                val path = MediaStore.Images.Media.insertImage(
                    getContentResolver(),
                    myBitmap,
                    "Title",
                    null
                )
                val imageUri = Uri.parse(path)
                share.putExtra(Intent.EXTRA_STREAM, imageUri)
                startActivity(Intent.createChooser(share, "Select"))






















            } catch (e: Exception) {
            }

        }*/
    }

    private fun toggleDrawer() {
        if (binding.drawerLayout.isDrawerOpen(binding.lvDrawer)) {
            binding.drawerLayout.closeDrawer(binding.lvDrawer)
        } else {
            binding.drawerLayout.openDrawer(binding.lvDrawer)
        }
    }


    fun setupProfile() {
        Glide.with(this).load(PrefManager.getInstance(this)!!.userDetail.avtaar).circleCrop()
            .into(headerProfileBinding!!.ivUserImage)
        headerProfileBinding!!.tvEmail.text = PrefManager.getInstance(this)!!.userDetail.email
        headerProfileBinding!!.tvUserName.text =
            PrefManager.getInstance(this)!!.userDetail.first_name


    }

    override fun onBackPressed() {
        if (active is FragmentFeeds) {
            var fragment = active as FragmentFeeds
            if (!fragment.isBottomsheetExpanded()) {
                fm.beginTransaction().hide(active).show(fragmentHome).commit()
                active = fragmentHome
                binding.navView.getMenu().findItem(R.id.idFragmentHome).setChecked(true);
            }
        } else if (active is FlagmentHome) {
            super.onBackPressed()
        } else {
            fm.beginTransaction().hide(active).show(fragmentHome).commit()
            active = fragmentHome
            binding.navView.getMenu().findItem(R.id.idFragmentHome).setChecked(true);

        }


    }






}