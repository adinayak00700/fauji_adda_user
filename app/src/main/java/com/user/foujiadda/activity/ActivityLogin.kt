package com.user.foujiadda.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController

import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityLoginBinding


class ActivityLogin : BaseActivity() {

    lateinit var navController: NavController
    lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding= DataBindingUtil.setContentView(this, R.layout.activity_login)!!
        setUpNavigation()
    }

    override fun onClick(viewId: Int, view: View?) {

    }


    fun setUpNavigation() {
        navController=findNavController(R.id.nav_host_fragment)
    }
}