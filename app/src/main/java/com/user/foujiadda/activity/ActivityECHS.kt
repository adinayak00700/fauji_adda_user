package com.user.foujiadda.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityEchsBinding
import com.user.foujiadda.fragment.FragmentECHSForm

class ActivityECHS : AppCompatActivity() {
    private lateinit var binding:ActivityEchsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil. setContentView(this,R.layout.activity_echs)
        loadFragment(FragmentECHSForm(type = getString(R.string.echs_empanelled)))



        binding.toolbar.tvhead.text=getString(R.string.echs)
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()

        }

        binding.tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position
                if (position==0){
                    loadFragment(FragmentECHSForm(type = getString(R.string.echs_empanelled)))
                }else if (position==1){
                    loadFragment(FragmentECHSForm(type = getString(R.string.echs_polyclinic)))
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
    }
    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = getSupportFragmentManager()
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        fragmentTransaction.replace(binding!!.frame.id, (fragment)!!, backStateName)
        fragmentTransaction.commit()
    }
}