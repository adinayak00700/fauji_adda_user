package com.user.foujiadda.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.tabs.TabLayout
import com.mechanicforyou.user.utilities.DialogNewsFilter
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseActivity
import com.user.foujiadda.databinding.ActivityNewsBinding
import com.user.foujiadda.fragment.FragmentNews
import com.user.foujiadda.models.ModelNewsFilter
import com.user.foujiadda.models.NewsFilter
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ActivityNews : BaseActivity(), DialogNewsFilter.Callbackk {

    var fragmentList = ArrayList<Fragment>()
    var search : String = ""
    private lateinit var binding: ActivityNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news)
        val view = binding.root


        setContentView(view)

        getNewsFilters();


        binding.toolbar.topAppBar.setOnClickListener {
            onBackPressed()
        }

        binding.toolbar.filter.setOnClickListener {
            var dialog= DialogNewsFilter(this,this,filterList)
            dialog.show()
        }

        binding.toolbar.search.setOnClickListener {
            binding.toolbar.filter.visibility = View.GONE
            binding.toolbar.search.visibility = View.GONE
            binding.toolbar.searchInput.visibility =View.VISIBLE
            binding.toolbar.searchCloseBtn.visibility =View.VISIBLE



            binding.toolbar.searchInput.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {
                    search  = binding.toolbar.searchInput.text.toString()
                    loadFragment(FragmentNews(type = "recent", searchKey =search ))
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    loadFragment(FragmentNews(type = "recent", searchKey =search ))
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    loadFragment(FragmentNews(type = "recent", searchKey =search ))
                }
            })
        }

        binding.toolbar.searchCloseBtn.setOnClickListener {
            binding.toolbar.filter.visibility = View.VISIBLE
            binding.toolbar.search.visibility = View.VISIBLE
            binding.toolbar.searchInput.visibility =View.GONE
            binding.toolbar.searchCloseBtn.visibility =View.GONE
        }





        loadFragment(FragmentNews(type = "recent", searchKey =search ))

        binding.tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position
                if (position==0){
                    loadFragment(FragmentNews(type = "recent", searchKey =search))
                }else if (position==1){
                    loadFragment(FragmentNews(type = "popular", searchKey =search))
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })

    }

    var filterList = ArrayList<NewsFilter>()

    fun getNewsFilters() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        RestClient.getInst().getNewsCategory(hashMap)
            .enqueue(object : Callback<ModelNewsFilter?> {
                override fun onResponse(
                    call: Call<ModelNewsFilter?>,
                    response: Response<ModelNewsFilter?>
                ) {

               if (response.body()!!.result){
                 filterList.addAll(response.body()!!.data)
               /*    var viewpager = NewsViewPagerAdapter(supportFragmentManager, fragmentList, titleList)
                   binding.viewpager.adapter = viewpager*/
               }

                }

                override fun onFailure(call: Call<ModelNewsFilter?>, t: Throwable) {

                }
            })
    }

    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = getSupportFragmentManager()
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        fragmentTransaction.replace(binding!!.frame.id, (fragment)!!, backStateName)
        fragmentTransaction.commit()
    }

    override fun onSelectFilter(filter: String) {
        Log.d("ajhdgsasd","askdhkas")
       var fragment= supportFragmentManager.findFragmentById(R.id.frame) as FragmentNews
        fragment.getNews(filter)

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(viewId: Int, view: View?) {
        TODO("Not yet implemented")
    }


}