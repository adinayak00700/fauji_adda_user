package com.user.foujiadda.activity

import DialogFragmentLoader
import FragmentVerification
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ActivityMobileNumberBinding

class ActivityMobileNumber : AppCompatActivity() {
    lateinit var fragmentLoader: DialogFragmentLoader

    private var requestCode : String = ""
    private var emauil : String = ""
    lateinit var binding:ActivityMobileNumberBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.  setContentView(this,R.layout.activity_mobile_number)

       requestCode =  intent.getStringExtra("requestCode").toString()
       emauil =  intent.getStringExtra("email").toString()

        binding.toolbar.tvhead.text="Update Mobile Number"

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }

        intent.let {

        }

        binding.buttonOk.setOnClickListener {
            fragmentLoader = DialogFragmentLoader(
                FragmentVerification(
                    "mobileUpdate",
                    binding.etMobileNumber.text.toString(),
                    emauil,
                    object : FragmentVerification.Callbackk {
                        override fun onUserVerifiedForSignUp(mobile: String?) {

                        }

                        override fun onMobileNumberVerified(mobile: String?) {
                            fragmentLoader.dismiss()
                            var nesData=Intent()
                            nesData.putExtra("mobile",mobile)
                            setResult(Activity.RESULT_OK,nesData)
                            finish()
                        }

                        override fun onUserVerifiedForResetPassword(mobile: String?) {

                        }

                        override fun onUserVerifiedForChangeNumber(mobile: String?) {

                        }
                    }), "Verification"
            )


            fragmentLoader.show(supportFragmentManager, "asd")
        }
    }
}