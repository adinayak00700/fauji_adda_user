import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DailyNeedRowsBinding
import com.user.foujiadda.databinding.FeedRowsBinding
import com.user.foujiadda.databinding.VendorReviewsRowsBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelSuccess

import com.user.foujiadda.models.News
import com.user.foujiadda.models.VendorReview
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.models.viewmodel.VendorReviewViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.PriceFormatter
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class VendorReviewsAdapter(
    val viewModel: VendorReviewViewModel,
    val context: Context,
    var callback: Callbackk
) :
    RecyclerView.Adapter<VendorReviewsAdapter.NewsViewHolder>() {
    var list = ArrayList<VendorReview>()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): VendorReviewsAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.vendor_reviews_rows, parent, false)


        return NewsViewHolder(DataBindingUtil.bind<VendorReviewsRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: VendorReviewsAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: VendorReviewsRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(review: VendorReview) {

            binding.tvUserName.text=review.userName
            binding.tvTextDescriptioon.text=review.review

            Glide.with(context).load(review.user_image).circleCrop().into(binding.shapeableImageView)

            binding.ratingBar.rating=review.rating.toFloat()





            binding.tvTime.text=TimesAgo2.covertTimeToText(review.created_at,false)

        }

    }

    fun submitList(newData: List<VendorReview>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk {
        fun onClickONCommentLayout(postId: Int);
    }


}