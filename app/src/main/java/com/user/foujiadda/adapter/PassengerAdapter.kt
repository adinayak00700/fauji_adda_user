import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.user.foujiadda.R
import com.user.foujiadda.databinding.PassengerRowsBinding
import com.user.foujiadda.models.Passenger


class PassengerAdapter(var context: Context, var list: ArrayList<Passenger>) :
    RecyclerView.Adapter<PassengerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.passenger_rows, parent, false)

        return MyViewHolder(DataBindingUtil.bind<PassengerRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
      holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    public interface Callbackk {
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }

    inner class MyViewHolder(var binding: PassengerRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(passenger: Passenger) {
            binding.tvName.text = passenger.name
            binding.tvStatus.text = passenger.current_status
        }
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}