import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.user.foujiadda.R
import com.user.foujiadda.databinding.NotificationRowsBinding
import com.user.foujiadda.models.NotificationData
import com.user.foujiadda.models.viewmodel.NotificationViewModel
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.TimesAgo2


class NotificationAdapter(val viewModel: NotificationViewModel, val context: Context , var callback: NotifictionDeleteBack) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {

    var list= ArrayList<NotificationData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.notification_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<NotificationRowsBinding>(view)!!)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(var binding: NotificationRowsBinding) : RecyclerView.ViewHolder(binding.root)
    {
        @RequiresApi(Build.VERSION_CODES.N)
        fun bind(notification: NotificationData) {
            /*Glide.with(context).load(notification.image).circleCrop().into(binding.ivNotification)*/
            binding.tvSubTitle.setText(Html.fromHtml(notification.message, Html.FROM_HTML_MODE_COMPACT));
            binding.tvTitle.text = notification.title
            binding.date.text = TimesAgo2.covertTimeToText(notification.created_at, true)

            binding.layoutNotificationItem.setOnClickListener {

                callback.onClickNotification(notification)
            }

            binding.delete.setOnClickListener {
                callback.onNotificationDeleteClick(notification)
            }
        }
    }

    fun submitList(newData: List<NotificationData>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    interface NotifictionDeleteBack{
        fun onNotificationDeleteClick(notification: NotificationData)

        fun onClickNotification(notification: NotificationData)
    }


}