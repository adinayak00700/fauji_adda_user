package com.user.foujiadda.adapter

import android.R.attr.x
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ItemrateandreviewBinding
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.models.viewmodel.ViewModelRateAndReview
import com.user.foujiadda.utilities.TimesAgo2


class RateAndReviewAdapter(val viewModel: ViewModelRateAndReview, val context: Context, var callback: RateCallBAck):
    RecyclerView.Adapter<RateAndReviewAdapter.NewsViewHolder>()  {
    var list= ArrayList<Vendor>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RateAndReviewAdapter.NewsViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.itemrateandreview, parent, false)
        return NewsViewHolder(DataBindingUtil.bind<ItemrateandreviewBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: RateAndReviewAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: ItemrateandreviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(rateview: Vendor) {
            Glide.with(context).load(rateview.image).circleCrop().into(binding.shapeableImageView)
            binding.tvName.text=rateview.name

//            binding.tvAddress.text=vendor.address

            var timesAg = TimesAgo2.covertTimeToText(rateview.created_at, true)

            binding.tvtvType.text=timesAg

           /* binding.root.setOnClickListener {
                callback.onClickOnVendor(rateview)
            }*/


            binding.editReview.setOnClickListener {
                callback.onCickEditReview(rateview, true)
            }

            val factor: Double = rateview.avg_rating.toDouble()


            val result = Math.round(x * factor) / factor
            binding.ratingBar.rating = result.toFloat()
            binding.appCompatTextView3.text= "(" + rateview.total_rating + ")"


            binding.tvcommentCount.text = "%.2f".format(rateview.avg_rating)

            binding.tvAddress.text = rateview.review
        }
    }

    fun submitList(newData: List<Vendor>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface RateCallBAck{
        fun onCickEditReview(vendor: Vendor , edit : Boolean)
    }
}