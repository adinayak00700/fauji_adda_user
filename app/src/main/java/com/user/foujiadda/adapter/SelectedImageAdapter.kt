import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.CategoryRowsBinding
import com.user.foujiadda.databinding.GalleryUploadRowsBinding
import com.user.foujiadda.models.Category
import com.wedguruphotographer.model.ModelGalleryUpload


class SelectedImageAdapter(var context: Context, var list: ArrayList<ModelGalleryUpload>, var callback:Callbackk) :
    RecyclerView.Adapter<SelectedImageAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.gallery_upload_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<GalleryUploadRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {



        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    public interface Callbackk{
        fun onClickOnCategory(catId:String)
    }

    inner class MyViewHolder(var binding: GalleryUploadRowsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(image:ModelGalleryUpload){
            Glide.with(context).load(image.imagePath).into(binding.ivImage)
        }
    }



    fun add(modelPhotos: ModelGalleryUpload, isNewSelected: Boolean) {
        if (isNewSelected) {
            val modelPhotos1 =
                ModelGalleryUpload(modelPhotos.imagePath,)
            modelPhotos1.newSelected=true
            list.add(modelPhotos1)
        } else {
            list.add(modelPhotos)
        }
        notifyItemInserted(list.size - 1)
        if (list.size == 0) {
        } else {
        }
    }

    fun addAllData(list: java.util.ArrayList<ModelGalleryUpload>) {
        this.list = list
        notifyDataSetChanged()
    }

    val newSelectedPhotos: java.util.ArrayList<String>
        get() {
            val photos = java.util.ArrayList<String>()
            for (i in list.indices) {
                if (list[i].newSelected) {
                    photos.add(list[i].imagePath)
                }
            }
            return photos
        }
    val allPhotos: java.util.ArrayList<String>
        get() {
            val photos = java.util.ArrayList<String>()
            for (i in list.indices) {
                photos.add(list[i].imagePath)
            }
            return photos
        }

/*    fun remove(holder: GalleryUploadAdapter.Holder) {
        val actualPosition = holder.adapterPosition
        list.removeAt(actualPosition)
        notifyItemRemoved(actualPosition)
        notifyItemRangeChanged(actualPosition, list.size)
        if (list.size == 0) {
          //  callbackk.onNoPhotosSelected()
        }
    }*/

/*    private fun apiHitRemoveImgage(position: Int, albumId: String, holder: Holder) {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.kToken] =
            PrefManager.getInstance(context).getUserDetail().getData().getJwtToken()
        hashMap["id"] = albumId
        RestClient.getInst().removeAlbumApi(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body().isStatus()) {
                    remove(holder)
                } else {
                }
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT)
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }*/



    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }



}