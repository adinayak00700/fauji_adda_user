
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


internal class NewsViewPagerAdapter(
    fm: FragmentManager?,
    fragments: List<Fragment>?,
    titleLists: List<String>
) :
    FragmentPagerAdapter(fm!!) {
    private var mFragmentList: List<Fragment>? = ArrayList()
    private var mFragmentTitleList: List<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return mFragmentList!![position]
    }

    override fun getCount(): Int {
        return mFragmentList?.size ?: 0
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }

    init {
        mFragmentList = fragments
        mFragmentTitleList = titleLists
    }
}