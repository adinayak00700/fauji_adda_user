import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.user.foujiadda.R
import com.user.foujiadda.models.ModelTopCategory


class TopCategoryAdapter(
    var context: Context,
    var list: MutableList<ModelTopCategory>,
    var callback: Callbackk
) :
        RecyclerView.Adapter<TopCategoryAdapter.MyViewHolder>() {
    var row_index = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.horizontal_large_cat_rows, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {

            row_index = position
            notifyDataSetChanged()
            callback.onClickOnTopCat(list.get(position).categoryName)

        }
        holder.catName.text = list.get(position).categoryName



        if (row_index == position) {
            holder.backgroundLayout.setBackground(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.small_primary_round_corner_border
                )
            )
            holder.catName.setTextColor(ContextCompat.getColor(context, R.color.black))
        } else {

            holder.backgroundLayout.setBackground(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.small_primary_round_corner
                )
            )
            holder.catName.setTextColor(ContextCompat.getColor(context, R.color.white))

        }


    }

    override fun getItemCount(): Int {
        return list.size
    }
     fun setCurrentCat(position: Int) {
       row_index=position
         notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var catName = itemView.findViewById<TextView>(R.id.tvStartTime)
       var backgroundLayout = itemView.findViewById<ConstraintLayout>(R.id.mainLayout)
    }


    interface Callbackk {
        fun onClickOnTopCat(catId: String);
    }
}