import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import com.user.foujiadda.R
import com.user.foujiadda.databinding.ItemFilterLayoutBinding
import com.user.foujiadda.models.JobFilterData
import com.user.foujiadda.models.ModelTopCategory
import com.user.foujiadda.models.NewsFilter


class NewsFilterAdapter(
    var context: Context,
    var list: MutableList<NewsFilter>,
    var callback: Callbackk
) :
    RecyclerView.Adapter<NewsFilterAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_filter_layout, parent, false)
        return MyViewHolder(DataBindingUtil.bind<ItemFilterLayoutBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(var binding : ItemFilterLayoutBinding) : RecyclerView.ViewHolder(binding.root) {


        fun  bind(filter:NewsFilter){
            binding.tvTitle.text=filter.name


            binding.root.setOnClickListener {
                callback.onCLickONFilter(filter.id)
                Log.d("asdasdasd","asdkjas")
            }
        }

    }


    interface Callbackk {
        fun onCLickONFilter(catId: String);
    }
}