import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.user.foujiadda.R
import com.user.foujiadda.activity.Item_Details_Show
import com.user.foujiadda.databinding.DailyNeedRowsBinding

import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.NewsViewModel

class NewsRecyclerAdapter(
    val viewModel: NewsViewModel,
    var list: ArrayList<News>,
    val context: Context
) :
    RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): NewsRecyclerAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.daily_need_rows, parent, false)

 /*       view.layoutParams = ViewGroup.LayoutParams((parent.width * 0.5).toInt(),ViewGroup.LayoutParams.MATCH_PARENT)*/

        return NewsViewHolder(DataBindingUtil.bind<DailyNeedRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: NewsRecyclerAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))

        holder.itemView.setOnClickListener {

            val intent = Intent(context, Item_Details_Show::class.java)
            intent.putExtra("id",list[position].id)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }



    inner class NewsViewHolder(var binding: DailyNeedRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(news: News) {

            Glide.with(context).load(news.image).into(binding.ivNewsImage)
            binding.tvTitle.text=news.title


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.tvWeight.setText(Html.fromHtml(news.description, Html.FROM_HTML_MODE_COMPACT));
            } else {
                binding.tvWeight.setText(Html.fromHtml(news.description));
            }

        }

    }

}