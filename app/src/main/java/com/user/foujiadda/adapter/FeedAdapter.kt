import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DailyNeedRowsBinding
import com.user.foujiadda.databinding.FeedRowsBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelSuccess

import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.HashMap

class FeedAdapter(
    val viewModel: FeedViewModel,
    val context: Context,
    var callback: Callbackk,
    var menuCallBack : OptionsMenuClickListener
) :
    RecyclerView.Adapter<FeedAdapter.NewsViewHolder>() {
    var list = ArrayList<Feed>()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): FeedAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.feed_rows, parent, false)
        return NewsViewHolder(DataBindingUtil.bind<FeedRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: FeedAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: FeedRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(feed: Feed) {


            if (feed.images.size == 0) {
                binding.clBanner.visibility = View.GONE
            } else {

                binding.clBanner.visibility = View.VISIBLE

                val urls: MutableList<String> = ArrayList()
                var slider = SliderAdapterExample(context)

                for (i in feed.images.indices) {
                    urls.add(feed.images[i].image)
                }
                slider.addItem(urls as ArrayList<String>)
                binding.imageSlider.setSliderAdapter(slider);
                binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                binding.imageSlider.startAutoCycle();

            }

            if (feed.commentCount != "0") {
                binding.tvTextShare.text = "Comment(" + feed.commentCount + ")"
            } else {
                binding.tvTextShare.text = "Comment"
            }
            Glide.with(context).load(feed.userProfile).circleCrop().into(binding.shapeableImageView)
            binding.tvTextDescriptioon.text = feed.description
            binding.tvLikeCount.text = feed.likeCount

            var timesAg = TimesAgo2.covertTimeToText(feed.created_at, true)
            binding.tvTime.text = timesAg
            binding.tvUserName.text = feed.user_name;
            //    binding.tvUserName.text=feed.name;
            if (feed.like == 1) {
                binding.imageView12.setImageResource(R.drawable.ic_vector__1_)
            } else {
                binding.imageView12.setImageResource(R.drawable.likee)
            }


            if (feed.already_follow == "Yes") {
                binding.tvFollow.setTextColor(
                    context.getResources().getColor(R.color.colorPrimary)
                );
            } else {
                binding.tvFollow.setTextColor(
                    context.getResources().getColor(R.color.colorGray)
                );
            }


            binding.tvFollowMenu.setOnClickListener {
               menuCallBack.onOptionsMenuClicked(position, feed.id)

            }


            binding.layoutShare.setOnClickListener {

                if (feed.images.isEmpty()){
//                    var data= "http://foujiaddanew.com/appplay?"+"postId="+feed.id + "" + "\n" +feed.description
                    var data= feed.user_name+ " " +"has asked on Fouji Adda app."+ "\n" + feed.description + "\n" +"Click below link to reply post"+
                            "\n" + "https://www.foujiaddanew.com/appplay?" + "postId=" + feed.id
                    IntentHelper.shareAPPDeeplink(context,data)
                }else{
                    AsyncTask.execute {
                        try {

                            val url = URL(feed.images[0].image)
                            var connection: HttpURLConnection? = null
                            connection = url.openConnection() as HttpURLConnection?
                            connection!!.connect()
                            var inputStream: InputStream? = null
                            inputStream = connection.inputStream
                            val myBitmap = BitmapFactory.decodeStream(inputStream)
                            val share = Intent(Intent.ACTION_SEND)
                            share.type = "Image/jpeg"
                            share.type = "text/html"
                            share.putExtra(Intent.EXTRA_TEXT, feed.user_name+ " " +"has asked on Fouji Adda app."+ "\n" + feed.description + "\n" +"Click below link to reply post"+
                                    "\n" + "https://www.foujiaddanew.com/appplay?" + "postId=" + feed.id)
                            val bytes = ByteArrayOutputStream()
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                            val path = MediaStore.Images.Media.insertImage(context.getContentResolver(), myBitmap, "Title", null)
                            val imageUri = Uri.parse(path)
                            share.putExtra(Intent.EXTRA_STREAM, imageUri)
                            context.startActivity(Intent.createChooser(share, "Select"))
                        } catch (e: Exception) {
                        }
                    }
                }
            }

            binding.layoutLike.setOnClickListener {
                likeUnlikeApi(adapterPosition, feed)
            }

            if (feed.already_follow == "Yes") {
                binding.tvFollow.text = "Followed"
            } else {
                binding.tvFollow.text = "Follow"
            }


            /*binding.layoutUserDetails.setOnClickListener {

                if (feed.own_post == "No") {
                    context.startActivity(
                        IntentHelper.getOtherUserProfile(context).putExtra("id", feed.user_id)
                    )
                }
            }*/

            binding.tvUserName.setOnClickListener {

                if (feed.own_post == "No") {
                    context.startActivity(
                        IntentHelper.getOtherUserProfile(context).putExtra("id", feed.user_id)

                    )
                }else{
                    context.startActivity(
                        IntentHelper.getOtherUserProfile(context).putExtra("id", feed.user_id).putExtra("userId", feed.own_post)
                    )
                }
            }

            binding.layoutComment.setOnClickListener {
                callback.onClickONCommentLayout(list.get(position).id.toInt())
            }

          /*  binding.shapeableImageView.setOnClickListener {
                callback.onProfileClick(list[position].id.toInt())
            }

            binding.tvUserName.setOnClickListener {
                callback.onProfileClick(list[position].id.toInt())
                Log.d("acuac",list[position].id.toString() )
            }

            binding.tvTime.setOnClickListener {
                callback.onProfileClick(list[position].id.toInt())
            }*/

            if (feed.own_post == "No") {
                binding.tvFollow.visibility = View.VISIBLE
            } else {
                binding.tvFollow.visibility = View.GONE
            }
            binding.tvFollow.setOnClickListener {
                followUnfollowApi(adapterPosition, feed)
            }

        }

    }


    private fun likeUnlikeApi(
        position: Int, feed: Feed
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["post_id"] = feed.id
        if (feed.like == 0) {
            hashMap["like"] = "1"
        } else {
            hashMap["like"] = "0"
        }
        RestClient.getInst().likeUnlikeFeed(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    if (feed.like == 0) {
                        feed.like = 1
                        feed.likeCount = (feed.likeCount.toInt() + 1).toString()
                    } else {
                        feed.like = 0
                        feed.likeCount = (feed.likeCount.toInt() - 1).toString()
                    }
                    viewModel.update(feed, position)
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }


    private fun followUnfollowApi(position: Int, feed: Feed) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["postUserID"] = feed.user_id
        if (feed.already_follow == "No") {
            hashMap["follow"] = "Yes"
        } else {
            hashMap["follow"] = "No"
        }
        RestClient.getInst().followUnfollowUser(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    if (feed.already_follow == "No") {
                        feed.already_follow = "Yes"
                    } else {
                        feed.already_follow = "No"
                    }
                    viewModel.update(feed, position)
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }


    fun submitList(newData: List<Feed>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk {
        fun onClickONCommentLayout(postId: Int)
        fun onProfileClick(postId: Int)

    }


    // create an interface for onClickListener
    // so that we can handle data most effectively in MainActivity.kt
    interface OptionsMenuClickListener {
        fun onOptionsMenuClicked(position: Int, id  : String)
    }



}