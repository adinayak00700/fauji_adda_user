import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.activity.Item_Details_Show
import com.user.foujiadda.databinding.NewsRowsBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsAdapter(
    val viewModel: NewsViewModel,
    val context: Context
) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    var list= ArrayList<News>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): NewsAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.news_rows, parent, false)
        /*       view.layoutParams = ViewGroup.LayoutParams((parent.width * 0.5).toInt(),ViewGroup.LayoutParams.MATCH_PARENT)*/
        return NewsViewHolder(DataBindingUtil.bind<NewsRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: NewsAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))

        holder.itemView.setOnClickListener {
            val intent = Intent(context, Item_Details_Show::class.java)
            intent.putExtra("id", list[position].id)
            context.startActivity(intent)

        }
    }

   fun addItem() {
       list=viewModel.list
       notifyDataSetChanged()
    }


    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }



    inner class NewsViewHolder(var binding: NewsRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(news: News) {

            Glide.with(context).load(news.image).into(binding.ivNewsImage)
            binding.tvTitle.text=news.title


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.tvWeight.text = Html.fromHtml(news.description, Html.FROM_HTML_MODE_COMPACT);
            }else{
                binding.tvWeight.text=(Html.fromHtml(news.description));
            }

            if (news.is_like == 1) {
                binding.imageView12.setColorFilter(
                    context.getResources().getColor(R.color.colorBlue)
                );
            } else {
                binding.imageView12.setColorFilter(
                    context.getResources().getColor(R.color.colorGray)
                );
            }

            binding.layoutShare.setOnClickListener {
                var data= "http://foujiaddanew.com/appplay?"+"postId="+news.id
                IntentHelper.shareAPPDeeplink(context,data)
            }

            binding.layoutLike.setOnClickListener {
                likeUnlikeApi(adapterPosition, news)
            }
        }
    }

    private fun likeUnlikeApi(position: Int, feed: News) {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["news_id"] = feed.id
        if (feed.is_like == 0) { hashMap["like"] = "1"
        } else { hashMap["like"] = "0"
        }
        RestClient.getInst().likeUnlikeFeed(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    if (feed.is_like == 0) {
                        feed.is_like = 1
                        feed.likeCount = (feed.likeCount.toInt() + 1).toString()
                    } else {
                        feed.is_like = 0
                        feed.likeCount = (feed.likeCount.toInt() - 1).toString()
                    }
                    viewModel.update(feed, position)
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }
}