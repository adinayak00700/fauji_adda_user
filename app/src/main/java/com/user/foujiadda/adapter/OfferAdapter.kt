import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.OfferRowsBinding
import com.user.foujiadda.models.Offer


class OfferAdapter(var context: Context, var callback:CallbackkOffer) :
    RecyclerView.Adapter<OfferAdapter.MyViewHolder>() {
    var list = ArrayList<Offer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.offer_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<OfferRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            //  callbackk.onClickOnAddress(list.get(position))
        }

        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    /*public interface Callbackk {
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }*/

    inner class MyViewHolder(var binding: OfferRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(offer: Offer) {

            Glide.with(context)
                .load(offer.image)
                .placeholder(R.drawable.logo)
                .circleCrop()
                .into(binding.ivNotification)
            binding.tvTitle.text = offer.title

            binding.tvSubTitle.text = "Use Coupon : " + offer.coupon_code


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.tvSubTitle2.setText(
                    Html.fromHtml(
                        offer.description,
                        Html.FROM_HTML_MODE_COMPACT
                    )
                )
            } else {
                binding.tvSubTitle2.setText(Html.fromHtml(offer.description));
            }
            binding.tvApplyCoupon.setOnClickListener {
                if (!offer.activate_offer.isEmpty()) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(offer.activate_offer))
                    context.startActivity(browserIntent)
                    callback.onClickOnApplyCoupan(list[position].id)
                }else{
                    makeToast(context.getString(R.string.offer_not_avaibel_data))
                }
            }
        }
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


    public fun addTolIst(array: ArrayList<Offer>) {
        this.list = array
        notifyDataSetChanged()
    }



    public interface CallbackkOffer{
        fun onClickOnApplyCoupan(id : String )
    }
}