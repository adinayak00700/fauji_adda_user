import android.content.Context
import android.database.DatabaseUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.ProductRowsBinding
import com.user.foujiadda.models.Product


class ProductAdapter(var context: Context) :
    RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {
    var productsList=ArrayList<Product>();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.product_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<ProductRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(productsList[position])

    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    public interface Callbackk{
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }

    inner class MyViewHolder(var binding: ProductRowsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun  bind(product: Product){
            binding.tvPrice.setText(product.cost_price)
            binding.tvQuantity.setText(product.unit_value+" "+product.unit )


        }
    }

    public fun addProducts(data: List<Product>) {
        this.productsList= data as ArrayList<Product>
    }



    private fun makeToast(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }


}