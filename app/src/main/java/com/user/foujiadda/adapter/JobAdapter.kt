import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DailyNeedRowsBinding
import com.user.foujiadda.databinding.FeedRowsBinding
import com.user.foujiadda.databinding.JobRowsBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.JobData
import com.user.foujiadda.models.ModelSuccess

import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.JobViewModel
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class JobAdapter(
    val viewModel: JobViewModel,
    val context: Context,
    var callback:Callbackk
) :
    RecyclerView.Adapter<JobAdapter.NewsViewHolder>() {
    var list= ArrayList<JobData>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): JobAdapter.NewsViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.job_rows, parent, false)

        return NewsViewHolder(DataBindingUtil.bind<JobRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: JobAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    fun addItem() {
        list=viewModel.list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: JobRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(job: JobData) {

            binding.tvTitle.text=job.title
            binding.tvTitle2.text=job.qualification
            binding.tvAmount.text="INR "+job.salary+"/" + job.salary_type


            if (job.time_type=="full_time"){
                binding.tvType.text="Full Time"
            }else if (job.time_type=="part_time") {
                binding.tvType.text="Part Time"
            }else{
                binding.tvType.text = job.time_type

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.description.text = Html.fromHtml(job.description, Html.FROM_HTML_MODE_COMPACT);
            }else{
                binding.description.text=(Html.fromHtml(job.description));
            }

//            binding.description.text=job.description
                binding.tvStartDate.text=job.start_date+" - "+job.end_date
          /*  Glide.with(context)
                .load(job.image)
                .placeholder(R.color.colorPrimary)
                .into(binding.ivIMage)*/

            if (job.image.isEmpty()){
                Glide.with(context).load(R.drawable.fauji_adda_logo_new).into(binding.ivIMage)
            }else {
                Glide.with(context).load(job.image).into(binding.ivIMage)
            }
            binding.root.setOnClickListener {
                callback.onClickOnJob(job)
            }
        }
    }

    fun submitList(newData: List<JobData>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk{
        fun onClickOnJob(job: JobData);
    }


}