import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DailyNeedRowsBinding
import com.user.foujiadda.databinding.FeedRowsBinding
import com.user.foujiadda.databinding.VendorRowsBinding
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelSuccess

import com.user.foujiadda.models.News
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.models.viewmodel.VendorViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class VendorAdapter(
    val viewModel: VendorViewModel,
    val context: Context,
    var callback:Callbackk
) :
    RecyclerView.Adapter<VendorAdapter.NewsViewHolder>() {
    var list= ArrayList<Vendor>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): VendorAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.vendor_rows, parent, false)
        return NewsViewHolder(DataBindingUtil.bind<VendorRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: VendorAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }
    fun addItem() {
        list=viewModel.list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: VendorRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(vendor: Vendor) {
            Glide.with(context).load(vendor.image).circleCrop().into(binding.shapeableImageView)
            binding.tvName.text=vendor.name
            binding.tvAddress.text=vendor.address

            if(vendor.distance.isEmpty() || vendor.distance=="0"){
                binding.tvtvType.visibility = View.GONE
            }else {
                binding.tvtvType.text = vendor.distance + "Km"
            }

            binding.root.setOnClickListener {
                callback.onClickOnVendor(vendor)
            }

            binding.ratingBar.rating=vendor.avg_rating.toFloat()
            binding.tvcommentCount.text=vendor.total_rating.toString()
        }
    }

    fun submitList(newData: List<Vendor>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk{
        fun onClickOnVendor(vendor: Vendor);
    }


}