import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DailyNeedRowsBinding
import com.user.foujiadda.databinding.CommentRowsBinding
import com.user.foujiadda.models.Comment
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.ModelSuccess

import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.CommentViewModel
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.TimesAgo2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class CommentAdapter(
    val viewModel: CommentViewModel,
    val context: Context,
    var callback:Callbackk
) :
    RecyclerView.Adapter<CommentAdapter.CommentViewholder>() {
    var list= ArrayList<Comment>()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): CommentAdapter.CommentViewholder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.comment_rows, parent, false)


        return CommentViewholder(DataBindingUtil.bind<CommentRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: CommentAdapter.CommentViewholder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class CommentViewholder(var binding: CommentRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.tvName.text=comment.user_name
                binding.tvTitle.text=comment.comment
            binding.tvDate.text=TimesAgo2.covertTimeToText(comment.created_at,true)

            Glide.with(context).load(comment.image).circleCrop().into(binding.ivUserImage)

        }
    }



    fun submitList(newData: List<Comment>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk{
        fun onClickONCommentLayout(postId: Int);
    }


}