import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.AvtaarRowsBinding

import com.user.foujiadda.models.Avtaaar
import com.user.foujiadda.models.JobData

import com.user.foujiadda.models.viewmodel.JobViewModel

class AvtaarAdapter(
    val context: Context,
    var callback:Callbackk
) :
    RecyclerView.Adapter<AvtaarAdapter.NewsViewHolder>() {
    var list= ArrayList<Avtaaar>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): AvtaarAdapter.NewsViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.avtaar_rows, parent, false)

        return NewsViewHolder(DataBindingUtil.bind<AvtaarRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: AvtaarAdapter.NewsViewHolder, position: Int) {
        holder.bind(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class NewsViewHolder(var binding: AvtaarRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(javatar: Avtaaar) {

            Glide.with(context).load(javatar.image).circleCrop().into(binding.ivImage)

            binding.root.setOnClickListener {
callback.onClickOnAvtaar(javatar)

            }
        }
    }

    fun submitList(newData: List<Avtaaar>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    public interface Callbackk{
        fun onClickOnAvtaar(avtaar: Avtaaar);
    }


}