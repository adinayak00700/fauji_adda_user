import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.user.foujiadda.R
import com.user.foujiadda.databinding.CategoryRowsBinding
import com.user.foujiadda.models.Category


class CategoryAdapter(var context: Context, var list: ArrayList<Category>, var callback:Callbackk) :
    RecyclerView.Adapter<CategoryAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.category_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<CategoryRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.itemView.setOnClickListener {
         callback.onClickOnCategory(list[position].id)
        }

        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    public interface Callbackk{
         fun onClickOnCategory(catId:String)
    }

    inner class MyViewHolder(var binding: CategoryRowsBinding) : RecyclerView.ViewHolder(binding.root) {
            fun bind(category:Category){
                binding.tvTitle.text=category.name
                Glide.with(context).load(category.image).into(binding.imageview)
            }
    }



    private fun makeToast(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }


}