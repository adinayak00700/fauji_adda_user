package com.user.foujiadda.adapter;

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R
import com.user.foujiadda.databinding.LeftMenuRowsBinding

import java.util.*

class DrawerAdapter(private val context: Context, private val callback: Callback, skip : String) :
    BaseAdapter() {
    var binding: LeftMenuRowsBinding? = null
    private val icons: ArrayList<Drawable?> = ArrayList<Drawable?>()
    private val title = ArrayList<String>()
    override fun getCount(): Int {
       return icons.size
    }

    override fun getItem(position: Int): Any {
        TODO("Not yet implemented")
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = LayoutInflater.from(context).inflate(R.layout.left_menu_rows, parent, false)
        binding = DataBindingUtil.bind(view)
        binding!!.tvTitle.setText(title[position])
        binding!!.ivIcon.setImageDrawable(icons[position])
        binding!!.getRoot().setOnClickListener(View.OnClickListener { callback.onClickonDraweerItem(position) })
        return view
    }


    interface Callback {
        fun onClickonDraweerItem(position: Int)
    }

    init {


        if (skip == "skip"){
            title.add(context.resources.getString(R.string.rateapp))
            title.add(context.resources.getString(R.string.share_app))
            title.add(context.getString(R.string.loginsignup))
            title.add(context.resources.getString(R.string.follow_us))

            icons.add(context.resources.getDrawable(R.drawable.rate))
            icons.add(context.resources.getDrawable(R.drawable.share))
            icons.add(context.resources.getDrawable(R.drawable.login))
            icons.add(context.resources.getDrawable(R.drawable.likee))

        }else {

            title.add(context.resources.getString(R.string.settings))
            title.add(context.resources.getString(R.string.rateapp))
            title.add(context.resources.getString(R.string.share_app))
            title.add(context.resources.getString(R.string.about_us))
            title.add(context.resources.getString(R.string.follow_us))
            title.add(context.resources.getString(R.string.review_n_comment))
            title.add(context.resources.getString(R.string.logout))


            icons.add(context.resources.getDrawable(R.drawable.setting))
            icons.add(context.resources.getDrawable(R.drawable.rate))
            icons.add(context.resources.getDrawable(R.drawable.share))
            icons.add(context.resources.getDrawable(R.drawable.ic_u_exclamation_circle))
            icons.add(context.resources.getDrawable(R.drawable.likee))
            icons.add(context.resources.getDrawable(R.drawable.commentt))

            icons.add(context.resources.getDrawable(R.drawable.logout))
        }


    }

}