package com.user.foujiadda.networking;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import user.faujiadda.models.ModelSubcategory;

import com.user.foujiadda.models.AboutUs;
import com.user.foujiadda.models.ForgetPasswordModel;
import com.user.foujiadda.models.ModelAvtar;
import com.user.foujiadda.models.ModelBanner;
import com.user.foujiadda.models.ModelCategory;
import com.user.foujiadda.models.ModelComment;
import com.user.foujiadda.models.ModelFeeds;
import com.user.foujiadda.models.ModelJob;
import com.user.foujiadda.models.ModelJobFilter;
import com.user.foujiadda.models.ModelMCQ;
import com.user.foujiadda.models.ModelNews;

import com.user.foujiadda.models.ModelNewsFilter;
import com.user.foujiadda.models.ModelNotification;
import com.user.foujiadda.models.ModelOffers;
import com.user.foujiadda.models.ModelOtheruserProfile;
import com.user.foujiadda.models.ModelPNR;
import com.user.foujiadda.models.ModelProducts;

import com.user.foujiadda.models.ModelRateAndReview;
import com.user.foujiadda.models.ModelRegionalData;
import com.user.foujiadda.models.ModelStation;
import com.user.foujiadda.models.ModelSuccess;
import com.user.foujiadda.models.ModelTrain;
import com.user.foujiadda.models.ModelUser;

import com.user.foujiadda.models.ModelVehicleDetails;
import com.user.foujiadda.models.ModelVendor;
import com.user.foujiadda.models.ModelVendorDetails;
import com.user.foujiadda.models.ModelVendorReviews;
import com.user.foujiadda.models.UpdateModelResponse;
import com.user.foujiadda.models.UpdateSettings;
import com.user.foujiadda.models.spins.ModelCity;
import com.user.foujiadda.models.spins.ModelCompany;

import com.user.foujiadda.models.spins.ModelState;

import com.user.foujiadda.models.spins.ModelVarient;
import com.user.foujiadda.models.spins.ModelVehicle;
import com.user.foujiadda.models.viewmodel.ModelDepot;
import com.user.foujiadda.models.viewmodel.ModelSuccessPost;
import com.user.foujiadda.utilities.UrlFactory;


public class RestClient {


    private static RestClient mInstance = new RestClient();
    OkHttpClient client;
    Retrofit retrofit;
    private RestService mRestService;

    private RestClient() {
    }

    public static RestClient getInst() {
        return mInstance;
    }

    /**
     * Here is the setup call for http service for this app. setup is done once in MyApplication class.
     * Step 1 : Logging is added using HttpLoggingInterceptor. Logging is removed from retrofit2 so
     * it has to be added as a part of OkHttp interceptor.
     * Step 2 : Build an OkHttpClient with logging interceptor.
     * Step 3 : Retrofit is build with baseUrl, okhttp client, Gson Converter factory for easy JSON to POJO conversion.
     */
    public void setup() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES);
        // Should be used only in Debug Mode.
        if (true) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(true ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE); //// TODO: 21-07-2016
            builder.addInterceptor(interceptor);
        }
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        client = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(UrlFactory.getBaseUrl())
                .client(client)
                //.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mRestService = retrofit.create(RestService.class);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

   public Call<ModelUser> userSignUp(HashMap hashMap) {
        return mRestService.signup(hashMap);
    }
    public Call<ModelBanner> getBanners(HashMap hashMap) {
        return mRestService.getBanners(hashMap);
    }

    public Call<ModelVendorReviews> getReviews(HashMap hashMap) {
        return mRestService.getReviews(hashMap);
    }

    public Call<ModelOffers> getOffers(HashMap hashMap) {
        return mRestService.getOffers(hashMap);
    }

    public Call<ModelState> getState(HashMap hashMap) {
        return mRestService.getState(hashMap);
    }

    public Call<ModelRegionalData> getRegionalData(HashMap hashMap) {
        return mRestService.getRegionalData(hashMap);
    }

    public Call<ModelNewsFilter> getNewsCategory(HashMap hashMap) {
        return mRestService.getNewsCategory(hashMap);
    }



    public Call<ModelVehicleDetails> vehicle_detail(HashMap hashMap) {
        return mRestService.vehicle_detail(hashMap);
    }


    public Call<ModelCity> getCity(HashMap hashMap) {
        return mRestService.getCity(hashMap);
    }

    public Call<ModelCity> getHospitalCity(HashMap hashMap) {
        return mRestService.getHospitalCity(hashMap);
    }
    public Call<ModelSuccess> addLead(HashMap hashMap) {
        return mRestService.addLead(hashMap);
    }
    public Call<ModelSuccess> updateLocation(HashMap hashMap) {
        return mRestService.updateLocation(hashMap);
    }


    public Call<ModelCompany> getVehicleBrand(HashMap hashMap) {
        return mRestService.getVehicleBrand(hashMap);
    }


    public Call<ModelJobFilter> getJobFilters(HashMap hashMap) {
        return mRestService.getJobFilters(hashMap);
    }




    public Call<ModelVehicle> getVehicleModel(HashMap hashMap) {
        return mRestService.getVehicleModel(hashMap);
    }


    public Call<ModelSuccess> change_password(HashMap hashMap) {
        return mRestService.change_password(hashMap);
    }
    public Call<ModelSuccess> sendOtp(HashMap hashMap) {
        return mRestService.send_otp(hashMap);
    }




    public Call<ModelVarient> getVehicleVarient(HashMap hashMap) {
        return mRestService.getVehicleVarient(hashMap);
    }

    public Call<ModelDepot> getDepot(HashMap hashMap) {
        return mRestService.getDepot(hashMap);
    }


    public Call<ModelCategory> getCategories(HashMap hashMap) {
        return mRestService.getCategories(hashMap);
    }
    public Call<ModelNews> getNews(HashMap hashMap) {
        return mRestService.getNews(hashMap);
    }
    public Call<ModelUser> userLogin(HashMap hashMap) {
        return mRestService.login(hashMap);
    }

    public Call<ForgetPasswordModel> forgetPassword(HashMap hashMap) {
        return mRestService.forgetPassword(hashMap);
    }

    public Call<UpdateSettings> updateSetting(HashMap hashMap) {
        return mRestService.updateSetting(hashMap);
    }

    public Call<ModelUser> getProfile(HashMap hashMap) {
        return mRestService.getProfile(hashMap);
    }


    public Call<ModelOtheruserProfile> getOtheruserProfile(HashMap hashMap) {
        return mRestService.getOtheruserProfile(hashMap);
    }

    public Call<ModelSubcategory> subcategory(HashMap hashMap) {
        return mRestService.subcategory(hashMap);
    }

    public Call<ModelNotification> getNotification(HashMap hashMap) {
        return mRestService.getNotification(hashMap);
    }

    public Call<ModelNotification> getNotificationDelete(HashMap hashMap) {
        return mRestService.getNotificationDelete(hashMap);
    }

    public Call<ModelSuccess> likeUnlikeFeed(HashMap hashMap) {
        return mRestService.likeUnlikeFeed(hashMap);
    }

    public Call<ModelSuccessPost> likeBlockFeed(HashMap hashMap) {
        return mRestService.likeBlockFeed(hashMap);
    }

    public Call<ModelSuccessPost> likePostReportFeed(HashMap hashMap) {
        return mRestService.likePostReportFeed(hashMap);
    }

    public Call<ModelSuccess> likeUnlikeNews(HashMap hashMap) {
        return mRestService.likeUnlikeNews(hashMap);
    }
    public Call<ModelSuccess> followUnfollowUser(HashMap hashMap) {
        return mRestService.followUnfollowUser(hashMap);
    }
    public Call<ModelVendor> getVendors(HashMap hashMap) {
        return mRestService.getVendors(hashMap);
    }

    public Call<ModelRateAndReview> getRateAndReview(HashMap hashMap) {
        return mRestService.getRateAndReview(hashMap);
    }

    public Call<UpdateModelResponse> getupdate() {
        return mRestService.getGetUpdate();
    }


    public Call<ModelVendorDetails> getVendorDetails(HashMap hashMap) {
        return mRestService.getVendorDetails(hashMap);
    }
    public Call<ModelSuccess> addReview(HashMap hashMap) {
        return mRestService.addReview(hashMap);
    }

    public Call<ModelSuccess> editReview(HashMap hashMap) {
        return mRestService.editReview(hashMap);
    }

    public Call<ModelAvtar> getAvtaars(HashMap hashMap) {
        return mRestService.getAvtaars(hashMap);
    }

    public Call<ModelFeeds> getFeeds(HashMap hashMap) {
        return mRestService.getFeeds(hashMap);
    }
    public Call<ModelJob> getJobs(HashMap hashMap) {
        return mRestService.getJobs(hashMap);
    }

    public Call<ModelComment> getComments(HashMap hashMap) {
        return mRestService.getComment(hashMap);
    }

    public Call<ModelSuccess> addComments(HashMap hashMap) {
        return mRestService.addComment(hashMap);
    }

    public Call<ModelMCQ> getMCLIST(HashMap hashMap) {
        return mRestService.getMCLIST(hashMap);
    }


    public Call<ModelStation> getStationList(HashMap hashMap) {
        return mRestService.getStationList(hashMap);
    }

    public Call<ModelTrain> getTrainList(HashMap hashMap) {
        return mRestService.getTrainList(hashMap);
    }

    public Call<ModelPNR> getPNRDetails(HashMap hashMap) {
        return mRestService.getPNRDetails(hashMap);
    }

    public Call<ModelProducts> products(HashMap hashMap) {
        return mRestService.products(hashMap);
    }

    public Call<ModelJob> jobAPply(HashMap hashMap) {
        return mRestService.jobAPply(hashMap);
    }

    public Call<AboutUs> aboutUs() {
        return mRestService.aboutUs();
    }

    public void addPost(
            String jwtToken,
            String text,
            ArrayList<String> imagePath,
            Callback<ModelSuccess> callback) {

        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        RequestBody text1 = RequestBody.create(MediaType.parse("text/plain"), text);

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imagePath.size()];
        for (int index = 0; index < imagePath.size(); index++) {
            File file = new File(imagePath.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[index] = MultipartBody.Part.createFormData("image[]", file.getName(), surveyBody);
        }

        Call<ModelSuccess> call = mRestService.addPost(jwtToken1, surveyImagesParts, text1);
        call.enqueue(callback);
    }


/*
   public void hitEditProfileApi(
            String jwtToken,
            String phone,
            String first_name,
            String last_name,
            String email,
            String dob,
            String gender,

            File file_image, Boolean isImageSelected,
            Callback<ModelSuccess> callback) {
        RequestBody phone1 = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        RequestBody first_name1 = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody last_name1 = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody dob1 = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody gender1 = RequestBody.create(MediaType.parse("text/plain"), gender);
        if (isImageSelected) {
            RequestBody thumbnailBody = RequestBody.create(MediaType.parse("image/*"), file_image);
            MultipartBody.Part profile_image1 = MultipartBody.Part.createFormData("image", file_image.getName(), thumbnailBody); //image[] for multiple image
            Call<ModelSuccess> call = mRestService.editProfileApi(profile_image1,jwtToken1,first_name1,email1);
            call.enqueue(callback);
        } else {
            Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,first_name1,email1);
            call.enqueue(callback);
        }

    }


    public void hitEditNumberProfileApi(
            String jwtToken,
            String phone,
            Callback<ModelSuccess> callback) {
        RequestBody phone1 = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
            Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,phone1);
            call.enqueue(callback);
        }

*/



    public void etProfileApi(
            String jwtToken,
            String first_name,
            String last_name,
            String email,
            String dob,
            String aboout,
            String state,
            String city,
            Callback<ModelSuccess> callback) {
        RequestBody firstname1 = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody lastname1 = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody dob1 = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody aboout1 = RequestBody.create(MediaType.parse("text/plain"), aboout);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        RequestBody state1 = RequestBody.create(MediaType.parse("text/plain"), state);
        RequestBody city1 = RequestBody.create(MediaType.parse("text/plain"), city);
        Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,firstname1,lastname1,email1,dob1,aboout1,state1,city1);
        call.enqueue(callback);
    }
    public void hitEditProfileApiOnlyPhoto(
            String jwtToken,
            File file_image,
            Callback<ModelSuccess> callback) {

        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);

        RequestBody thumbnailBody = RequestBody.create(MediaType.parse("image/*"), file_image);
        MultipartBody.Part profile_image1 = MultipartBody.Part.createFormData("image", file_image.getName(), thumbnailBody); //image[] for multiple image
        Call<ModelSuccess> call = mRestService.updateProfileImage(profile_image1,jwtToken1);
        call.enqueue(callback);

    }



    public void etProfileApi(
            String jwtToken,
            String avtaar,
            Callback<ModelSuccess> callback) {
        RequestBody avtaar1 = RequestBody.create(MediaType.parse("text/plain"), avtaar);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,avtaar1);
        call.enqueue(callback);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage,"IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

}
