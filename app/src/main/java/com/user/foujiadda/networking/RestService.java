package com.user.foujiadda.networking;




import com.example.datingapp.utilities.Constants;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import user.faujiadda.models.ModelSubcategory;

import com.user.foujiadda.models.AboutUs;
import com.user.foujiadda.models.ForgetPasswordModel;
import com.user.foujiadda.models.ModelAvtar;
import com.user.foujiadda.models.ModelBanner;
import com.user.foujiadda.models.ModelCategory;
import com.user.foujiadda.models.ModelComment;
import com.user.foujiadda.models.ModelFeeds;
import com.user.foujiadda.models.ModelJob;
import com.user.foujiadda.models.ModelJobFilter;
import com.user.foujiadda.models.ModelMCQ;
import com.user.foujiadda.models.ModelNews;

import com.user.foujiadda.models.ModelNewsFilter;
import com.user.foujiadda.models.ModelNotification;
import com.user.foujiadda.models.ModelOffers;
import com.user.foujiadda.models.ModelOtheruserProfile;
import com.user.foujiadda.models.ModelPNR;
import com.user.foujiadda.models.ModelProducts;

import com.user.foujiadda.models.ModelRateAndReview;
import com.user.foujiadda.models.ModelRegionalData;
import com.user.foujiadda.models.ModelStation;
import com.user.foujiadda.models.ModelSuccess;
import com.user.foujiadda.models.ModelTrain;
import com.user.foujiadda.models.ModelUser;

import com.user.foujiadda.models.ModelVehicleDetails;
import com.user.foujiadda.models.ModelVendor;
import com.user.foujiadda.models.ModelVendorDetails;
import com.user.foujiadda.models.ModelVendorReviews;
import com.user.foujiadda.models.UpdateModelResponse;
import com.user.foujiadda.models.UpdateSettings;
import com.user.foujiadda.models.spins.ModelCity;
import com.user.foujiadda.models.spins.ModelCompany;

import com.user.foujiadda.models.spins.ModelState;


import com.user.foujiadda.models.spins.ModelVarient;
import com.user.foujiadda.models.spins.ModelVehicle;
import com.user.foujiadda.models.viewmodel.ModelDepot;
import com.user.foujiadda.models.viewmodel.ModelSuccessPost;

public interface RestService {

    @POST(Constants.SIGNUPAPI)
    Call<ModelUser> signup(@Body HashMap hashMap);

    @POST("login")
    Call<ModelUser> login(@Body HashMap hashMap);

    @POST("forgot_password")
    Call<ForgetPasswordModel> forgetPassword(@Body HashMap hashMap);


    @POST(Constants.BANNER_API)
    Call<ModelBanner> getBanners(@Body HashMap hashMap);



    @POST("update_setting")
    Call<UpdateSettings> updateSetting(@Body HashMap hashMap);


    @POST(Constants.VENDOR_LIST)
    Call<ModelVendor> getVendors(@Body HashMap hashMap);


    @POST(Constants.USER_RATE_LIST)
    Call<ModelRateAndReview> getRateAndReview(@Body HashMap hashMap);

    @POST(Constants.APP_UPDATE_AVAILABLE)
    Call<UpdateModelResponse> getGetUpdate();

    @POST(Constants.VENDOR_DETAILS)
    Call<ModelVendorDetails> getVendorDetails(@Body HashMap hashMap);


    @POST(Constants.ADD_RATING)
    Call<ModelSuccess> addReview(@Body HashMap hashMap);

    @POST(Constants.EDIT_RATING)
    Call<ModelSuccess> editReview(@Body HashMap hashMap);

    @POST(Constants.AVTAAR_LIST)
    Call<ModelAvtar> getAvtaars(@Body HashMap hashMap);

    @POST(Constants.CATEGORY_API)
    Call<ModelCategory> getCategories(@Body HashMap hashMap);
    
    @POST(Constants.GET_PROFILE)
    Call<ModelUser> getProfile(@Body HashMap hashMap);
    
    @POST(Constants.getOtheruserProfile)
    Call<ModelOtheruserProfile> getOtheruserProfile(@Body HashMap hashMap);

    @POST(Constants.NEWS_LIST)
    Call<ModelNews> getNews(@Body HashMap hashMap);

    @POST(Constants.SUBCATEGORY_API)
    Call<ModelSubcategory> subcategory(@Body HashMap hashMap);

    @POST(Constants.PRODUCTS_LIST_API)
    Call<ModelProducts> products(@Body HashMap hashMap);

    @POST(Constants.POST_LIST)
    Call<ModelFeeds> getFeeds(@Body HashMap hashMap);

    @POST(Constants.JOB_LIST)
    Call<ModelJob> getJobs(@Body HashMap hashMap);

    @POST(Constants.JOB_APPLY)
    Call<ModelJob> jobAPply(@Body HashMap hashMap);

    @GET(Constants.ABOUTUS)
    Call<AboutUs> aboutUs();

    @POST(Constants.NOTIFICATION)
    Call<ModelNotification> getNotification(@Body HashMap hashMap);

    @POST(Constants.NOTIFICATIONDELETE)
    Call<ModelNotification> getNotificationDelete(@Body HashMap hashMap);



    @POST(Constants.COMMENTS_LIST)
    Call<ModelComment> getComment(@Body HashMap hashMap);




    @POST(Constants.COMMENTS_ADD)
    Call<ModelSuccess> addComment(@Body HashMap hashMap);



    @POST(Constants.MCOLIST)
    Call<ModelMCQ> getMCLIST(@Body HashMap hashMap);
    @POST(Constants.STATION_LIST)
    Call<ModelStation> getStationList(@Body HashMap hashMap);
    @POST(Constants.TRAIN_LIST)
    Call<ModelTrain> getTrainList(@Body HashMap hashMap);
    @POST(Constants.CHECK_PNR)
    Call<ModelPNR> getPNRDetails(@Body HashMap hashMap);

    @POST(Constants.STATE_LIST)
    Call<ModelState> getState(@Body HashMap hashMap);

    @POST(Constants.VEHICLE_DETAIL)
    Call<ModelVehicleDetails> vehicle_detail(@Body HashMap hashMap);


    @POST(Constants.REGIONAL_LIST)
    Call<ModelRegionalData> getRegionalData(@Body HashMap hashMap);


    @POST(Constants.LIKE_UNLIKE_API)
    Call<ModelSuccess> likeUnlikeFeed(@Body HashMap hashMap);


    @POST(Constants.LIKE_BLOCK_API)
    Call<ModelSuccessPost> likeBlockFeed(@Body HashMap hashMap);

    @POST(Constants.LIKE_POST_REPORT_API)
    Call<ModelSuccessPost> likePostReportFeed(@Body HashMap hashMap);

    @POST(Constants.FOLLOW_UNFOLLOW_USER)
    Call<ModelSuccess> followUnfollowUser(@Body HashMap hashMap);

    @POST(Constants.LIKE_UNLIKE_NEWS_API)
    Call<ModelSuccess> likeUnlikeNews(@Body HashMap hashMap);

    @POST(Constants.CITY_LIST)
    Call<ModelCity> getCity(@Body HashMap hashMap);

    @POST(Constants.CITY_LIST_HOSPITAL)
    Call<ModelCity> getHospitalCity(@Body HashMap hashMap);

    @POST(Constants.ADD_LEAD_HOSPITAL)
    Call<ModelSuccess> addLead(@Body HashMap hashMap);

    @POST(Constants.UPDATE_LOCATION)
    Call<ModelSuccess> updateLocation(@Body HashMap hashMap);



    @POST(Constants.NEWS_CATEGORY)
    Call<ModelNewsFilter> getNewsCategory(@Body HashMap hashMap);

    @POST(Constants.VENDOR_REVIEWS)
    Call<ModelVendorReviews> getReviews(@Body HashMap hashMap);


    @POST(Constants.OFFER_LIST)
    Call<ModelOffers> getOffers(@Body HashMap hashMap);


    @POST(Constants.JOB_FILTERS)
    Call<ModelJobFilter> getJobFilters(@Body HashMap hashMap);


    @POST(Constants.BRAND_LIST)
    Call<ModelCompany> getVehicleBrand(@Body HashMap hashMap);

    @POST(Constants.MODEL_LIST)
    Call<ModelVehicle> getVehicleModel(@Body HashMap hashMap);


    @POST(Constants.CHANGE_PASSWORD)
    Call<ModelSuccess> change_password(@Body HashMap hashMap);
    @POST(Constants.SEND_OTP)
    Call<ModelSuccess> send_otp(@Body HashMap hashMap);



    @POST(Constants.VARIENT_LIST)
    Call<ModelVarient> getVehicleVarient(@Body HashMap hashMap);

    @POST(Constants.DEPOT)
    Call<ModelDepot> getDepot(@Body HashMap hashMap);



    @Multipart
    @POST(Constants.UPDATE_PROFILE_API)
    Call<ModelSuccess> updateProfileImage(
            @Part MultipartBody.Part profile_image,
            @Part("token") RequestBody jwtToken);


    @Multipart
    @POST(Constants.UPDATE_PROFILE_API)
    Call<ModelSuccess> editProfileApi(
            @Part("token") RequestBody jwtToken,
            @Part("first_name") RequestBody firstnam,
            @Part("last_name") RequestBody lastname,
            @Part("email") RequestBody email,
            @Part("dob") RequestBody dob,
            @Part("about") RequestBody about,
            @Part("state_id") RequestBody state,
            @Part("city_id") RequestBody city

    );

    @Multipart
    @POST(Constants.UPDATE_PROFILE_API)
    Call<ModelSuccess> editProfileApi(
            @Part("token") RequestBody jwtToken,
            @Part("avtaar_id") RequestBody avtaar


    );

    @Multipart
    @POST("post_add")
    Call<ModelSuccess> addPost(
            @Part("token") RequestBody jwtToken,
            @Part MultipartBody.Part[] file_url,
            @Part("description") RequestBody couple_id
    );


/*    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> updateProfileImage(
            @Part MultipartBody.Part profile_image,
            @Part("jwtToken") RequestBody jwtToken);


    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part MultipartBody.Part profile_image,
            @Part("jwtToken") RequestBody jwtToken,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
         *//*   @Part("email") RequestBody email,*//*
            @Part("dob") RequestBody dob,
            @Part("gender") RequestBody gender);

    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part("jwtToken") RequestBody jwtToken,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
       *//*     @Part("email") RequestBody email,*//*
            @Part("dob") RequestBody dob,
            @Part("gender") RequestBody gender);
    @Multipartl
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part("jwtToken") RequestBody jwtToken,
            @Part("number") RequestBody first_name);*/



}


