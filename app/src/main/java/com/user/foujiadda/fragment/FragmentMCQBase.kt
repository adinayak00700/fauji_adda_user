package com.user.foujiadda.fragment.csd

import TopCategoryAdapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentResultOwner
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentMcqBaseBinding
import com.user.foujiadda.fragment.FragmentCheckPNR
import com.user.foujiadda.fragment.FragmentECHSForm
import com.user.foujiadda.fragment.FragmentMCQuota
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.models.Train
import com.user.foujiadda.utilities.IntentHelper


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FragmentMCQBase(var callback:Callbackk) : Fragment(), FragmentCSDForm.Callbackk,
    FragmentMCQuota.Callbackk {
    private var type: String? = null
    private var param2: String? = null
    private lateinit var binding:FragmentMcqBaseBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getString(ARG_PARAM1)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_mcq_base, container, false)
        binding=DataBindingUtil.bind<FragmentMcqBaseBinding>(view)!!
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadFragment(FragmentMCQuota(this@FragmentMCQBase))
        binding.tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab!!.position
                if (position==0){
                    loadFragment(FragmentMCQuota(this@FragmentMCQBase))
                }/*else if (position==1){
                    loadFragment(FragmentCheckPNR())
                }*/
                else if (position==1){
                    loadFragment(FragmentGuestHouseSearchForm())
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
    }


    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = childFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.frame.id, (fragment)!!, backStateName)
        fragmentTransaction.commit()
    }





    public interface Callbackk{
        fun  onClickOnSearchTrain(train: Train)
    }

    override fun onClickOnGetFormResult(selectedFilter: ModelSelectedFilter, cityName: String) {

    }

    override fun onClickOnSearchTrain(train: Train) {
        callback.onClickOnSearchTrain(train)
    }
}