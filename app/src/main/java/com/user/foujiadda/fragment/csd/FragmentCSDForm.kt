package com.user.foujiadda.fragment.csd

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentCSDFormBinding
import com.user.foujiadda.models.ModelCustumSpinner
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.models.ModelVendor
import com.user.foujiadda.models.spins.*
import com.user.foujiadda.models.viewmodel.ModelDepot
import com.user.foujiadda.models.viewmodel.VendorViewModel
import com.user.foujiadda.networking.RestClient
import com.wedguruphotographer.adapter.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentCSDForm(var type: String, var callback: Callbackk) : Fragment() {

    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val brandList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val depotList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val modelList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
  //  val varientList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val vehicleTypeList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()

    var types : String = ""
    var isClickable : Boolean = false


    private lateinit var binding: FragmentCSDFormBinding
    private lateinit var selectedState: ModelCustumSpinner
    private lateinit var selectedCity: ModelCustumSpinner
    private lateinit var selectedDepot: ModelCustumSpinner
    private lateinit var selectedCarCompany: ModelCustumSpinner
    private lateinit var selectedModel: ModelCustumSpinner
    private lateinit var selectedVarient: ModelCustumSpinner
    private lateinit var selectedVehicleType: ModelCustumSpinner


    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var cityAdapter: CustumSpinAdapter
    private lateinit var brandAdapter: CustumSpinAdapter
    private lateinit var modelAdapter: CustumSpinAdapter

    // private lateinit var varientAdapter: CustumSpinAdapter
    private lateinit var vehicleTypeAdapter: CustumSpinAdapter
    private lateinit var depotAdapter: CustumSpinAdapter

    var selectedFilter: ModelSelectedFilter? = null
    private lateinit var viewModel: VendorViewModel

    var state = ""
    var brand = ""
    var city = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_c_s_d_form, container, false)
        binding = DataBindingUtil.bind<FragmentCSDFormBinding>(view)!!




        return binding.root
    }

    fun setupVisibility() {
        when (type) {

            "Car Price" -> {
                getAllVehicleBrandList("Car")

                binding.spinnerState.visibility = View.VISIBLE
                binding.spinnerCity.visibility = View.VISIBLE
                binding.spinnerBrand.visibility = View.VISIBLE
                binding.spinnerModel.visibility = View.VISIBLE

                types = "Car"
            }

            "Bike Price" -> {
                getAllVehicleBrandList("Bike")
                binding.spinnerState.visibility = View.VISIBLE
                binding.spinnerCity.visibility = View.VISIBLE
                binding.spinnerBrand.visibility = View.VISIBLE
                binding.spinnerModel.visibility = View.VISIBLE

                types = "Bike"

            }

            "Dealers" -> {

                binding.spinnerState.visibility = View.VISIBLE
                binding.spinnerCity.visibility = View.VISIBLE
                binding.tvVehicleType.visibility = View.VISIBLE
                binding.spinnerBrand.visibility = View.VISIBLE

                types = "Dealers"

            }

            "Find" -> {
                binding.spinnerState.visibility = View.VISIBLE
                binding.spinnerCity.visibility = View.GONE

                types = "Find"

            }
        }
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupVisibility()
        getAllDepot()
        getAllStateList()


        var state = ModelCustumSpinner(id = "0", name = "Select State")
        stateList.add(state)

        stateAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            stateList, false
        )
        binding.spinnerState.setAdapter(stateAdapter)

        var city = ModelCustumSpinner(id = "0", name = "Select City")
        cityList.add(city)
        cityAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            cityList, false
        )

        binding.spinnerCity.setAdapter(cityAdapter)
        var brand = ModelCustumSpinner(id = "0", name = "Select Company")
        brandList.add(brand)
        brandAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            brandList, false
        )

        binding.spinnerBrand.setAdapter(brandAdapter)
        var model:ModelCustumSpinner
        if (type=="Car Price"){
           model = ModelCustumSpinner(id = "0", name = "Select Car")
        }else{
            model = ModelCustumSpinner(id = "0", name = "Select Bike")
        }

        modelList.add(model)
        modelAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            modelList, false
        )
        binding.spinnerModel.setAdapter(modelAdapter)

/*        var varient = ModelCustumSpinner(id = "0", name = "Select Varient")
        varientList.add(varient)*/
/*
        varientAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            varientList, false
        )
        binding.spinnerVarient.setAdapter(varientAdapter)

*/


        var depot = ModelCustumSpinner(id = "0", name = "Select Depot")
        depotList.add(depot)
        depotAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            depotList, false
        )
        binding.spinnerDepot.setAdapter(depotAdapter)


        var vehicleType = ModelCustumSpinner(id = "0", name = "Choose Car/Bike")
        vehicleTypeList.add(vehicleType)
        vehicleTypeList.add(ModelCustumSpinner(id = "0", name = "Bike"))
        vehicleTypeList.add(ModelCustumSpinner(id = "0", name = "Car"))
        vehicleTypeAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            vehicleTypeList, false
        )
        binding.tvVehicleType.setAdapter(vehicleTypeAdapter)
//        callback.onClickOnGetFormResult(selectedFilter, selectedCity.name)

        binding.tvVehicleType.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedVehicleType = parent!!.getItemAtPosition(position) as ModelCustumSpinner


                getAllVehicleBrandList(selectedVehicleType.name)

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        binding.buttonFormOk.setOnClickListener {

            if (isValid()) {
                binding.progresbar.visibility = View.VISIBLE

                if(type=="Car Price")
                {
                    if (selectedCity.id=="0"){
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
                    }else if (selectedModel.id=="0") {
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select Model", Toast.LENGTH_SHORT).show()
                    }else {
                        selectedFilter = ModelSelectedFilter(
                            state = selectedState.id,
                            city = selectedCity.id,
                            vehicleType = "Car",
                            type =type,
                            company_id = selectedCarCompany.id,
                            vehicle_id = selectedModel.id,
                            depot_id = "",
                            filterType = "csd_dealers",
                            appointmentDate = ""
                        )
                        getVendors(type)

                    }

                }
                else if (type=="Bike Price"){

                    if (selectedCity.id=="0"){
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
                    }else if (selectedModel.id=="0") {
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select Model", Toast.LENGTH_SHORT).show()
                    }else {
                        selectedFilter = ModelSelectedFilter(
                            state = selectedState.id,
                            city = selectedCity.id,
                            vehicleType = "Bike",
                            type = type,
                            company_id = selectedCarCompany.id,
                            vehicle_id = selectedModel.id,
                            depot_id = "",
                            filterType = "csd_dealers",
                            appointmentDate = ""
                        )
                        getVendors(type)

                    }
                }
                else if (type=="Dealers"){

                    if (selectedCity.id=="0"){
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
                    }else if (selectedCarCompany.id=="0"){
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Plase Select Company", Toast.LENGTH_SHORT).show()
                    }else{
                        selectedFilter = ModelSelectedFilter(
                            state = selectedState.id,
                            city = selectedCity.id,
                            vehicleType = selectedVehicleType.name,
                            company_id = selectedCarCompany.id,
                            vehicle_id = "",
                            type = type,
                            depot_id = "",
                            filterType = "csd_dealers",
                            appointmentDate = ""
                        )
                        getVendors(type)

                    }
                }
                else if (type=="Find"){
                    selectedFilter = ModelSelectedFilter(
                        state = selectedState.id,
                        city = "",
                        vehicleType = "",
                        company_id = "",
                        type =type,
                        vehicle_id = "",
                        depot_id = "",
                        filterType = "csd_store",
                        appointmentDate = ""
                    )
                    getVendors(type)
                }


                /*if (type=="Car Price" ){
                  selectedFilter = ModelSelectedFilter(
                        state = selectedState.id,
                        city = selectedCity.id,
                        vehicleType = "Car",
                        type=type,
                        company_id = selectedCarCompany.id,
                        vehicle_id = selectedModel.id,
                        depot_id = "",
                        filterType = "csd_dealers",
                        appointmentDate = ""
                    );
                }
                 else
                    if (type=="Bike Price"){
                        selectedFilter = ModelSelectedFilter(
                            state = selectedState.id,
                            city = selectedCity.id,
                            vehicleType = "Bike",
                            type=type,
                            company_id = selectedCarCompany.id,
                            vehicle_id = selectedModel.id,
                            depot_id = "",
                            filterType = "csd_dealers",
                            appointmentDate = ""
                        );
                    }
                    else if (type=="Dealers"){
                    selectedFilter = ModelSelectedFilter(
                        state = selectedState.id,
                        city = selectedCity.id,
                        vehicleType = selectedVehicleType.name,
                        company_id = selectedCarCompany.id,
                        vehicle_id = "",
                        type=type,
                        depot_id = "",
                        filterType = "csd_dealers",
                        appointmentDate = ""
                    );
                }
                    else  if (type=="Find"){
                    selectedFilter = ModelSelectedFilter(
                        state = selectedState.id,
                        city = "",
                        vehicleType = "",
                        company_id = "",
                        type=type,
                        vehicle_id = "",
                        depot_id = "",
                        filterType = "csd_store",
                        appointmentDate = ""
                    );
                }
*/

               /* if (selectedFilter != null) {
                    if (!this::selectedCity.isInitialized) {

                        callback.onClickOnGetFormResult(selectedFilter!!, "")

                    }else{
                        callback.onClickOnGetFormResult(selectedFilter!!, selectedCity.name)
                    }
                }*/
            }
        }
    }



    public interface Callbackk {
        fun onClickOnGetFormResult(selectedFilter: ModelSelectedFilter,cityName:String);
    }



    fun getAllStateList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().getState(hashMap).enqueue(object : Callback<ModelState?> {
            override fun onResponse(
                call: Call<ModelState?>,
                response: Response<ModelState?>
            ) {
                if (response.body() != null) {

                    for (i in 0 until response.body()!!.data.size) {
                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        stateList.add(model)
                    }

                    binding.spinnerState.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {

                            selectedState = parent!!.getItemAtPosition(position) as ModelCustumSpinner


                            getAllCityList()

                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelState?>, t: Throwable) {

            }
        })
    }

    fun getAllCityList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["state_id"] = selectedState.id

        RestClient.getInst().getCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {

                        var hint = cityList[0]
                        cityList.clear()
                        cityList.add(hint)

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }

                        /*if (response.body()!!.data.isNotEmpty()){
                            Log.d("kajsdhkas","aosdas")
                            selectedCity= ModelCustumSpinner(response.body()!!.data[0].id,response.body()!!.data[0].id)

                        }*/
                        binding.spinnerCity.setSelection(0)
                        cityAdapter.notifyDataSetChanged()
                        if (this@FragmentCSDForm::selectedCarCompany.isInitialized){
                            getAllModelList()
                        }

                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity = parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }

    fun getAllVehicleBrandList(vehicleType: String) {


        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["type"] = vehicleType
        RestClient.getInst().getVehicleBrand(hashMap).enqueue(object : Callback<ModelCompany?> {
            override fun onResponse(
                call: Call<ModelCompany?>,
                response: Response<ModelCompany?>
            ) {
                if (response.body() != null) {

                    var hint = brandList[0]
                    brandList.clear()
                    brandList.add(hint)
                    for (i in 0 until response.body()!!.data.size) {
                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        brandList.add(model)
                    }
                    binding.spinnerBrand.setSelection(0)
                    brandAdapter.notifyDataSetChanged()
                    // Set the custom adapter to the spinner
                    binding.spinnerBrand.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            selectedCarCompany = parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            Log.d("asdsad", selectedCarCompany.toString())


                                getAllModelList()

                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelCompany?>, t: Throwable) {

            }
        })
    }


    fun getAllModelList() {

        if (!this::selectedState.isInitialized){
            Toast.makeText(requireContext(), "please select state", Toast.LENGTH_SHORT).show()
        }else{
            val hashMap = HashMap<String, String>()

            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
            hashMap["state_id"]=selectedState.id
//        hashMap["city_id"]=selectedCity.id
            hashMap["company_id"] = selectedCarCompany.id
            hashMap["type"] = types
            /* hashMap["city"] = selectedCity.id
             hashMap["state"] = selectedState.id*/


            RestClient.getInst().getVehicleModel(hashMap).enqueue(object : Callback<ModelVehicle?> {
                override fun onResponse(
                    call: Call<ModelVehicle?>,
                    response: Response<ModelVehicle?>
                ) {
                    if (response.body() != null) {
                        if (response.body()!!.result) {


                            if (response.body()!!.data.isEmpty()) {
                                binding.spinnerModel.setSelection(0)

                                if (type=="Dealers") {


                                    if (!this@FragmentCSDForm::selectedCity.isInitialized){

                                    }else{
                                        selectedFilter = ModelSelectedFilter(
                                            state = selectedState.id,
                                            city = selectedCity.id,
                                            vehicleType = selectedVehicleType.id,
                                            company_id = selectedCarCompany.id,
                                            vehicle_id = "",
                                            type = type,
                                            depot_id = "",
                                            filterType = "csd_dealers",
                                            appointmentDate = ""
                                        )
                                    }


                                        //   getVendors()
                                }
                                else {
//                                     selectedCarCompany = "" as ModelCustumSpinner
                                     showLogoutDialog("This vehicle is not available through CSD In" + selectedCity.name)
                            }

                            }  else {

                                var hint = modelList[0]
                                modelList.clear()
                                modelList.add(hint)

                                for (i in 0 until response.body()!!.data.size) {
                                    var model = ModelCustumSpinner(
                                        id = response.body()!!.data[i].id,
                                        name = response.body()!!.data[i].vehicle_name
                                    )
                                    modelList.add(model)
                                }

                                binding.spinnerModel.setSelection(0)
                                modelAdapter.notifyDataSetChanged()

                                // Set the custom adapter to the spinner
                                binding.spinnerModel.setOnItemSelectedListener(object :
                                    AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View,
                                        position: Int,
                                        id: Long
                                    ) {
                                        selectedModel = parent!!.getItemAtPosition(position) as ModelCustumSpinner
                                        //  getAllVarientList()
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                                })
                            }

                        }
                    }
                }

                override fun onFailure(call: Call<ModelVehicle?>, t: Throwable) {
                    Toast.makeText(requireContext(), t.message, Toast.LENGTH_LONG).show()
                }
            })
        }


    }


    fun getVendors(type: String) {

        if (type=="Dealers"){

            val hashMap = HashMap<String, String>()
            hashMap.put("token", PrefManager.getInstance(requireContext())!!.userDetail.token)
            this.selectedFilter!!.state?.let { hashMap.put("state_id", it) }
            this.selectedFilter!!.city?.let { hashMap.put("city_id", it) }
            this.selectedFilter!!.vehicleType?.let { hashMap.put("vehicle_type", it) }
            this.selectedFilter!!.company_id?.let { hashMap.put("company_id", it) }
            this.selectedFilter!!.depot_id?.let { hashMap.put("regional_centerID", it) }
            this.selectedFilter!!.appointmentDate?.let { hashMap.put("appointment_date", it) }
            this.selectedFilter!!.vehicle_id?.let { hashMap.put("vehicle_id", it) }
            this.selectedFilter!!.filterType?.let { hashMap.put("type", it) }

            RestClient.getInst().getVendors(hashMap).enqueue(object : Callback<ModelVendor> {
                override fun onResponse(call: Call<ModelVendor>, response: Response<ModelVendor>) {
                    if (response.body()!!.result) {

                        if (response.body()!!.data.isEmpty()) {
                            binding.progresbar.visibility = View.GONE

                            showLogoutDialog("No"+ " "+ selectedCarCompany.name + " " +"CSD dealer found in" + " " +selectedCity.name )

                        } else {

                            if (selectedFilter != null) {
                                if (!this@FragmentCSDForm::selectedCity.isInitialized) {
                                    binding.progresbar.visibility = View.GONE
                                    callback.onClickOnGetFormResult(selectedFilter!!, "")

                                } else {
                                    binding.progresbar.visibility = View.GONE
                                    callback.onClickOnGetFormResult(selectedFilter!!, selectedCity.name
                                    )
                                }
                            }
                        }

                    } else {
                        binding.progresbar.visibility = View.GONE
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<ModelVendor>, t: Throwable) {
                    //   makeToast(t.message)
                }
            })

        }else {

            if (selectedFilter != null) {
                if (!this::selectedCity.isInitialized) {
                    binding.progresbar.visibility = View.GONE
                    callback.onClickOnGetFormResult(selectedFilter!!, "")

                }else{
                    binding.progresbar.visibility = View.GONE
                    callback.onClickOnGetFormResult(selectedFilter!!, selectedCity.name)
                }
            }


        }
    }



    fun getAllVarientList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["state_id"] = selectedState.id
        hashMap["city_id"] = selectedCity.id
        hashMap["type"] = "Car"
        hashMap["company_id"] = selectedCarCompany.id
        hashMap["vehicle_id"] = selectedModel.id

        RestClient.getInst().getVehicleVarient(hashMap).enqueue(object : Callback<ModelVarient?> {
            override fun onResponse(
                call: Call<ModelVarient?>,
                response: Response<ModelVarient?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {


                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                         //   varientList.add(model)
                        }
                        // varientAdapter.notifyDataSetChanged()

                        // Set the custom adapter to the spinner
                        binding.spinnerVarient.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedVarient =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelVarient?>, t: Throwable) {

            }
        })
    }


    fun getAllDepot() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        RestClient.getInst().getDepot(hashMap).enqueue(object : Callback<ModelDepot?> {


            override fun onFailure(call: Call<ModelDepot?>, t: Throwable) {

            }

            override fun onResponse(call: Call<ModelDepot?>, response: Response<ModelDepot?>) {
                if (response.body() != null) {
                    if (response.body()!!.result) {

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            depotList.add(model)
                        }
                        // varientAdapter.notifyDataSetChanged()

                        // Set the custom adapter to the spinner
                        binding.spinnerDepot.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedDepot =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }
        })
    }


    fun isValid(): Boolean {
        Log.d("liuhfldv",type)
        if (type =="Find") {
            if (!this::selectedState.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
                return false
            }
        }


        if (type == "Car Price" || type == "Bike Price") {

            if (!this::selectedState.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
                return false
            }

            if (!this::selectedCity.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
                return false
            }

            if (!this::selectedCarCompany.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select Company", Toast.LENGTH_SHORT).show()
                return false
            }

            if (!this::selectedModel.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select Model", Toast.LENGTH_SHORT).show()
                return false
            }
        }

        if (type == "Dealers") {

            if (!this::selectedState.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
                return false
            }

            if (!this::selectedCity.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
                return false
            }

            if (selectedVehicleType.name=="Choose Car/Bike")
            {
                Toast.makeText(requireContext(), "Plase Select Car/Bike", Toast.LENGTH_SHORT).show()
                return false

            }

            if (!this::selectedCarCompany.isInitialized) {
                Toast.makeText(requireContext(), "Plase Select Company", Toast.LENGTH_SHORT).show()
                return false
            }

        }

        return true
    }

    fun showLogoutDialog(masage : String) {
        val builder = AlertDialog.Builder(requireContext())
        //set title for alert dialog
        builder.setMessage(masage)

        stateAdapter.notifyDataSetChanged()
        cityAdapter.notifyDataSetChanged()
        brandAdapter.notifyDataSetChanged()
        modelAdapter.notifyDataSetChanged()
        //set message for alert dialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.show()
    }


    override fun onResume() {
        super.onResume()
//        ModelSelectedFilter(null,null,null,null,null,null,null,null,null)



    }


}