package com.user.foujiadda.fragment.csd

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentVehicleResultBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.models.spins.*
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.wedguruphotographer.adapter.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap


class FragmentVehicleResult(var selectedFilter: ModelSelectedFilter, var cityName: String) :
    Fragment() {
    lateinit var binding: FragmentVehicleResultBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                //   activity!!.supportFragmentManager.popBackStack()
                /*    if(shouldInterceptBackPress()){
                        // in here you can do logic when backPress is clicked
                    }else{
                        isEnabled = false
                        activity?.onBackPressed()
                    }*/
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_vehicle_result, container, false)
        binding = DataBindingUtil.bind<FragmentVehicleResultBinding>(view)!!

        binding.layout.setOnClickListener {
            return@setOnClickListener
        }
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getVehicleDetails()
        binding.buttonFindDealer.text = "Find Dealer in " + cityName
        binding.buttonFindDealer.setOnClickListener {

            if (PrefManager.getInstance(requireContext())!!.userDetail.token=="skip"){

            val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(requireContext()))
            val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
            dialog.setCancelable(false)
            dialog.setView(dialogBinding.root)

            dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                val intent = Intent(requireContext(), ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                requireContext().startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
            }

            dialogBinding.signup.setOnClickListener {
                val intent = Intent(requireContext(), ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                requireContext().startActivity(intent)
            }
            dialog.show()

        } else{

            startActivity(
                IntentHelper.getVendorActivity(requireContext()).putExtra("data", selectedFilter)
            )
        }

        }


    }


    fun getVehicleDetails() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["depot_id"] = selectedFilter.depot_id!!
        hashMap["model_id"] = "";
        hashMap["state_id"] = selectedFilter.state!!
        hashMap["city_id"] = selectedFilter.city!!
        hashMap["type"] = selectedFilter.vehicleType!!
        hashMap["company_id"] = selectedFilter.company_id!!
        hashMap["vehicle_id"] = selectedFilter.vehicle_id!!


        RestClient.getInst().vehicle_detail(hashMap)
            .enqueue(object : Callback<ModelVehicleDetails?> {
                override fun onResponse(
                    call: Call<ModelVehicleDetails?>,
                    response: Response<ModelVehicleDetails?>
                ) {
                    if (response.body() != null) {
                        try {
                            if (response.body()!!.data != null) {
                                var vehicleDetails = response.body()!!.data
                                binding.tvTitle.text = vehicleDetails.vehicle_name + " price in " + cityName
                                binding.tvPriceValue.text = vehicleDetails.price
                                binding.tvModelNameValue.text =  vehicleDetails.model
                                Glide.with(context!!).load(vehicleDetails.image)
                                    .into(binding.ivIMage)
                                binding.tvIndexNumberVlue.text = vehicleDetails.index_number
                                binding.layoutData.visibility = View.VISIBLE
                                binding.layoutNoData.visibility = View.GONE
                            } else {
                                binding.layoutData.visibility = View.GONE
                                binding.layoutNoData.visibility = View.VISIBLE
                            }
                        } catch (e: Exception) {
                        }
                    }
                }

                override fun onFailure(call: Call<ModelVehicleDetails?>, t: Throwable) {

                }
            })
    }


}