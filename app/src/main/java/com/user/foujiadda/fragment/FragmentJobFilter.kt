package com.user.foujiadda.fragment


import JobFilterAdapter
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentJobFilterBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.models.spins.ModelCity
import com.user.foujiadda.models.spins.ModelState
import com.user.foujiadda.networking.RestClient
import com.wedguruphotographer.adapter.CustumSpinAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentJobFilter() : Fragment() {


    private lateinit var cityAdapter: CustumSpinAdapter
    private lateinit var selectedState: ModelCustumSpinner
    private lateinit var selectedCity: ModelCustumSpinner


    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()


    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var regionalAdapter: CustumSpinAdapter


    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""
    private lateinit var binding: FragmentJobFilterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_job_filter, container, false)
        binding = DataBindingUtil.bind<FragmentJobFilterBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAllStateList()
        getFilters()

        var state = ModelCustumSpinner(id = "0", name = "Select State")
        stateList.add(state)
        stateAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            stateList, false
        )
        binding.spinnerState.setAdapter(stateAdapter)


        var city = ModelCustumSpinner(id = "0", name = "Select City")
        cityList.add(city)
        cityAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            cityList, false
        )
        binding.spinnerCity.setAdapter(cityAdapter)


    }


    fun getAllStateList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().getState(hashMap).enqueue(object : Callback<ModelState?> {
            override fun onResponse(
                call: Call<ModelState?>,
                response: Response<ModelState?>
            ) {
                if (response.body() != null) {

                    for (i in 0 until response.body()!!.data.size) {
                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        stateList.add(model)
                    }





                    binding.spinnerState.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            selectedState =
                                parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            getAllCityList()
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelState?>, t: Throwable) {

            }
        })
    }

    fun getAllCityList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["state_id"] = selectedState.id

        RestClient.getInst().getCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {


                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }
                        cityAdapter.notifyDataSetChanged()



                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner


                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }

    fun getFilters() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token



        RestClient.getInst().getJobFilters(hashMap).enqueue(object : Callback<ModelJobFilter?> {
            override fun onResponse(
                call: Call<ModelJobFilter?>,
                response: Response<ModelJobFilter?>
            ) {
                if (response.body() != null) {
                    var educationFilterList = ArrayList<JobFilterData>()
                    var salaryFilterList = ArrayList<JobFilterData>()
                    for (i in response.body()!!.data.education) {
                        educationFilterList.add(i)
                    }

                    var educationFilterAdapater = JobFilterAdapter(
                        context!!,
                        educationFilterList,
                        "education",
                        object : JobFilterAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {

                            }
                        })

                    binding.rvEducation.adapter = educationFilterAdapater
                    binding.rvEducation.layoutManager =
                        LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)


                    for (i in response.body()!!.data.salary) {
                        salaryFilterList.add(i)
                    }

                    var salaryFilterAdapter = JobFilterAdapter(
                        context!!,
                        salaryFilterList,
                        "salary",
                        object : JobFilterAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {

                            }
                        })

                    binding.rvSalary.adapter = salaryFilterAdapter
                    binding.rvSalary.layoutManager =
                        LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)


                }
            }

            override fun onFailure(call: Call<ModelJobFilter?>, t: Throwable) {

            }
        })
    }

    public interface Callbackk {
        fun onClickOnApplyJobFilter()
    }
}



