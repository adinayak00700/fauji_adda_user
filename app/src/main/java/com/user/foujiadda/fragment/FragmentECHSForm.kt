package com.user.foujiadda.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment

import com.user.foujiadda.databinding.FragmentEchsFormBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.models.spins.ModelCity
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.CustomDatePicker
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.PriceFormatter
import com.wedguruphotographer.adapter.CustumSpinAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentECHSForm(var type: String) : BaseFragment() {

var selectedAppoutmentDate=""
    private lateinit var cityAdapter: CustumSpinAdapter

    private lateinit var selectedCity: ModelCustumSpinner
    private lateinit var selectedCenter: ModelCustumSpinner

    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val regionalDataList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()


    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var regionalAdapter: CustumSpinAdapter

    private lateinit var dateselect : String

    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""
    private lateinit var binding: FragmentEchsFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_echs_form, container, false)
        binding = DataBindingUtil.bind<FragmentEchsFormBinding>(view)!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAllRegionalData()


        var city = ModelCustumSpinner(id = "0", name = "Select City")
        cityList.add(city)
        cityAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            cityList, false
        )
        binding.spinnerCity.setAdapter(cityAdapter)


        var regionalData = ModelCustumSpinner(id = "0", name = "Select Region")
        regionalDataList.add(regionalData)
        regionalAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            regionalDataList, false
        )
        binding.spinnerCenter.setAdapter(regionalAdapter)


        if (type=="echs_empanelled"){

           binding.tvDate.visibility=View.VISIBLE

        }else{
            binding.tvDate.visibility=View.GONE
            binding.spinnerCity.visibility =View.GONE
        }

        binding.buttonOk.setOnClickListener {
            var selectedFilter:ModelSelectedFilter?=null


            if (isValid()) {

                    if (type == "echs_empanelled") {

                        if (!this::selectedCity.isInitialized) {
                            Toast.makeText(requireContext(), "please Select City", Toast.LENGTH_SHORT).show()

                        }else if (selectedCity.id == "0"){
                            Toast.makeText(requireContext(), "please Select City", Toast.LENGTH_SHORT).show()
                        }else if (selectedAppoutmentDate.isNotEmpty()) {
                            Log.d("asdasdasd","hjkhujl;'khjk")
                            selectedFilter = ModelSelectedFilter(
                                state = "",
                                city = selectedCity.id,
                                vehicleType = "",
                                company_id = "",
                                vehicle_id = "",
                                type = type,
                                depot_id = selectedCenter.id,
                                filterType = "echs_empanelled",
                                appointmentDate = selectedAppoutmentDate
                            )

                            startActivity(
                                IntentHelper.getVendorActivity(requireContext())
                                    .putExtra("data", selectedFilter)
                            )

                        } else {

                            makeToast("select date")

                        }

                        Log.d("asdasdasd","hjkhujl;'khjk")
                    } else {

                        selectedFilter = ModelSelectedFilter(
                            state = "",
                            city = "",
                            vehicleType = "",
                            company_id = "",
                            vehicle_id = "",
                            type = type,
                            depot_id = selectedCenter.id,
                            filterType = "echs_polyclinic",
                            appointmentDate = ""
                        )

                        startActivity(
                            IntentHelper.getVendorActivity(requireContext())
                                .putExtra("data", selectedFilter)
                        )


                    }

            }
        }


    binding.tvDate.setOnClickListener {
            CustomDatePicker.showPicker(requireContext(),object :CustomDatePicker.Callbackk{
                override fun onGetDate(date: String?) {
                    binding.tvDate.text=date

                    dateselect = date.toString()
                    selectedAppoutmentDate=PriceFormatter.ymdFormatfromDmy(date)
                }
            })
        }
    }


    fun getAllCityList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["center_id"] = selectedCenter.id

        RestClient.getInst().getHospitalCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {

                        var hint = cityList[0]
                        cityList.clear()
                        cityList.add(hint)

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }

                        binding.spinnerCity.setSelection(0)
                        cityAdapter.notifyDataSetChanged()



                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity = parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }


    fun getAllRegionalData() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
/*        hashMap["type"] =type
        hashMap["city_id"] =selectedCity.id
        hashMap["state_id"] =selectedState.id*/
        RestClient.getInst().getRegionalData(hashMap)
            .enqueue(object : Callback<ModelRegionalData?> {
                override fun onResponse(
                    call: Call<ModelRegionalData?>,
                    response: Response<ModelRegionalData?>
                ) {
                    if (response.body() != null) {

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            regionalDataList.add(model)
                        }
                        binding.spinnerCenter.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCenter =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner
                                getAllCityList()

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }

                override fun onFailure(call: Call<ModelRegionalData?>, t: Throwable) {

                }
            })
    }

    fun isValid(): Boolean {

        if (!this::selectedCenter.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
            return false
        }




        return true
    }

}