package com.user.foujiadda.fragment

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.datingapp.application.App
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.facebook.*
import com.facebook.CallbackManager.Factory.create
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentLoginBinding
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.models.UserDetails
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.NetworkUtils
import com.user.foujiadda.utilities.ValidationHelper
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentLogin.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentLogin : BaseFragment() {
    private var param1: String? = null
    private var param2: String? = null
    var mGoogleSignInClient: GoogleSignInClient? = null
    var callbackManager: CallbackManager? = null
    private val RC_SIGN_IN = 100


private lateinit var  binding:FragmentLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view=inflater.inflate(R.layout.fragment_login, container, false)
        binding=DataBindingUtil.bind<FragmentLoginBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpGoogleSignIn()
        binding.ivGooglreView16.setOnClickListener {

            if (isNetworkAvailable()){
                openGoogleSignIn()
            }else{
                Toast.makeText(requireContext(), "No internet Connection Found", Toast.LENGTH_LONG).show()
            }



        }
        binding.ivfacebook.setOnClickListener {
            openFacebookLogin()
        }



        val bundle = this.arguments
        if (bundle!= null){
            binding.etEmailAddress.setText(bundle.getString("amount"))
        }



        binding.tvSkip.setOnClickListener {
            var user= UserDetails("","","","","","skip","","", "","","","","","","")
            PrefManager.getInstance(requireActivity())!!.userDetail=user
            startActivity(IntentHelper.getDashboardActivity(requireActivity()))
        }

        binding.tvForgotPasswortd.setOnClickListener {
            findNavController().navigate(R.id.forgetPasswordFragment)
        }

        binding.buttonSignIn.setOnClickListener {
        if (isValid()){
            loginApi()
        }

        }
        binding.tvCreateNewAccount.setOnClickListener {
            findNavController().navigate(R.id.fragmentLoginToidFragmentSignup)
        }
        printHashKey(requireContext())
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FragmentLogin().apply {
                arguments = Bundle().apply {

                }
            }
    }

    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }

    fun loginApi() {
        val hashMap = HashMap<String, String>()
        hashMap["email"] =binding.etEmailAddress.text.toString()
        hashMap["password"] =binding.etPassword.text.toString()
        hashMap["social_id"] = ""
        hashMap["device_id"] = ""
        hashMap["device_token"] = App.getInstance().firebaseToken.toString()
        hashMap["device_type"] = "android"
        RestClient.getInst().userLogin(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
            Log.d("asdasdasd",response.body()!!.result.toString())
                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                    PrefManager.getInstance(requireContext())!!.keyIsLoggedIn = true
                    startActivity(IntentHelper.getDashboardActivity(context))
                    activity!!.finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

    fun printHashKey(pContext: Context) {
        try {
            val info = pContext.packageManager.getPackageInfo(
                pContext.packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                //Log.d("asdfahjds", hashKey)
            }
        } catch (e: NoSuchAlgorithmException) {
        } catch (e: Exception) {
        }
    }


    fun setUpGoogleSignIn() {
       val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

     /*   val gso: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("AIzaSyChwzUiZOIaQ_zGnGaQX8B6QuR0Aw-D9Hg")
                .requestEmail()
                .build()*/
        callbackManager = null
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

    }

    private fun openGoogleSignIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (callbackManager != null) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)

//         Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listenerr.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val account = task.getResult(ApiException::class.java)
            if (account != null) {
                socialLoginApi("google", account.id!!, account.givenName!!, account.familyName!!, account.email!!)
            }
        }

        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }



    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {

            val account = completedTask.getResult(ApiException::class.java)

        Log.d("google", account.toString())
            makeToast("Signed in successfully")
            socialLoginApi("google", account.id!!, account.givenName!!, account.familyName!!, account.email!!)
            // Signed in successfully, show authenticated UI.
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
    }

    private fun updateUIGoogleSignIn(account: GoogleSignInAccount?) {}


    fun openFacebookLogin() {
        setupFacebookLogin()
    }

    fun setupFacebookLogin() {


        callbackManager = create()

        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }

            override fun onSuccess(result: LoginResult) {
                makeToast("FB Signed in Successfully")
                getFbInfo()
            }
        })



        var isLoggedIn: Boolean = false
        try {
            val accessToken: AccessToken = AccessToken.getCurrentAccessToken()!!

            isLoggedIn = accessToken != null && !accessToken.isExpired
        } catch (e: Exception) {

        }
        if (isLoggedIn) {
           socialLoginApi("fb", Profile.getCurrentProfile()!!.id!!, "", "", "")
        } else {
            LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList(
                    "public_profile",
                    "email",
                    "user_gender",
                    "user_birthday"
                )
            )
        }
    }


    private fun getFbInfo() {
        val request: GraphRequest = GraphRequest.newMeRequest(
            AccessToken.getCurrentAccessToken(),
            object : GraphRequest.GraphJSONObjectCallback {

                override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                    try {
                        Log.d("asdljlgiybvf", response.toString())
                        val id = `object`!!.getString("id")
                        val first_name = `object`.getString("first_name")
                        val last_name = `object`.getString("last_name")
                        /*   val image_url = "http://graph.facebook.com/$id/picture?type=large"*/
                        var email: String = `object`.getString("email")
                        Log.d("asdkjasd", "asdasd")
                       socialLoginApi(
                            "fb",
                            Profile.getCurrentProfile()!!.id!!,
                            Profile.getCurrentProfile()!!.firstName!!,
                            Profile.getCurrentProfile()!!.lastName!!,
                            email
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("asdasdfgg", e.message.toString())
                    }
                }
            })
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id,first_name,last_name,email,gender,birthday"
        ) // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
        request.parameters=parameters
        request.executeAsync()
    }

    fun socialLoginApi(

        type: String?,
        socialId: String,
        f_name: String,
        l_name: String,
        email: String
    ) {

        val hashMap = java.util.HashMap<String, String>()
        val deviceId = Settings.Secure.ANDROID_ID
        val deviceToken: String = ""
       hashMap[Constants.kDeviceId] = deviceId
        hashMap[Constants.kDeviceToken] = deviceToken

        hashMap["first_name"] = f_name
        hashMap["last_name"] = l_name
        hashMap["email"] = email
        hashMap["password"] = ""
        hashMap["social_id"] = socialId
        hashMap["device_id"] = ""
        hashMap["device_token"] = App.getInstance().firebaseToken.toString()
        hashMap["device_type"] = "android"
        RestClient.getInst().userLogin(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                if (response.body()!!.result) {
                    val userDetails = response.body()
                    PrefManager.getInstance(context!!)!!.userDetail=userDetails!!.data
                    PrefManager.getInstance(context!!)!!.keyIsLoggedIn=true
                    startActivity(IntentHelper.getDashboardActivity(context))
                    activity!!.finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {

                makeToast(t.message)
            }
        })
    }


    fun isValid():Boolean{
       if (ValidationHelper.isNull(binding.etEmailAddress.text.toString())){
            makeToast("Please Enter Email")
            return false;
        }
        else   if (!isValidEmail(binding.etEmailAddress.text.toString())){
            makeToast("Please Enter Valid Email")
            return false;
        }
        else   if (ValidationHelper.isNull(binding.etPassword.text.toString())){
            makeToast("Please Enter Password")
            return false;
        }
        return  true

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }



/*    public fun setMobileNumber(number:String){
        binding!!.etMobile.setText(number)
    }*/


}