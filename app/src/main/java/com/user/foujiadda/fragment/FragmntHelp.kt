package com.user.foujiadda.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.user.foujiadda.MainActivity
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentHelpBinding
import com.user.foujiadda.utilities.NetworkUtils


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentHelp : Fragment(){
    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""
    private lateinit var binding: FragmentHelpBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_help, container, false)
        binding = DataBindingUtil.bind<FragmentHelpBinding>(view)!!

        binding.layout.setOnClickListener {
            return@setOnClickListener
        }
//        val u: Uri = Uri.parse("tel:" + "8814838485")
        binding.callImage1.setOnClickListener {

            val contact = "+918814838485" // use country code with your phone number

            val url = "https://api.whatsapp.com/send?phone=$contact"
            try {
                val pm = requireActivity().packageManager
                pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            } catch (e: NameNotFoundException) {
                Toast.makeText(
                    requireContext(),
                    "Whatsapp app not installed in your phone",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }


        }



        binding.emailImage1.setOnClickListener {

            val intent = Intent(Intent.ACTION_SEND)
            val recipients = arrayOf("help@foujiadda.com")
            intent.putExtra(Intent.EXTRA_EMAIL, recipients)
            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject text here...")
            intent.putExtra(Intent.EXTRA_TEXT, "Body of the content here...")
            intent.putExtra(Intent.EXTRA_CC, "mailcc@gmail.com")
            intent.type = "text/html"
            intent.setPackage("com.google.android.gm")
            startActivity(Intent.createChooser(intent, "Send mail"))

        }

        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



    }

}