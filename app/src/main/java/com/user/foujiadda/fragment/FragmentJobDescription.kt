package com.user.foujiadda.fragment

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentJobDescriptionBinding
import com.user.foujiadda.models.JobData
import com.user.foujiadda.models.ModelJob
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentJobDescription(var job: JobData) : BaseFragment() {
    private  lateinit var  binding:FragmentJobDescriptionBinding
    var isDoubleClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_job_description, container, false)
        binding=DataBindingUtil.bind<FragmentJobDescriptionBinding>(view!!)!!
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvAge.text="Age "+job.age+" years"
        binding.tvQua.text=job.qualification
        binding.tvLocaton.text=job.location
        binding.tvSalaryValue.text="INR "+job.salary+"/"+job.salary_type
        binding.tvNoofPost.text=job.posts+" Posts"
        binding.tvRole.text=job.title
        binding.tvTitle.text=job.title
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.tvJobDescriptionValue.text=(Html.fromHtml(job.description, Html.FROM_HTML_MODE_COMPACT));
        }else{
            binding.tvJobDescriptionValue.text=(Html.fromHtml(job.description));
        }

        if (job.job_link.isEmpty()){

        }else{
            binding.toast.visibility = View.GONE
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.tvEligibilityValue.text=(Html.fromHtml(job.eligibilityValue, Html.FROM_HTML_MODE_COMPACT));
        } else {
            binding.tvEligibilityValue.text=(Html.fromHtml(job.eligibilityValue));
        }


        if (job.type == "Pvt") {

            if (job.is_apply == "Yes") {
                binding.buttonApply.isClickable = false
                binding.buttonApply.setText("Already Apply")
                binding.toast.visibility = View.VISIBLE
            } else {

                binding.buttonApply.visibility = View.VISIBLE
            }
        }


        binding.buttonApply.setOnClickListener {
            addLead(job.id)
            getJobDetails(job.id)

            if (job.type == "Pvt") {
                if (isDoubleClicked) {
                    binding.buttonApply.isClickable = false
                } else {
                    binding.buttonApply.isClickable = false
                }
            }
        }
    }

    private fun getJobDetails(jobApplyId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["job_id"] = jobApplyId


        RestClient.getInst().jobAPply(hashMap).enqueue(object : Callback<ModelJob> {
            override fun onResponse(call: Call<ModelJob>, response: Response<ModelJob>) {

                if (response.body()!!.result) {
                    binding.buttonApply.setText("Already Apply")
                    binding.toast.visibility = View.VISIBLE
                }

            }

            override fun onFailure(call: Call<ModelJob>, t: Throwable) {
                Toast.makeText(context, "response.message()" , Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun addLead(typeId: String) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(requireContext())!!.userDetail.token)
        hashMap.put("type", "job")
        hashMap.put("type_id",typeId)
        hashMap.put("city_id", "")
        hashMap.put("appointment_date", "")
        hashMap.put("state_id", "")

        RestClient.getInst().addLead(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                  try {
                          val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(job.job_link))
                          startActivity(browserIntent)

                  }catch (e:Exception){

                  }
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }




}