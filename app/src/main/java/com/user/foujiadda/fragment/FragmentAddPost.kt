package com.wedguruphotographer.fragments

import SelectedImageAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.datingapp.application.App
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentAddFeedBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.NetworkUtils
import com.wedguruphotographer.model.ModelGalleryUpload

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.*
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class FragmentAddFeed(var callbackk:Callbackk) : BaseFragment(), View.OnClickListener, SelectedImageAdapter.Callbackk {
    private val SELECT_PICTURES = 101
    var PICK_IMAGE_MULTIPLE = 1
    var imageEncoded: String? = null
    var imagesEncodedList: List<String>? = null
    var inputStream: InputStream? = null
    lateinit var galleryUploadAdapter:SelectedImageAdapter
    var imagesModel = ArrayList<ModelGalleryUpload>()
    lateinit var binding: FragmentAddFeedBinding
    private var categoryId: String? = null


    override
    fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_add_feed, container, false)
        binding = DataBindingUtil.bind(view)!!
        return view
    }

    override
    fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProfile()
        binding.recyclerView.setLayoutManager(GridLayoutManager(context, 3))
        binding.buttonAdd.setOnClickListener(this)
        binding.buttonSave.setOnClickListener(this)
        galleryUploadAdapter = SelectedImageAdapter(requireContext(), imagesModel, this)
        binding.recyclerView.adapter=galleryUploadAdapter
        binding.recyclerView.layoutManager=GridLayoutManager(context,3)

    }

    fun selectImages() {
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURES)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                if (data!!.clipData != null) {
                    val count =
                        data!!.clipData!!.itemCount //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                    for (i in 0 until count) {
                        val imageUri = data!!.clipData!!.getItemAt(i).uri
                        try {
                            Log.d("asdasdasdasd", getFilePathFromUri(imageUri))
                            galleryUploadAdapter.add(
                                ModelGalleryUpload(
                                    getFilePathFromUri(imageUri),
                                ), true
                            )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                } else if (data.data != null) {
                    try {
                        galleryUploadAdapter.add(
                            ModelGalleryUpload(
                                getFilePathFromUri(data.data),
                            ), true
                        )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }


    @Throws(IOException::class)
    fun getFilePathFromUri(uri: Uri?): String {
        try {
            inputStream = requireActivity().getContentResolver().openInputStream(uri!!)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        val fileName = getFileName(uri)
        val file: File = File(
            requireActivity().getCacheDir().getAbsolutePath().toString() + "/" + fileName
        )
        writeFile(inputStream, file)
        return file.absolutePath
    }

    fun writeFile(`in`: InputStream?, file: File?) {
        var out: OutputStream? = null
        try {
            out = FileOutputStream(file)
            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                out?.close()
                `in`!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    override fun onClick(v: View) {
        when (v.id) {
            R.id.buttonAdd -> selectImages()
            R.id.buttonSave -> addPhotosApi()
        }


    }
    fun addPhotosApi() {

        RestClient.getInst()
            .addPost(PrefManager.getInstance(requireContext())!!.userDetail.token,binding.etText.text.toString(),
        galleryUploadAdapter.newSelectedPhotos,
        object : Callback<ModelSuccess> {
            override fun onResponse(
                call: Call<ModelSuccess>,
                response: Response<ModelSuccess>
            ) {
                if (response.body()!!.result) {

                    callbackk.onPostAdded()
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

    public  interface  Callbackk{
        fun onPostAdded()
    }


    companion object {
        private const val SELECT_PICTURES = 101
        const val OPEN_MEDIA_PICKER = 1
        fun getFileName(uri: Uri?): String {
            var fileName = getFileNameFromCursor(uri)
            if (fileName == null) {
                val fileExtension = getFileExtension(uri)
                fileName = "temp_file" + if (fileExtension != null) ".$fileExtension" else ""
            } else if (!fileName.contains(".")) {
                val fileExtension = getFileExtension(uri)
                fileName = "$fileName.$fileExtension"
            }
            return fileName
        }

        fun getFileExtension(uri: Uri?): String? {
            val fileType: String = App.getInstance().getContentResolver().getType(uri!!)!!
            return MimeTypeMap.getSingleton().getExtensionFromMimeType(fileType)
        }

        fun getFileNameFromCursor(uri: Uri?): String? {
            val fileCursor: Cursor = App.getInstance().getContentResolver()
                .query(uri!!, arrayOf(OpenableColumns.DISPLAY_NAME), null, null, null)!!
            var fileName: String? = null
            if (fileCursor != null && fileCursor.moveToFirst()) {
                val cIndex = fileCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (cIndex != -1) {
                    fileName = fileCursor.getString(cIndex)
                }
            }
            return fileName
        }
    }

    init {
        this.categoryId = categoryId
    }

    override fun onClickOnCategory(catId: String) {

    }


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token

        if (isNetworkAvailable()) {
            RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
                override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                    dismissLoader()

                    if (response.body()!!.result) {

                        if (response.body()!!.data.token == "skip") {
                            val dialogBinding =
                                EmptyLayoutBinding.inflate(LayoutInflater.from(context))
                            val dialog = MaterialAlertDialogBuilder(
                                requireContext(),
                                R.style.MyThemeOverlayAlertDialog
                            ).create()
                            dialog.setCancelable(false)
                            dialog.setView(dialogBinding.root)

                            dialogBinding.pleaseLogin.text =
                                "Please Login / Sign up to update your post"

                            dialogBinding.login.setOnClickListener {
                                val intent = Intent(requireContext(), ActivityLogin::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                requireContext().startActivity(intent)
                            }

                            dialogBinding.signup.setOnClickListener {
                                val intent = Intent(requireContext(), ActivityLogin::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                requireContext().startActivity(intent)
                            }

                            dialogBinding.close.setOnClickListener {
                                startActivity(IntentHelper.getDashboardActivity(context))
                            }

                            dialog.show()
                        } else {
                            try {
                                Glide.with(context!!).load(response.body()!!.data.avtaar)
                                    .circleCrop().into(binding.ivUserImage)

                            } catch (e: Exception) {
                            }
                            if (response.body()!!.data.img_permission == "1") {
                                binding.buttonAdd.visibility = View.VISIBLE
                            }
                        }

                    }

                }

                override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                    dismissLoader()
                  /*  makeToast(getString(R.string.went_wrong_msg))
                    makeToast(t.message)*/
                }
            })
        }else{
            Toast.makeText(requireContext(), "No internet Connection Found", Toast.LENGTH_LONG).show()
        }
    }

    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }

}