package com.user.foujiadda.fragment

import CommentAdapter
import FeedAdapter
import JobAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.EmptyLayoutBinding

import com.user.foujiadda.databinding.FragmentMCQuotaBinding
import com.user.foujiadda.factory.CommentViewModelFactory
import com.user.foujiadda.factory.FeedViewModelFactory
import com.user.foujiadda.factory.JobViewModelFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.spins.ModelCity

import com.user.foujiadda.models.spins.ModelState


import com.user.foujiadda.models.viewmodel.CommentViewModel
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.JobViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.CustomDatePicker
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.PriceFormatter
import com.user.foujiadda.utilities.ValidationHelper
import com.wedguruphotographer.adapter.CustumSpinAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentMCQuota(var callbackk:Callbackk) : BaseFragment() {

    var selectedAppoutmentDate=""
    private lateinit var mcAdapter: CustumSpinAdapter

    private lateinit var selectedMCO: ModelCustumSpinner
    private lateinit var selectedStation: ModelCustumSpinner
    private lateinit var selectedTrain: ModelCustumSpinner
    val mcList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val trainList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val stationList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    lateinit var selectedData:Train


    private lateinit var regionalAdapter: CustumSpinAdapter
    private lateinit var trainAdapter: CustumSpinAdapter

    private lateinit var binding: FragmentMCQuotaBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_m_c_quota, container, false)
        binding = DataBindingUtil.bind<FragmentMCQuotaBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAllMCList()


        var mco = ModelCustumSpinner(id = "0", name = "Select MCO")
        mcList.add(mco)
        mcAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            mcList, false
        )
        binding.spinnermcq.setAdapter(mcAdapter)


        var station = ModelCustumSpinner(id = "0", name = "From Station")
        stationList.add(station)
        regionalAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            stationList, false
        )
        binding.spinnerFrom.setAdapter(regionalAdapter)

        var train = ModelCustumSpinner(id = "0", name = "Select Train")
        trainList.add(train)
        trainAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            trainList, false
        )
        binding.spinnerTrain.setAdapter(trainAdapter)

        binding.buttonOk.setOnClickListener {

            if (isValid()) {

                if (PrefManager.getInstance(requireContext())!!.userDetail.token=="skip"){

                    val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(requireContext()))
                    val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
                    dialog.setCancelable(false)
                    dialog.setView(dialogBinding.root)

                    dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                        val intent = Intent(requireContext(), ActivityLogin::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        requireContext().startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
                    }

                    dialogBinding.signup.setOnClickListener {
                        val intent = Intent(requireContext(), ActivityLogin::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        requireContext().startActivity(intent)
                    }
                    dialog.show()

                } else{

                    if (selectedStation.id == "0"){
                        Toast.makeText(requireContext(), "please select station", Toast.LENGTH_SHORT).show()
                    }else if (selectedTrain.id == "0"){
                        Toast.makeText(requireContext(), "please select train", Toast.LENGTH_SHORT).show()

                    }else {


                        callbackk.onClickOnSearchTrain(selectedData)
                    }
                }


            }
        }
    }

    fun getAllMCList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token


        RestClient.getInst().getMCLIST(hashMap).enqueue(object : Callback<ModelMCQ?> {
            override fun onResponse(
                call: Call<ModelMCQ?>,
                response: Response<ModelMCQ?>
            ) {
                if (response.body() != null) {
                  if (response.body()!!.result) {
                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            mcList.add(model)
                        }
                        mcAdapter.notifyDataSetChanged()

                        binding.spinnermcq.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedMCO =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner

                                    getAllstation()
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelMCQ?>, t: Throwable) {

            }
        })
    }


    fun getAllstation() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["mco_id"] =selectedMCO.id
        hashMap["search"]=""
        RestClient.getInst().getStationList(hashMap)
            .enqueue(object : Callback<ModelStation?> {
                override fun onResponse(
                    call: Call<ModelStation?>,
                    response: Response<ModelStation?>
                ) {
                    if (response.body() != null) {

                        stationList.clear()
                        var station = ModelCustumSpinner(id = "0", name = "From Station")
                        selectedStation=station
                        stationList.add(station)
                        regionalAdapter.notifyDataSetChanged()


                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            stationList.add(model)
                        }

                        binding.spinnerFrom.setSelection(0)

                        binding.spinnerFrom.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedStation =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner
                             if (selectedStation.id!="0"){
                                 getAllTrains()
                             }

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }

                override fun onFailure(call: Call<ModelStation?>, t: Throwable) {

                }
            })
    }

    fun getAllTrains() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["mco_id"] =selectedMCO.id
        hashMap["station_id"] =selectedStation.id
        hashMap["search"]=""
        RestClient.getInst().getTrainList(hashMap)
            .enqueue(object : Callback<ModelTrain?> {
                override fun onResponse(
                    call: Call<ModelTrain?>,
                    response: Response<ModelTrain?>
                ) {
                    if (response.body() != null) {
                        trainList.clear()
                        var station = ModelCustumSpinner(id = "0", name = "Select Train")
                        selectedTrain=station
                        trainList.add(station)
                        trainAdapter.notifyDataSetChanged()
                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].train_name
                            )
                            trainList.add(model)
                        }
                        binding.spinnerTrain.setSelection(0)

                        binding.spinnerTrain.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedTrain = parent!!.getItemAtPosition(position) as ModelCustumSpinner


                            if (position>0){
                                selectedData=response.body()!!.data[position-1]
                            }

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }

                override fun onFailure(call: Call<ModelTrain?>, t: Throwable) {

                }
            })
    }

    public interface Callbackk{
        fun onClickOnSearchTrain(train :Train)
    }



    fun isValid(): Boolean {

        if (!this::selectedMCO.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select MCO", Toast.LENGTH_SHORT).show()
            return false
        }else

        if (!this::selectedStation.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select Station", Toast.LENGTH_SHORT).show()
            return false
        }else
            if (!this::selectedData.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select Train", Toast.LENGTH_SHORT).show()
            return false
        }



        return true
    }
}