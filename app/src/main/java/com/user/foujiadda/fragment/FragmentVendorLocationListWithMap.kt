package com.user.foujiadda.fragment

import VendorAdapter
import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.callbacks.AddressCallbacks
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentVendorLocationListWithMapBinding
import com.user.foujiadda.enums.AppMarker
import com.user.foujiadda.factory.VendorViewModelFactory
import com.user.foujiadda.map.AddressManager
import com.user.foujiadda.map.DashboardMapManager
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.models.ModelVendor
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.models.viewmodel.VendorViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentVendorLocationListWithMap(var selectedFilter: ModelSelectedFilter?) : Fragment(),
    AddressCallbacks, OnMapReadyCallback,
    VendorAdapter.Callbackk {
    private lateinit var vendorAdapter: VendorAdapter
    private lateinit var viewModel: VendorViewModel
    private lateinit var binding: FragmentVendorLocationListWithMapBinding
    private var page : Int  = 1
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var pastVisiblesItems: Int = 0
    private var lastpastVisiblesItems: Int = 0
    private var PageSize: Int = 0
    private var isPagination : Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view =
            inflater.inflate(R.layout.fragment_vendor_location_list_with_map, container, false)
        binding = DataBindingUtil.bind<FragmentVendorLocationListWithMapBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("asdasdasd","Gson().toJson(selectedFilter)")
        Log.d("asdasdasd",Gson().toJson(selectedFilter))

        binding.progressBar.visibility = View.VISIBLE

        binding.rvVendors.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isPagination){
                    if (dy > 0) {
                        visibleItemCount = (binding.rvVendors.layoutManager as LinearLayoutManager).childCount
                        totalItemCount = (binding.rvVendors.layoutManager as LinearLayoutManager).itemCount
                        pastVisiblesItems = (binding.rvVendors.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                        lastpastVisiblesItems = (binding.rvVendors.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount){
                            isPagination = false;
                            page += 1
                            getVendors()

                        }
                    }
                }
            }
        })


        if (PrefManager.getInstance(requireContext())!!.userDetail.token=="skip"){

            val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(requireContext()))
            val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
            dialog.setCancelable(false)
            dialog.setView(dialogBinding.root)

            dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                val intent = Intent(requireContext(), ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                requireContext().startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
            }

            dialogBinding.signup.setOnClickListener {
                val intent = Intent(requireContext(), ActivityLogin::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                requireContext().startActivity(intent)
            }
            dialog.show()

        } else{

            initialiseMap()
            val factory = VendorViewModelFactory()
            binding.rvVendors.layoutManager = LinearLayoutManager(requireContext())
            viewModel = ViewModelProviders.of(this, factory).get(VendorViewModel::class.java)
            vendorAdapter = VendorAdapter(viewModel, requireContext(), this)

            viewModel.livedatalist.observe(viewLifecycleOwner, Observer {
                vendorAdapter.addItem()
            })
            binding.rvVendors.adapter = vendorAdapter
        }
    }

    private lateinit var googleMap: GoogleMap

    // private lateinit var deviceLocationManager: DeviceLocationManager
    private lateinit var mapManager: DashboardMapManager
    private lateinit var addressManager: AddressManager
    private lateinit var mapFragment: SupportMapFragment

    @SuppressLint("ClickableViewAccessibility")
    private fun initialiseMap() {
        addressManager = AddressManager()
        addressManager.setCallbacks(this)
        setUpMap()
        getVendors()
        binding.transparentImageview.setOnTouchListener(View.OnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    // Disallow ScrollView to intercept touch events.
                   /* binding.nestedScrollView.requestDisallowInterceptTouchEvent(true)*/
                    // Disable touch on transparent view
                    false
                }
                MotionEvent.ACTION_UP -> {
                    // Allow ScrollView to intercept touch events.
                    /*binding.nestedScrollView.requestDisallowInterceptTouchEvent(false)*/
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    /*binding.nestedScrollView.requestDisallowInterceptTouchEvent(true)*/
                    false
                }
                else -> true
            }
        })

    }

    fun setUpMap() {
        mapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0
        googleMap.getUiSettings().setTiltGesturesEnabled(false)
        googleMap.getUiSettings().setRotateGesturesEnabled(false)
        googleMap.setMinZoomPreference(Constants.MAP_MIN_ZOOM)
        googleMap.setMaxZoomPreference(Constants.MAP_MAX_ZOOM)
        googleMap.getUiSettings().setIndoorLevelPickerEnabled(false)
        googleMap.getUiSettings().setMyLocationButtonEnabled(false)
        googleMap.getUiSettings().setMapToolbarEnabled(false)
        googleMap.getUiSettings().setCompassEnabled(true)
        googleMap.getUiSettings().setZoomControlsEnabled(false)
        mapManager = DashboardMapManager()
        mapManager.registerMap(googleMap)


        googleMap.setOnInfoWindowClickListener {

            try {
                /*       val markerData: ModelNearbyVendor.DataBean = Gson().fromJson(it!!.tag.toString(),
                           ModelNearbyVendor.DataBean::class.java)
                       startActivity(
                           IntentHelper.getServiceScreen(requireContext()).putExtra(
                               Constants.serviceCatId,"0").putExtra("type","vendorDetails").putExtra(
                               Constants.vendorid,markerData.id).putExtra(
                               Constants.model_id,markerData.model_id).putExtra(
                               Constants.business_name,markerData.business_name).putExtra(
                               Constants.business_name,markerData.business_name))
       */
            } catch (e: Exception) {

            }


        }
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            /*           val success: Boolean = googleMap.setMapStyle(
                           MapStyleOptions.loadRawResourceStyle(
                               context!!, R.raw.map_dark_style
                           )
                       )*/
            /*       if (!success) {
                       Log.e("MapsActivityRaw", "Style parsing failed.")
                   }*/
        } catch (e: Resources.NotFoundException) {
            Log.e("MapsActivityRaw", "Can't find style.", e)
        }


    }


    override fun onPickUpAddressFound(pickAddress: String?) {
    }

    override fun onDropAddressFound(dropAddress: String?) {
    }

    fun getVendors() {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(requireContext())!!.userDetail.token)
        selectedFilter!!.state?.let { hashMap.put("state_id", it) }
        selectedFilter!!.city?.let { hashMap.put("city_id", it) }
        selectedFilter!!.vehicleType?.let { hashMap.put("vehicle_type", it) }
        selectedFilter!!.company_id?.let { hashMap.put("company_id", it) }
        selectedFilter!!.depot_id?.let { hashMap.put("regional_centerID", it) }
        selectedFilter!!.appointmentDate?.let { hashMap.put("appointment_date", it) }
        selectedFilter!!.vehicle_id?.let { hashMap.put("vehicle_id", it) }
        selectedFilter!!.filterType?.let { hashMap.put("type", it) }
        hashMap["pages"] = page.toString()

        RestClient.getInst().getVendors(hashMap).enqueue(object : Callback<ModelVendor> {
            override fun onResponse(call: Call<ModelVendor>, response: Response<ModelVendor>) {
                if (response.body()!!.result) {
                    isPagination = response.body()!!.total_pages >= PageSize.toString()
                    PageSize = response.body()!!.total_pages.toInt()

                    if (response.body()!!.data.isEmpty()){
                        binding.layoutNoData.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                    }else {
                        binding.progressBar.visibility = View.GONE
                        binding.layoutNoData.visibility = View.GONE
                        viewModel.add(response.body()!!.data as ArrayList<Vendor>)
                        showVendorsOnMap(response.body()!!.data as MutableList<Vendor>)
                    }

                   try {
                       mapManager.moveToSelectedAddressType(AppMarker.PICK, LatLng(response.body()!!.data[0].lat.toDouble(), response.body()!!.data[0].lng.toDouble()))

                   } catch (e:java.lang.Exception){

                   }
                }
            }

            override fun onFailure(call: Call<ModelVendor>, t: Throwable) {
//                Toast.makeText(context, "No list Found", Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onClickOnVendor(vendor: Vendor) {
        startActivity(IntentHelper.getVendorDetailsScreen(context).putExtra("id",vendor.id).putExtra("type",vendor.vendorType).putExtra("date",selectedFilter!!.appointmentDate));
    }

    private fun showVendorsOnMap(vendors: MutableList<Vendor>) {
        for (i in 0 until vendors.size) {
            mapManager.drawVendorMarkerOnMap(
                vendors[i],
            )
        }
    }
}