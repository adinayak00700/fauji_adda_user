package com.user.foujiadda.fragment

import CommentAdapter
import FeedAdapter
import JobAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mechanicforyou.user.utilities.FilterDialog
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentJobListBinding
import com.user.foujiadda.factory.CommentViewModelFactory
import com.user.foujiadda.factory.FeedViewModelFactory
import com.user.foujiadda.factory.JobViewModelFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.viewmodel.CommentViewModel
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.JobViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentJobList(var type:String,var callback:Callbackk, states: String, citys: String, educations : String, salarys: String ) : Fragment(), JobAdapter.Callbackk{
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""
    private lateinit var binding: FragmentJobListBinding

    var state = states
    var city = citys
    var educations = educations
    var salarys = salarys
    private var page : Int  = 1
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var pastVisiblesItems: Int = 0
    private var lastpastVisiblesItems: Int = 0
    private var PageSize: Int = 0
    private var isPagination : Boolean = false

  /*  var state : String = ""
    var city : String = ""
    var educations : String = ""
    var salarys : String = ""*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_job_list, container, false)
        binding = DataBindingUtil.bind<FragmentJobListBinding>(view)!!
        getJobs()



        if (state.isEmpty())
        {
            state = ""
        }

        if (city.isEmpty()){
            city = ""
        }

        if (educations.isEmpty()){
            educations = ""
        }

        if (salarys.isEmpty()){
            salarys = ""
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initJobAdapter()


        binding.rvFeeds.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isPagination){
                    if (dy > 0) {
                        visibleItemCount = (binding.rvFeeds.layoutManager as LinearLayoutManager).childCount
                        totalItemCount = (binding.rvFeeds.layoutManager as LinearLayoutManager).itemCount
                        pastVisiblesItems = (binding.rvFeeds.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                        lastpastVisiblesItems = (binding.rvFeeds.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount){
                            isPagination = false;
                            page += 1
                            getJobs()

                        }
                    }
                }
            }
        })
 
    }

    override fun onResume() {
        super.onResume()
        initJobAdapter()
        getJobs()
    }


    private lateinit var viewModel: JobViewModel
    private lateinit var jobAdapter: JobAdapter
    fun initJobAdapter() {
        binding.rvFeeds.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val factory = JobViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(JobViewModel::class.java)
        jobAdapter = JobAdapter(viewModel, requireContext(), this)

        viewModel.livedatalist.observe(viewLifecycleOwner, Observer {
//            jobAdapter.submitList(it)
            jobAdapter.addItem()

            /*jobAdapter.list.addAll(it)*/

        })
        binding.rvFeeds.adapter = jobAdapter
    }

    fun getJobs() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["salary"] = salarys
        hashMap["education"] = educations
        hashMap["type"] =type
        hashMap["state_id"] = state
        hashMap["city_id"] = city
        hashMap["pages"] = page.toString()

        RestClient.getInst().getJobs(hashMap).enqueue(object : Callback<ModelJob> {
            override fun onResponse(call: Call<ModelJob>, response: Response<ModelJob>) {try {
                if (response.body()!!.result) {
                    Log.d("shdasda", "popopoo")

                    isPagination = response.body()!!.total_pages >= PageSize.toString()
                    viewModel.add(response.body()!!.data as ArrayList<JobData>)

                    binding.rvFeeds.adapter?.notifyDataSetChanged()
                    PageSize = response.body()!!.total_pages.toInt()

                }

            }catch (e:Exception){
                val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(context))
                val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
                dialog.setCancelable(false)
                dialog.setView(dialogBinding.root)
                dialogBinding.pleaseLogin.text = "Please Login / Sign up to check jobs"

                dialogBinding.login.setOnClickListener {
//
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
                }

                dialogBinding.signup.setOnClickListener {
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
                }

                dialogBinding.close.setOnClickListener {
                    startActivity(IntentHelper.getDashboardActivity(context))
                }

                dialog.show()

            }
            }

            override fun onFailure(call: Call<ModelJob>, t: Throwable) {

            }
        })
    }


    public  interface Callbackk {
        fun onClickOnJob(job:JobData)
    }

    override fun onClickOnJob(job: JobData) {
      //  callback.onClickOnJob(job)


        startActivity(IntentHelper.getJobDetailsScreen(requireContext()).putExtra("data",job))
    }

    public fun onCLickONApplyButton(
        selecteState: String,
        selecteCity: String,
        education: String,
        salary: String
    ) {
        state = selecteState
        city = selecteCity
        educations = education
        salarys = salary

        getJobs()

    }


}