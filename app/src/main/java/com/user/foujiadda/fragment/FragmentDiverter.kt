package com.user.foujiadda.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentDiverterBinding
import com.user.foujiadda.models.UserDetails
import com.user.foujiadda.utilities.IntentHelper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentDiverter.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentDiverter : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
private  lateinit var  binding:FragmentDiverterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        var view=inflater.inflate(R.layout.fragment_diverter, container, false)
        binding=DataBindingUtil.bind<FragmentDiverterBinding>(view)!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentDiverter.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentDiverter().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonLogin.setOnClickListener {
            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
        }
        binding.buttonCreateAccount.setOnClickListener {
            findNavController().navigate(R.id.fragmentDiverterToidFragmentSignup)
        }

        binding.tvSkip.setOnClickListener {
            var user= UserDetails("","","","","","skip","","", "","","","","","","")
            PrefManager.getInstance(requireActivity())!!.userDetail=user
            startActivity(IntentHelper.getDashboardActivity(requireActivity()))
        }
    }
}