package user.faujiadda.fragment

import NotificationAdapter
import OfferAdapter
import TransactionAdapter
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentOffeListBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap


class FragmentOfferList(var type: String) : Fragment(), OfferAdapter.CallbackkOffer{

    private lateinit var binding: FragmentOffeListBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.bind<FragmentOffeListBinding>(
            inflater.inflate(
                R.layout.fragment_offe_list,
                container,
                false
            )
        )!!
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       getOffers()
    }



    fun getOffers() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["search"] = ""




        RestClient.getInst().getOffers(hashMap).enqueue(object : Callback<ModelOffers> {
            override fun onResponse(call: Call<ModelOffers>, response: Response<ModelOffers>) {
                try {
                   if (response.body()!!.result) {
                        Log.d("shdasda", "popopoo")

                        var adapter = OfferAdapter(context!!, this@FragmentOfferList)
                        binding.rvNotifications.adapter = adapter
                        binding.rvNotifications.layoutManager = LinearLayoutManager(context!!)
                        adapter.addTolIst(response.body()!!.data as ArrayList<Offer>)

                    } else {

                    }


                } catch (e: Exception) {
                  //  startActivity(IntentHelper.getLoginScreen(activity!!))
                }


            }

            override fun onFailure(call: Call<ModelOffers>, t: Throwable) {

            }
        })
    }

    override fun onClickOnApplyCoupan( id: String) {
        addLead( id)
    }

    fun addLead( id: String) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(requireContext())!!.userDetail.token)
        hashMap.put("type", "offer")
        hashMap.put("type_id", id)
        hashMap.put("city_id", "")
        hashMap.put("appointment_date", "")
        hashMap.put("state_id", "")


        RestClient.getInst().addLead(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                } else {
                    // makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }


}