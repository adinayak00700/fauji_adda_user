package com.user.foujiadda.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentFollowUsBinding


class FragmentFollowUs : Fragment(){

    private var postId = ""
    private lateinit var binding: FragmentFollowUsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_follow_us, container, false)
        binding = DataBindingUtil.bind<FragmentFollowUsBinding>(view)!!





        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivFB.setOnClickListener {
            try {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/foujiadda"))
                startActivity(browserIntent)
            } catch (e: Exception) {

            }
        }

        binding.ivTwitter.setOnClickListener {
            try {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/fouji_adda"))
                startActivity(browserIntent)
            } catch (e: Exception) {

            }
        }

        binding.ivINsta.setOnClickListener {
            try {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/foujiadda"))
                startActivity(browserIntent)
            } catch (e: Exception) {

            }
        }

        binding.ivYputube.setOnClickListener {
            try {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/c/foujiadda"))
                startActivity(browserIntent)
            } catch (e: Exception) {

            }
        }

    }
}