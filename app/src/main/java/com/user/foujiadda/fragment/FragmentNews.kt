package com.user.foujiadda.fragment

import NewsAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentNewsBinding
import com.user.foujiadda.factory.NewsViewModelFactory
import com.user.foujiadda.models.ModelNews
import com.user.foujiadda.models.News
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentNews(var type:String, var searchKey : String) : Fragment() {
    private lateinit var binding: FragmentNewsBinding
    private var page : Int  = 1
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var pastVisiblesItems: Int = 0
    private var lastpastVisiblesItems: Int = 0
    private var PageSize: Int = 0
    private var isPagination : Boolean = false
    private lateinit var viewModel: NewsViewModel
    private lateinit var newsAdapter: NewsAdapter
    private var filters : String = ""
    private var searchs : String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_news, container, false)
        binding = DataBindingUtil.bind<FragmentNewsBinding>(view)!!
        getNews("")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNewsAdapter()
        binding.rvList.addOnScrollListener(object : RecyclerView.OnScrollListener(){

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                    if (dy > 0) {
                        visibleItemCount = (binding.rvList.layoutManager as LinearLayoutManager).childCount
                        totalItemCount = (binding.rvList.layoutManager as LinearLayoutManager).itemCount
                        pastVisiblesItems = (binding.rvList.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                        if (isPagination) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                isPagination = false;
                                page += 1
                                getNews("")
                            }
                        }

               }
            }
        })
    }

    fun initNewsAdapter() {
        binding.rvList.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val factory = NewsViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(NewsViewModel::class.java)
        newsAdapter = NewsAdapter(viewModel, requireContext())
        viewModel.livedatalist.observe(viewLifecycleOwner, Observer {
            Log.d("alskdjasda", it.size.toString())
            newsAdapter.addItem()
        })
        binding.rvList.adapter = newsAdapter
    }


    fun getNews(filter:String) {
        if(filter.isNotEmpty()){
            page = 1
            filters = filter
        }

        if (searchKey.isNotEmpty()){
            page = 1
            searchs = searchKey
        }

        binding.scrollProgress.visibility = View.VISIBLE
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["news_categoryID"] = filters
        hashMap["type"] = type
        hashMap["news_id"] = ""
        hashMap["is_home"] = ""
        hashMap["search"] = searchs
        hashMap["pages"] = page.toString()

        RestClient.getInst().getNews(hashMap).enqueue(object : Callback<ModelNews> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<ModelNews>, response: Response<ModelNews>) {
                if (response.body()!!.result) {

                    binding.rvList.visibility = View.VISIBLE

                    if (filter.isNotEmpty()){

                        binding.scrollProgress.visibility = View.GONE
                        isPagination = response.body()!!.total_pages > page.toString()
                        viewModel.clear()
                        viewModel.add(response.body()!!.data as ArrayList<News>)
                        PageSize = response.body()!!.total_pages.toInt()
                        Log.d("akcja", PageSize.toString())

                    } else if (searchs.isNotEmpty()){
                        binding.scrollProgress.visibility = View.GONE
                        isPagination = response.body()!!.total_pages > page.toString()
                        viewModel.clear()
                        viewModel.add(response.body()!!.data as ArrayList<News>)
                        PageSize = response.body()!!.total_pages.toInt()
                        Log.d("akcja", PageSize.toString())
                    }else{


                        binding.scrollProgress.visibility = View.GONE
                        isPagination = response.body()!!.total_pages > page.toString()
                        viewModel.add(response.body()!!.data as ArrayList<News>)
                        PageSize = response.body()!!.total_pages.toInt()
                        Log.d("akcja", PageSize.toString())
                    }
                }
            }

            override fun onFailure(call: Call<ModelNews>, t: Throwable) {
                binding.scrollProgress.visibility = View.GONE
            }
        })
    }
}