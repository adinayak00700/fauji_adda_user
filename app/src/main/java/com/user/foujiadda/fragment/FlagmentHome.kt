package user.faujiadda.fragment


import CategoryAdapter
import NewsRecyclerAdapter
import SliderAdapterExample
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cabuser.utilities.DeviceLocationManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.facebook.FacebookSdk
import com.google.android.gms.maps.model.LatLng
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.user.foujiadda.BuildConfig
import com.user.foujiadda.R
import com.user.foujiadda.databinding.DialogueupdateBinding
import com.user.foujiadda.databinding.FragmentFlagmentHomeBinding
import com.user.foujiadda.factory.NewsViewModelFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.viewmodel.NewsViewModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.NetworkUtils
import com.user.foujiadda.utilities.ValidationHelper
import okhttp3.internal.notify
import okhttp3.internal.notifyAll
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FlagmentHome : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var lat : Float? = null
    private var long : Float? = null

    private lateinit var binding: FragmentFlagmentHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

//    private  var currentLocation=LatLng(lat!!.toDouble(),long!!.toDouble())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_flagment_home, container, false)
        binding = DataBindingUtil.bind<FragmentFlagmentHomeBinding>(view)!!

        return binding.root

    }
    lateinit var deviceLocationManager:DeviceLocationManager


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (isNetworkAvailable()){

            deviceLocationManager=DeviceLocationManager(context = requireActivity(),object :DeviceLocationManager.Callbackk{
                override fun onGettingCurrentLocation(latitude: Double, longitude: Double) {

                    lat = latitude.toFloat()
                    long = longitude.toFloat()

                    if (PrefManager.getInstance(requireActivity())!!.userDetail.token == "skip"){

                    }else{
                        updateCurrentLocation()
                    }

                }
            })
            deviceLocationManager.getCurrentLocationn()

            initNewsAdapter()
            getBanner()
            getCategories()
            getNews()
//            getVendors()
            iniRefreshListener()

        }else{
            iniRefreshListener()
            binding.mainParent.visibility =View.GONE
            binding.noInternet.visibility =View.VISIBLE
            binding.refresh.setRefreshing(false)

        }
    }


    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }

    companion object {

        @JvmStatic
        fun newInstance(callbackkk: Callbackk) =
            FlagmentHome().apply {
                arguments = Bundle().apply {
                }
            }
    }

    lateinit var callback: Callbackk


    fun iniRefreshListener() {

        binding.refresh.setOnRefreshListener {

            if (isNetworkAvailable()){
                binding.mainParent.visibility =View.VISIBLE
                binding.noInternet.visibility =View.GONE

                isNetworkAvailable()
                // You can make your API call here,
                val handler = Handler()
                handler.postDelayed(Runnable {
                    if (binding.refresh.isRefreshing()) {
                        binding.refresh.isRefreshing = false
                    }
                }, 3000)

                initNewsAdapter()
                getBanner()
                getCategories()
                getNews()
//                getVendors()



            }else {
                binding.mainParent.visibility =View.GONE
                binding.noInternet.visibility =View.VISIBLE
                binding.refresh.isRefreshing = false
            }
        }

    }


    fun getCategories() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        binding.rvCategory.layoutManager = GridLayoutManager(requireContext(), 3)
        RestClient.getInst().getCategories(hashMap).enqueue(object : Callback<ModelCategory> {
            override fun onResponse(call: Call<ModelCategory>, response: Response<ModelCategory>) {
                if (response.body()!!.result) {
                    var catAdapter = CategoryAdapter(requireContext(),
                        response.body()!!.data as ArrayList<Category>,
                        object : CategoryAdapter.Callbackk {


                            override fun onClickOnCategory(catId: String) {


                                if (catId == "5") {
                                    startActivity(
                                        IntentHelper.getJobScreen(context!!)
                                            .putExtra("categoryId", catId)
                                    )

                                } else if (catId == "2") {
                                    startActivity(
                                        IntentHelper.getECHSScreen(context!!)
                                    )
                                } else if (catId == "3") {
                                    startActivity(
                                        IntentHelper.getTravelScreen(context!!)
                                    )
                                }

                                else if (catId == "4") {
                                    startActivity(
                                        IntentHelper.getNewsActivity(context!!)
                                    )
                                }
                                else if (catId == "6") {
                                    startActivity(
                                        IntentHelper.getOfferScreen(context!!)
                                    )
                                }

                                else {
                                    startActivity(
                                        IntentHelper.getCSDScreen(context!!)
                                            .putExtra("categoryId", catId)
                                    )
                                }
                            }
                        })
                    binding.rvCategory.adapter = catAdapter
                }
            }

            override fun onFailure(call: Call<ModelCategory>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }

    private lateinit var viewModel: NewsViewModel
    private lateinit var newsAdapter: NewsRecyclerAdapter
    fun initNewsAdapter() {

        binding.rvProducts.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val factory = NewsViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(NewsViewModel::class.java)
        viewModel.livedatalist.observe(viewLifecycleOwner, Observer {
            Log.d("alskdjasda", it.size.toString())
            newsAdapter = NewsRecyclerAdapter(viewModel, it, requireContext())
            binding.rvProducts.adapter = newsAdapter
            newsAdapter.notifyDataSetChanged()
        })

/*        binding.rvProducts.setLayoutManager(object : LinearLayoutManager(context!!) {
            override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                // force height of viewHolder here, this will override layout_height from xml
                lp.width = width/2
                return true
            }
        })*/


    }


    fun getNews() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["type"] = ""
        hashMap["news_categoryID"] = ""
        hashMap["news_id"] = ""
        hashMap["is_home"] = "Yes"
        hashMap["search"] = ""

        RestClient.getInst().getNews(hashMap).enqueue(object : Callback<ModelNews> {
            override fun onResponse(call: Call<ModelNews>, response: Response<ModelNews>) {
                if (response.body()!!.result) {
                    Log.d("shdasda", "popopoo")
                    viewModel.add(response.body()!!.data as ArrayList<News>)
                    binding.rvProducts.adapter?.notifyDataSetChanged()

                } else {
                }
            }
            override fun onFailure(call: Call<ModelNews>, t: Throwable) {
            }
        })
    }

    fun getBanner() {

        val hashMap = HashMap<String, String>()

        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token

        val urls: MutableList<String> = ArrayList()

        var slider = SliderAdapterExample(requireContext())
        slider.notifyDataSetChanged()

        RestClient.getInst().getBanners(hashMap).enqueue(object : Callback<ModelBanner> {
            override fun onResponse(call: Call<ModelBanner>, response: Response<ModelBanner>) {

                if (response.body()!!.result) {
                    for (i in response.body()!!.data.indices) {
                        urls.add(response.body()!!.data[i].banner_img)
                    }

                    slider.addItem(urls as ArrayList<String>)
                    binding.imageSlider.setSliderAdapter(slider);
                    binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                    binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                    binding.imageSlider.startAutoCycle();



                slider.addListener(object :SliderAdapterExample.Callbackk{
                    override fun onCLickONBanner(position: Int) {

                        try{
                            if (response.body()!!.data[position].service == "CSD"){
                                startActivity(
                                    IntentHelper.getCSDScreen(context!!)
                                        .putExtra("categoryId", "1")
                                )
                            } else if (response.body()!!.data[position].service == "ECHS"){

                                    startActivity(
                                        IntentHelper.getECHSScreen(context!!)
                                            .putExtra("categoryId", "1")
                                    )
                                }else  if (response.body()!!.data[position].service == "TRAVEL"){


                                        startActivity(
                                            IntentHelper.getTravelScreen(context!!)
                                                .putExtra("categoryId", "1")
                                        )
                                    }else if (response.body()!!.data[position].service == "NEWS"){


                                            startActivity(
                                                IntentHelper.getNewsActivity(context!!)
                                                    .putExtra("categoryId", "1")
                                            )
                                        }else if (response.body()!!.data[position].service == "JOBS"){


                                                startActivity(
                                                    IntentHelper.getJobScreen(context!!)
                                                        .putExtra("categoryId", "1")
                                                )
                                            }else  if (response.body()!!.data[position].service == "OFFERS"){

                                                    startActivity(
                                                        IntentHelper.getOfferScreen(context!!)
                                                            .putExtra("categoryId", "1")
                                                    )
                                                }else{
                                                        val browserIntent = Intent(
                                                            Intent.ACTION_VIEW,
                                                            Uri.parse(response.body()!!.data[position].link)
                                                        )
                                                        startActivity(browserIntent)
                            }
                        }catch (e: Exception){
                            Toast.makeText(requireContext(), "No Link found", Toast.LENGTH_LONG).show()
                        }
                    }
                })

                }

            }

            override fun onFailure(call: Call<ModelBanner>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }

    public interface Callbackk {
        fun onClickOnHamburgerIcon()
    }
/*
    fun getVendors() {

        RestClient.getInst().getupdate().enqueue(object :
            Callback<UpdateModelResponse> {

            override fun onResponse(
                call: Call<UpdateModelResponse>,
                response: Response<UpdateModelResponse>
            ) {
                if (response.body()!!.result) {
                    val currentersionCode: Int = BuildConfig.VERSION_CODE
                    if (response.body()!!.data.android_man_int.toInt() > currentersionCode) {//force update
                        var dialogUpdate = DialogUpdate( true, context!!)
                        dialogUpdate.show()

                    } else if (response.body()!!.data.android_cur_int.toInt() > currentersionCode) {//normal update
                        var dialogUpdate = DialogUpdate(false,context!!)
                        dialogUpdate.show()
                    }

                }
            }

            override fun onFailure(call: Call<UpdateModelResponse>, t: Throwable) {

            }
        })
    }*/

    class DialogUpdate(var isForceUpdate:Boolean ,var context: Context) {
        private var dialog: Dialog? = null
        private lateinit var binding: DialogueupdateBinding
        var isDialogShowing = false


        fun show() {
            dialog = Dialog(context ,R.style.MyCustomDialogTheme)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(FacebookSdk.getApplicationContext()).inflate(R.layout.dialogueupdate, null)
            binding= DataBindingUtil.bind(view)!!;
            dialog!!.setContentView(binding.root);
            dialog!!.setCancelable(false)
            if (dialog!!.window != null) {
                dialog!!.window!!.setBackgroundDrawable(null)
                dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog!!.window!!.setLayout(getWidth(FacebookSdk.getApplicationContext()) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)
                binding.tvTitle.text="Update"

                dialog!!.window!!.setDimAmount(0.5f)
                //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            }

            binding.buttonOk.setOnClickListener {

                try {
                    context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))
                } catch (e: ActivityNotFoundException) {
                    context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.user.foujiadda")))
                }

            }
            binding.buttonCancel.setOnClickListener {
                dismiss()
            }
            if (isForceUpdate){
                binding.tvSubTitle.text="App is outdated. Please update this app first"
                binding.buttonCancel.visibility=View.GONE
            }else{
                binding.tvSubTitle.text="App is outdated. Please update this app for new features"
            }
            dialog!!.show()
            isDialogShowing = true
        }

        fun dismiss() {
            isDialogShowing = false
            dialog!!.dismiss()
        }
        /*public interface Callbackk{
            fun onClickOnForceUpdate()
        }*/
        companion object {
            fun getWidth(context: Context): Int {
                val displayMetrics = DisplayMetrics()
                val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                windowmanager.defaultDisplay.getMetrics(displayMetrics)
                return displayMetrics.widthPixels
            }
        }
    }


    fun updateCurrentLocation() {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", PrefManager.getInstance(requireContext())!!.userDetail.token)
        hashMap.put("lat", lat.toString())
        hashMap.put("lng", long.toString())



        RestClient.getInst().updateLocation(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    try {

                    }catch (e:Exception){

                    }



                } else {

                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }
}