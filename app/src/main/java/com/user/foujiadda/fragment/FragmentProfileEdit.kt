package user.faujiadda.fragment

import DialogFragmentLoader
import FragmentVerification
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.canhub.cropper.CropImage
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mechanicforyou.user.utilities.AvtaarDialog
import com.user.foujiadda.MainActivity
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityDashboard
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.activity.ActivityMobileNumber
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentProfileEditBinding

import com.user.foujiadda.fragment.FragmentPasswordReset

import com.user.foujiadda.models.ModelCustumSpinner

import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.models.spins.ModelCity
import com.user.foujiadda.models.spins.ModelState
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.NetworkUtils
import com.wedguruphotographer.adapter.CustumSpinAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


import java.io.File
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentProfileEdit() : BaseFragment() {
    private lateinit var binding: FragmentProfileEditBinding

    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var cityAdapter: CustumSpinAdapter


    private lateinit var selectedState: ModelCustumSpinner
    private lateinit var selectedCity: ModelCustumSpinner


    var state1 = ""
    var state2 = ""

    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()

    private var param1: String? = null
    private var param2: String? = null
    lateinit var fragmentLoader: DialogFragmentLoader
    val c = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onResume() {
        super.onResume()
        binding.etDOB.text = SimpleDateFormat("dd.MM.yyyy").format(System.currentTimeMillis())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_profile_edit, container, false)
        binding = DataBindingUtil.bind<FragmentProfileEditBinding>(view)!!
        binding.etDOB.text = SimpleDateFormat("dd.MM.yyyy").format(System.currentTimeMillis())

            var cal = Calendar.getInstance()

            val dateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, monthOfYear)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    val myFormat = "dd.MM.yyyy" // mention the format you need
                    val sdf = SimpleDateFormat(myFormat, Locale.US)
                    binding.etDOB.text = sdf.format(cal.time)

                }

        // Display Selected date in textbox
        binding.etDOB.setOnClickListener {
            DatePickerDialog(
                requireContext(), dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        var state = ModelCustumSpinner(id = "0", name = "Select State")
        stateList.add(state)

        getAllStateList()

        stateAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            stateList, false
        )
        binding.spinnerState.setAdapter(stateAdapter)

        var city = ModelCustumSpinner(id = "0", name = "Select City")

        cityList.add(city)

        cityAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            cityList, false
        )
        binding.spinnerCity.setAdapter(cityAdapter)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        getProfile()

        selectedCity = ModelCustumSpinner("0", "Select City")
        selectedState = ModelCustumSpinner("0", "Select State")
        binding.ivUserImage.setOnClickListener {


            var avtarDialog = AvtaarDialog(requireContext(), object : AvtaarDialog.Callbackk {

                override fun onAvataarChanged() {
                    getProfile()
                }

            })
            avtarDialog.show()
        }


        /*binding.login.setOnClickListener {
            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
        }
        binding.signup.setOnClickListener {
            findNavController().navigate(R.id.fragmentDiverterToidFragmentSignup)
        }
*/

        binding.buttonSaveChanges.setOnClickListener {
                editProfileApi()
        }

        binding.tvUpdateNumber.setOnClickListener {
         startActivityForResult(IntentHelper.getNumberSvreen(requireContext()),112)
            val intent = Intent(requireContext(), ActivityMobileNumber::class.java)

            intent.putExtra("requestCode", "122")
            intent.putExtra("email", binding.etEmail.text.toString())
            startActivity(intent)

        }

        binding.tvChangePassword.setOnClickListener {

            fragmentLoader = DialogFragmentLoader(FragmentPasswordReset(object :
                FragmentPasswordReset.Callbackk {
                override fun onPasswordChangedSuccessfully() {
                    fragmentLoader.dismiss()
                }
            }), "Change Password")


            fragmentLoader.show(childFragmentManager, "asd")
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==112 && resultCode== Activity.RESULT_OK){
           binding.etPhone.setText(data!!.getStringExtra("mobile"))
           editProfileApi()
        }
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentProfileEdit().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }



    fun editProfileApi() {

        RestClient.getInst().etProfileApi(
            PrefManager.getInstance(requireContext())!!.userDetail.token,
            binding.etFirstName.text.toString(),
            binding.etLastName.text.toString(),
            binding.etEmail.text.toString(),
            binding.etDOB.text.toString(),
            binding.etAbout.text.toString(),
            selectedState.id,
            selectedCity.id,
            object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                        getProfile()
                        showLoader()
                        makeToast(getString(R.string.profile_update_success_msg))
                    }
                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                }
            })
    }

    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token

        if (isNetworkAvailable()) {
            RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
                override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                    dismissLoader()
                    if (response.body()!!.result) {

                        if (response.body()!!.data.token == "skip") {

                            val dialogBinding =
                                EmptyLayoutBinding.inflate(LayoutInflater.from(context))
                            val dialog = MaterialAlertDialogBuilder(
                                requireContext(),
                                R.style.MyThemeOverlayAlertDialog
                            ).create()
                            dialog.setCancelable(false)
                            dialog.setView(dialogBinding.root)

                            dialogBinding.login.setOnClickListener {
                                val intent = Intent(requireContext(), ActivityLogin::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                requireContext().startActivity(intent)
                            }

                            dialogBinding.signup.setOnClickListener {
                                val intent = Intent(requireContext(), ActivityLogin::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                requireContext().startActivity(intent)
                            }

                            dialogBinding.close.setOnClickListener {
                                startActivity(IntentHelper.getDashboardActivity(context))
                            }

                            dialog.show()


                        } else {
                            PrefManager.getInstance(requireContext())!!.userDetail =
                                response.body()!!.data
                            updateProfile()
                            state1 = response.body()!!.data.city_id


                        }


                    } else {
                        makeToast(response.body()!!.message)
                    }
                }

                override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                    dismissLoader()
                    /*makeToast(getString(R.string.went_wrong_msg))
                    makeToast(t.message)*/
                }
            })
        }else{
            Toast.makeText(requireContext(), "No internet Connection Found", Toast.LENGTH_LONG).show()
        }
    }

    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }

    fun getAllStateList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().getState(hashMap).enqueue(object : Callback<ModelState?> {
            override fun onResponse(
                call: Call<ModelState?>,
                response: Response<ModelState?>
            ) {
                if (response.body() != null) {

                    for (i in 0 until response.body()!!.data.size) {

                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        stateList.add(model)

                    }

                    binding.spinnerState.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            selectedState =
                                parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            getAllCityList()
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelState?>, t: Throwable) {


            }
        })
    }

    fun getAllCityList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["state_id"] = selectedState.id

        RestClient.getInst().getCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {

                        var hint = cityList[0]
                        cityList.clear()
                        cityList.add(hint)

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }
                        cityAdapter.notifyDataSetChanged()

                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }


    var imageFile: File? = null


    fun hitEditProfileApiOnlyImage() {
        Log.d("a.ksfhdlbasyklcnh;sef", """""""""");

        RestClient.getInst().hitEditProfileApiOnlyPhoto(
            PrefManager.getInstance(requireContext())!!.userDetail.token,
            imageFile,
            object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                        getProfile()
                    }

                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                }
            })
    }

    fun updateProfile() {

        binding.etEmail.inputType = InputType.TYPE_NULL
        binding.etPhone.inputType = InputType.TYPE_NULL

        binding.etEmail.setText(PrefManager.getInstance(requireContext())!!.userDetail.email)
        binding.etFirstName.setText(PrefManager.getInstance(requireContext())!!.userDetail.first_name)
        binding.etLastName.setText(PrefManager.getInstance(requireContext())!!.userDetail.last_name)
        binding.etEmail.setText(PrefManager.getInstance(requireContext())!!.userDetail.email)
        binding.etPhone.setText(PrefManager.getInstance(requireContext())!!.userDetail.mobile)
        binding.etAbout.setText(PrefManager.getInstance(requireContext())!!.userDetail.about)
        /*binding.etCity.setText(PrefManager.getInstance(requireContext())!!.userDetail.city)*/
        binding.etDOB.setText(PrefManager.getInstance(requireContext())!!.userDetail.dob)

        Glide.with(requireActivity())
            .load(PrefManager.getInstance(requireContext())!!.userDetail.avtaar).circleCrop()
            .into(binding.ivUserImage)

        Glide.with(requireActivity())
            .load(PrefManager.getInstance(requireContext())!!.userDetail.image)
            .into(binding.ivbackimage)
    }

    val cropImage = registerForActivityResult(CropImageContract()) { result ->
        if (result.isSuccessful) {
            // use the returned uri
            val uriContent = result.uriContent
            val uriFilePath = result.getUriFilePath(requireContext()) // optional usage
            imageFile = File(result.getUriFilePath(requireContext(), true))
            Glide.with(requireContext()).load(uriFilePath).circleCrop()
                .into(binding!!.ivUserImage)
            hitEditProfileApiOnlyImage()

        } else {
            // an error occurred
            val exception = result.error
        }
    }

    fun startCrop() {
        // start picker to get image for cropping and then use the image in cropping activity
        cropImage.launch(
            options {
                setGuidelines(CropImageView.Guidelines.ON)
            }
        )

        /*       //start picker to get image for cropping from only gallery and then use the image in
        //cropping activity
        cropImage.launch(
            options {
                setImagePickerContractOptions(
                    PickImageContractOptions(includeGallery = true, includeCamera = false)
                )
            }
        )*/

       /* // start cropping activity for pre-acquired image saved on the device and customize settings
        cropImage.launch(
            options() {
                setGuidelines(CropImageView.Guidelines.ON)
                setOutputCompressFormat(Bitmap.CompressFormat.PNG)
            }
        )*/

    }
    /*fun isValid(): Boolean {

        if (!this::selectedState.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
            return false
        }


        if (!this::selectedCity.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select City", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }*/
}