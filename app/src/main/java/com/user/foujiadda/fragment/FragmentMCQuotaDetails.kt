package com.user.foujiadda.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentMCQuotaDetailsBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.utilities.ValidationHelper


class FragmentMCQuotaDetails(var train: Train) : Fragment() {

    private lateinit var binding: FragmentMCQuotaDetailsBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_m_c_quota_details, container, false)
        binding = DataBindingUtil.bind<FragmentMCQuotaDetailsBinding>(view)!!


        binding.backgroundTar.setOnClickListener {}

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupdata()
    }

    fun setupdata() {

        binding.tvTrainNumberValue.text = train.train_no

        binding.tvTrainNameValue.text =train.train_name

        binding.tvFromValue.text = train.stationName
        binding.tvToValue.text = train.destination
        binding.tvRemarksValue.text = train.remark

        binding.tvac1.text = train.seat_available.AC1
        binding.tvac2.text = train.seat_available.AC2
        binding.tvac3.text = train.seat_available.AC3


        if (ValidationHelper.isNull(train.seat_available.AC1)){
            binding.tvac1.text = "0"
        }

        if (ValidationHelper.isNull(train.seat_available.AC2)){
            binding.tvac2.text = "0"
        }


        if (ValidationHelper.isNull(train.seat_available.AC3)){
            binding.tvac3.text = "0"
        }

        binding.tvec.text = train.seat_available.EC
        binding.tvcc.text = train.seat_available.CC
        binding.tvsl.text = train.seat_available.SL
        binding.tvfc.text = train.seat_available.FC
        binding.tv2s.text = train.seat_available.f2S


        binding.tvContactDetails.text="Contact "+train.mcoName+" for availability"
        binding.tvContactDetails.setOnClickListener {
            // Use format with "tel:" and phoneNumber created is
            // stored in u.
            // Use format with "tel:" and phoneNumber created is
            // stored in u.
            val u: Uri = Uri.parse("tel:" + train.mcoContact)

            // Create the intent and set the data for the
            // intent as the phone number.

            // Create the intent and set the data for the
            // intent as the phone number.
            val i = Intent(Intent.ACTION_DIAL, u)

            try {
                // Launch the Phone app's dialer with a phone
                // number to dial a call.
                startActivity(i)
            } catch (s: SecurityException) {
                // show() method display the toast with
                // exception message.
                Toast.makeText(requireContext(), "An error occurred", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

}