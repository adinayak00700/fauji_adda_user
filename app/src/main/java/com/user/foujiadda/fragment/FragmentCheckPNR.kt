package com.user.foujiadda.fragment

import PassengerAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentCheckPNRBinding
import com.user.foujiadda.models.ModelPNR
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.models.Passenger
import com.user.foujiadda.models.UserDetails
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.HashMap


class FragmentCheckPNR : BaseFragment() {
    private lateinit var binding: FragmentCheckPNRBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_check_p_n_r, container, false)

        binding = DataBindingUtil.bind<FragmentCheckPNRBinding>(view!!)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSignIn.setOnClickListener {

            if (PrefManager.getInstance(requireContext())!!.userDetail.token=="skip"){

                val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(requireContext()))
                val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
                dialog.setCancelable(false)
                dialog.setView(dialogBinding.root)

                dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
                }

                dialogBinding.signup.setOnClickListener {
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
                }
                dialog.show()

            } else{

                if (!ValidationHelper.isNull(binding.etEmailAddress.text.toString())){
                    if (binding.etEmailAddress.text.toString().length==10){
                        getPnrDetails()
                    }else{
                        makeToast("please enter 10 digit PNR Number")
                    }
                }
            }


        }
    }

    fun getPnrDetails() {
        showLoader()
        val hashMap = HashMap<String, String>()
        hashMap["pnr"] = binding.etEmailAddress.text.toString()
        hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().getPNRDetails(hashMap).enqueue(object : Callback<ModelPNR> {
            override fun onResponse(call: Call<ModelPNR>, response: Response<ModelPNR>) {
                dismissLoader()
                Log.d("asdasdasd", response.body()!!.result.toString())
                if (response.body()!!.result) {
                try {
                    var pnrdata = response.body()!!.data
                    binding.tvNodata.visibility=View.GONE
                    binding.detailsLayout.visibility=View.VISIBLE
                    binding.tvFromShort.text = pnrdata.boarding_station
                    binding.tvFromLong.text = pnrdata.boarding_station

                    binding.tvToLong.text = pnrdata.reservation_upto
                    binding.tvToShort.text = pnrdata.reservation_upto


                    binding.tvpnrNumber.text = binding.etEmailAddress.text.toString()
                    binding.tvpnrRight.text = pnrdata.train_number

                    var adapter =
                        PassengerAdapter(context!!, pnrdata.passenger as ArrayList<Passenger>)
                    binding.recyclerView2.adapter = adapter
                    binding.recyclerView2.layoutManager = LinearLayoutManager(context!!)
                    binding.tvChartPrepared.text = pnrdata.chart_status
                }catch (e:Exception){
                    binding.tvNodata.visibility=View.VISIBLE
                    binding.detailsLayout.visibility=View.GONE
                    Toast.makeText(context!!, "No Data Found", Toast.LENGTH_SHORT).show()
                }
                } else {
                    binding.tvNodata.visibility=View.VISIBLE
                    binding.detailsLayout.visibility=View.GONE
                    Toast.makeText(context!!, "No Data Found", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ModelPNR>, t: Throwable) {
                dismissLoader()
                //   makeToast(t.message)
            }
        })
    }
}