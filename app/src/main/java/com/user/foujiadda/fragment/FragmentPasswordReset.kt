package com.user.foujiadda.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentPasswordResetBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FragmentPasswordReset(var callback:Callbackk) : BaseFragment(), View.OnClickListener {

    var binding: FragmentPasswordResetBinding? = null
    var prefManager: PrefManager? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_password_reset, container, false)
        binding = DataBindingUtil.bind(view)
        return view
    }

    public interface Callbackk{
        fun onPasswordChangedSuccessfully()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefManager = PrefManager(context!!)
        binding!!.mContinueButton.setOnClickListener(this)
        binding!!.mainLayout.setOnClickListener(null)
    }

    @SuppressLint("NonConstantResourceId")
    override fun onClick(v: View) {
        when (v.id) {

            R.id.mContinueButton -> {
                if (isValid) {
                    hitResetPasswordApi()
                }
            }


        }
    }


    fun hitResetPasswordApi() {
        val hashMap = HashMap<String, String>()
        hashMap["token"]=PrefManager.getInstance(context!!)!!.userDetail.token
        hashMap["old_password"] = binding!!.etOldPassword.text.toString()
        hashMap["new_password"] = binding!!.etPassword2.text.toString()

        RestClient.getInst().change_password(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {

                    makeToast("Password Changed Successfully")
                    callback.onPasswordChangedSuccessfully()

                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }


    val isValid: Boolean
        get() {
            if (ValidationHelper.isNull(binding!!.etOldPassword.text.toString())) {
                makeToast(requireContext().getString(R.string.enter_old_password))
                return false
            }else
            if (ValidationHelper.isNull(binding!!.etPassword.text.toString())) {
                makeToast(requireContext().getString(R.string.enter_password))
                return false
            } else if (ValidationHelper.isNull(binding!!.etPassword2.text.toString())) {
                makeToast(getString(R.string.please_confirm_password))
                return false
            }
            else if (!binding!!.etPassword.text.toString().equals(binding!!.etPassword2.text.toString())) {
                makeToast(getString(R.string.password_must_be_same))
                return false
            }
           
            return true
        }








}