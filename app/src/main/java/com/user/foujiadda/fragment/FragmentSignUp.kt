package user.faujiadda.fragment

import DialogFragmentLoader
import FragmentVerification
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentSignUpBinding
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.HashMap
import android.util.Patterns

import android.text.TextUtils
import com.example.datingapp.application.App
import com.user.foujiadda.activity.TeamAndConditionActivity
import com.user.foujiadda.fragment.FragmentPasswordReset
import com.user.foujiadda.models.UserDetails

class FragmentSignUp : BaseFragment() {
private  lateinit var  binding: FragmentSignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding=DataBindingUtil.bind<FragmentSignUpBinding>(inflater.inflate(R.layout.fragment_sign_up, container, false))!!

        binding.termandcondition.setOnClickListener {
            val intent = Intent(requireContext(), TeamAndConditionActivity::class.java)
            startActivity(intent)
        }


        return binding.root
    }
    var fragmentLoader:DialogFragmentLoader?=null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonSignUp.setOnClickListener {
           if (isValid()){

               fragmentLoader = DialogFragmentLoader(FragmentVerification("signUp",binding.etPhone.text.toString(),binding.etEmail.text.toString(),object : FragmentVerification.Callbackk{
                   override fun onUserVerifiedForSignUp(mobile: String?) {
                        fragmentLoader!!.dismiss()
                       signUpApi()

                   }

                   override fun onMobileNumberVerified(mobile: String?) {

                   }

                   override fun onUserVerifiedForResetPassword(mobile: String?) {

                   }

                   override fun onUserVerifiedForChangeNumber(mobile: String?) {

                   }
               }), "Verification")


               fragmentLoader!!.show(childFragmentManager, "asd")

           }
        }

        binding.tvSkip.setOnClickListener {
            var user= UserDetails("","","","","","skip","","", "","","","","","","")
            PrefManager.getInstance(requireActivity())!!.userDetail=user
            startActivity(IntentHelper.getDashboardActivity(requireActivity()))
        }


        binding.tvlogin.setOnClickListener {
            findNavController().navigate(R.id.idFragmentLoginToFragmentSingup)
        }
    }





    fun isValid():Boolean{
        if (ValidationHelper.isNull(binding.etFirstName.text.toString())){
            makeToast("Please Enter First Name")
            return false;
        }else
            if (ValidationHelper.isNull(binding.etLastName.text.toString())){
            makeToast("Please Enter Last Name")
            return false;
        }
        else   if (ValidationHelper.isNull(binding.etEmail.text.toString())){
            makeToast("Please Enter Email")
            return false;
        }
        else   if (!isValidEmail(binding.etEmail.text.toString())){
            makeToast("Please Enter Valid Email")
            return false;
        }
        else   if (ValidationHelper.isNull(binding.etPhone.text.toString())){
            makeToast("Please Enter Phone")
            return false;
        }
        else   if (ValidationHelper.isNull(binding.etPassword.text.toString())){
            makeToast("Please Enter Password")
            return false;
        }
        else   if (!binding.radioButton.isChecked){
            makeToast("Please Agree to TnC")
            return false;
        }


        return  true

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }



    fun signUpApi() {
        val hashMap = HashMap<String, String>()
        hashMap["email"] =binding.etEmail.text.toString()
        hashMap["mobile"] =binding.etPhone.text.toString()
        hashMap["password"] =binding.etPassword.text.toString()
        hashMap["first_name"] =binding.etFirstName.text.toString()
        hashMap["last_name"] =binding.etLastName.text.toString()
        hashMap["social_id"] = ""
        hashMap["device_id"] = ""
        hashMap["device_token"] = App.getInstance().firebaseToken.toString()
        hashMap["device_type"] = "android"


        RestClient.getInst().userSignUp(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                    PrefManager.getInstance(requireContext())!!.keyIsLoggedIn = true
                    startActivity(IntentHelper.getDashboardActivity(context))
                    activity!!.finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

}