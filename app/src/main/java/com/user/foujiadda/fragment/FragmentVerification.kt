
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.datingapp.application.App
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentVerificationBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.ModelUser
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.MyCount


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class FragmentVerification(
    private val verificationType: String,
    private val mobile: String,
    private val emails: String,
    private val callbackk: Callbackk
) :
    BaseFragment(), MyCount.Callbackk, View.OnClickListener {
    var binding: FragmentVerificationBinding? = null
    var isTimeUp = false
    var prefManager: PrefManager? = null
    var countDownTimer: CountDownTimer? = null
    var verificationID: String? = null
    var counter = 0
    private val isLogin: Boolean? = null
    private var otp: String=""
    private val name: String? = null
    private var email: String = ""
    var userEmail : String = ""

    interface Callbackk {
        fun onUserVerifiedForSignUp(mobile: String?)
        fun onMobileNumberVerified(mobile: String?)
        fun onUserVerifiedForResetPassword(mobile: String?)
        fun onUserVerifiedForChangeNumber(mobile: String?)
    }
    override
    fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_verification, container, false)
        binding = DataBindingUtil.bind(view)
        prefManager = PrefManager(requireContext())
        return view
    }

    fun startCount() {
        val myCount = MyCount(60000, 1000, this)
        myCount.start()
    }
        override
    fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // autofetchOTP();
        startCount()
            sendOtp()

            /*email For*/




        binding!!.mainLayout.setOnClickListener(null)
        binding!!.buttonVerify.setOnClickListener(this)
        binding!!.otpCountDownTimer.setOnClickListener(null)
        binding!!.pinView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 4) {
                    verifyOTPForFunctionality()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })




    }



  /*  fun hitUserLoginApi() {
        showLoader()
        val hashMap = HashMap<String, String>()
        val deviceId = Settings.Secure.ANDROID_ID
        val deviceToken: String = App.getInstance().getFirebaseToken()!!
        hashMap[Constants.kDeviceId] = deviceId
        hashMap[Constants.kDeviceToken] = deviceToken
        hashMap[Constants.kDeviceType] = "android"
        hashMap[Constants.number] = mobile
        hashMap[Constants.otp] = binding.pinView.getText().toString()
        RestClient.getInst().hitLoginApi(hashMap).enqueue(object : Callback<UserDetails> {
            override fun onResponse(call: Call<UserDetails>, response: Response<UserDetails>) {
                dismissLoader()
                if (response.body().isStatus()) {
                    val userDetails: UserDetails? = response.body()
                    prefManager.setKeyIsLoggedIn(true)
                    prefManager.setUserDetail(userDetails)
                    try {
                        startActivity(Intent(requireContext(), ActivityDashBoard::class.java))
                        //   startActivity(IntentHelper.getDashboardActivity(App.getInstance()));
                        getActivity().finish()
                    } catch (e: Exception) {
                    }
                } else {
                    makeToast(response.body().getMessage())
                }
            }

            override fun onFailure(call: Call<UserDetails>, t: Throwable) {
                dismissLoader()
                makeToast(t.message)
            }
        })
    }*/
        override
    fun onCountEnd() {
        isTimeUp = true
        binding!!.otpCountDownTimer.setText("Resend")
        binding!!.otpCountDownTimer.setOnClickListener(this)
    }
override
    fun onTimeGet(
        day0: Int,
        day1: Int,
        hours0: Int,
        hours1: Int,
        minute0: Int,
        minute1: Int,
        second0: Int,
        second1: Int
    ) {
        binding!!.otpCountDownTimer.setText(minute0.toString() + minute1.toString() + ":" + second0 + second1)
        isTimeUp = false
    }

    @SuppressLint("NonConstantResourceId")
    override fun onClick(v: View) {
        when (v.id) {
            R.id.buttonVerify -> {

                if (binding!!.pinView.getText().toString().length == 4) {
                    verifyOTPForFunctionality()
                } else {
                    makeToast("Enter OTP sent on your mobile number & email address")
                }
            }
            R.id.otpCountDownTimer -> {
                binding!!.otpCountDownTimer.setOnClickListener(null)
                isTimeUp = false
                startCount()
            }
        }
    }

    private fun verifyOTPForFunctionality() {
        when (verificationType) {
  /*          "login" -> hitUserLoginApi()*/
            "signUp" -> verifyOTPFORRegister()
            "resetPassword" -> verifyOTPFORResetpassword()
            "mobileUpdate" -> verifyOTPFORMobileUpdate()
        }
    }

    fun verifyOTPFORRegister() {

        if (otp==binding!!.pinView.text.toString()){
            callbackk.onUserVerifiedForSignUp(mobile)
        }else{
            makeToast("Incorrect Pin")
        }

    }

    fun verifyOTPFORResetpassword() {
      /*  val hashMap = HashMap<String, String>()
        hashMap[Constants.otp] = binding.pinView.getText().toString()
        hashMap[Constants.number] = mobile
        RestClient.getInst().user_request_otp_for_forget_password(hashMap)
            .enqueue(object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body().isStatus()) {
                        callbackk.onUserVerifiedForResetPassword(mobile)
                    } else {
                        makeToast(response.body().getMessage())
                    }
                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                    makeToast(t.message)
                }
            })*/
    }

    fun verifyOTPFORMobileUpdate() {
        if (otp==binding!!.pinView.text.toString()){
            callbackk.onMobileNumberVerified(mobile)
        }else{
            makeToast("Incorrect Pin")
        }

    }

    fun updateProfileMobileNumberApi() {
   /*     showLoader()
        RestClient.getInst()
            .hitEditNumberProfileApi(PrefManager.getInstance(getContext()).getUserDetail().getData()
                .getJwtToken(),
                mobile, object : Callback<ModelSuccess> {
                    override fun onResponse(
                        call: Call<ModelSuccess>,
                        response: Response<ModelSuccess>
                    ) {
                        dismissLoader()
                        if (response.body().isStatus()) {
                            callbackk.onMobileNumberVerified(mobile)
                        } else {
                            makeToast(response.body().getMessage())
                        }
                    }

                    override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                        dismissLoader()
                        makeToast(t.message)
                    }
                })*/
    }


    fun sendOtp() {
        val hashMap = HashMap<String, String>()

        hashMap.put("mobile", mobile)
        hashMap.put("email", emails)


        RestClient.getInst().sendOtp(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    try {

                     otp=response.body()!!.OTP

                    }catch (e:Exception){

                    }
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }

    fun signUpUsingPhone() {
        callbackk.onUserVerifiedForSignUp(mobile)
    }



}
