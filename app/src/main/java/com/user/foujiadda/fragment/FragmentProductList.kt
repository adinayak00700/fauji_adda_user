package com.user.foujiadda.fragment

import ProductAdapter
import SubCategoryAdapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants.USER_TOKEN
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentProductListBinding
import com.user.foujiadda.models.ModelProducts
import com.user.foujiadda.networking.RestClient
import user.faujiadda.models.ModelSubcategory
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val CATEGORY_ID = "catId"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentProductList.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentProductList : BaseFragment() {
    private  var search=""
    private var categoryId: String? = ""
    private var param2: String? = ""
    private lateinit var binding: FragmentProductListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            categoryId = it.getString(CATEGORY_ID)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentProductListBinding>(
            inflater.inflate(
                R.layout.fragment_product_list,
                container,
                false
            )
        )!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentProductList.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(categoryId: String) =
            FragmentProductList().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)

                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!categoryId.equals("")) {
            getAllSubcategory()

        } else {
            getAllProducts("")
            binding.rvSubCategory.visibility = View.GONE
        }


    }

    private fun getAllProducts(subCategoryId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["sub_categoryID"] = subCategoryId
        hashMap["search"] = search

        RestClient.getInst().products(hashMap).enqueue(object : Callback<ModelProducts?> {
            override fun onResponse(
                call: Call<ModelProducts?>?,
                response: Response<ModelProducts?>
            ) {
                if (response.body()!!.result) {
                    var adapter = ProductAdapter(requireContext())
                    adapter.addProducts(response.body()!!.data)
                    binding.rvProducts.adapter = adapter
                    binding.rvProducts.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                } else {
                }
            }

            override fun onFailure(call: Call<ModelProducts?>?, t: Throwable) {

            }
        })
    }

    private fun getAllSubcategory() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["categoryID"] = this.categoryId!!

        RestClient.getInst().subcategory(hashMap).enqueue(object : Callback<ModelSubcategory?> {
            override fun onResponse(
                call: Call<ModelSubcategory?>?,
                response: Response<ModelSubcategory?>
            ) {
                if (response.body()!!.result) {
                    var adapter = SubCategoryAdapter(
                        requireContext(),
                        response.body()!!.data,
                        object : SubCategoryAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {
                                getAllProducts(catId)
                            }
                        })
                    binding.rvSubCategory.adapter = adapter
                    binding.rvSubCategory.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    if (response.body()!!.data.size > 0) {
                        getAllProducts(response.body()!!.data[0].id)
                    }
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSubcategory?>?, t: Throwable) {

            }
        })
    }

    fun searchData(dataa:String){
        this.search=dataa
        getAllProducts("")
    }


}