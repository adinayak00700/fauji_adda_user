package com.user.foujiadda.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentForgetPasswordBinding
import com.user.foujiadda.models.ForgetPasswordModel
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ForgetPasswordFragment : BaseFragment() {

    private lateinit var  binding: FragmentForgetPasswordBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view=inflater.inflate(R.layout.fragment_forget_password, container, false)

        binding= DataBindingUtil.bind<FragmentForgetPasswordBinding>(view)!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSignIn.setOnClickListener {
            if (isValid()) {
                binding.buttonSignIn.isClickable = false
                loginApi()
            }
        }
    }


    fun loginApi() {
        val hashMap = HashMap<String, String>()
        hashMap["email"] =binding.etEmailAddress.text.toString()
        RestClient.getInst().forgetPassword(hashMap).enqueue(object : Callback<ForgetPasswordModel> {
            override fun onResponse(call: Call<ForgetPasswordModel>, response: Response<ForgetPasswordModel>) {
                if (response.body()!!.result) {

                    Handler(
                        Looper.getMainLooper()
                    ).postDelayed({

                        val bundle = Bundle()
                        bundle.putString("amount", binding.etEmailAddress.text.toString())
                        findNavController().navigate(R.id.idFragmentLogin, bundle)

//                        startActivity(
//                            IntentHelper.getLoginScreen(requireContext())
////                                .putExtra("data", selectedFilter)
//                        )
                    }, 15000)

                        binding.buttonSignIn.visibility = View.GONE
                        binding.toast.visibility = View.VISIBLE
                        binding.toast.setText(response.body()!!.message)


                }
            }

            override fun onFailure(call: Call<ForgetPasswordModel>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }






    fun isValid():Boolean{
        if (ValidationHelper.isNull(binding.etEmailAddress.text.toString())){
            makeToast("Please Enter Email")
            return false;
        }
        return  true

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun onCickEditReview() {

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        // set the custom layout
        val customLayout: View = layoutInflater.inflate(R.layout.activity_password_send_confirmation, null)
        builder.setView(customLayout)
        val dialog = builder.create()
        // Set other dialog properties
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()

        }
}