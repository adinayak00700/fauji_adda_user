package com.user.foujiadda.fragment.csd

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.user.foujiadda.R
import com.user.foujiadda.activity.ActivityLogin
import com.user.foujiadda.databinding.EmptyLayoutBinding
import com.user.foujiadda.databinding.FragmentGuestHouseSearchFormBinding
import com.user.foujiadda.models.ModelCustumSpinner
import com.user.foujiadda.models.ModelSelectedFilter
import com.user.foujiadda.models.spins.*
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.wedguruphotographer.adapter.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentGuestHouseSearchForm.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentGuestHouseSearchForm() : Fragment() {

    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()



    private lateinit var binding: FragmentGuestHouseSearchFormBinding
    private lateinit var selectedState: ModelCustumSpinner
    private lateinit var selectedCity: ModelCustumSpinner



    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var cityAdapter: CustumSpinAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_guest_house_search_form, container, false)
        binding = DataBindingUtil.bind<FragmentGuestHouseSearchFormBinding>(view)!!
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getAllStateList()


        var state = ModelCustumSpinner(id = "0", name = "Select State")
        stateList.add(state)

        stateAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            stateList, false
        )
        binding.spinnerState.setAdapter(stateAdapter)


       /* var city = ModelCustumSpinner(id = "0", name = "Select City")
        cityList.add(city)
        cityAdapter = CustumSpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            cityList, false
        )
        binding.spinnerCity.setAdapter(cityAdapter)*/




        binding.buttonSubmit.setOnClickListener {

            if (PrefManager.getInstance(requireContext())!!.userDetail.token=="skip"){

                val dialogBinding = EmptyLayoutBinding.inflate(LayoutInflater.from(requireContext()))
                val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.MyThemeOverlayAlertDialog).create()
                dialog.setCancelable(false)
                dialog.setView(dialogBinding.root)

                dialogBinding.login.setOnClickListener {
//                            dialog.dismiss()
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
//                            findNavController().navigate(R.id.fragmentDiverterToidFragmentLogin)
                }

                dialogBinding.signup.setOnClickListener {
                    val intent = Intent(requireContext(), ActivityLogin::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    requireContext().startActivity(intent)
                }
                dialog.show()

            } else{

                if (isValid()) {
                    var selectedData=ModelSelectedFilter(
                        state = selectedState.id, city ="", vehicleType = "", company_id = "",
                        type ="", vehicle_id = "", depot_id = "", filterType = "guest_house",  appointmentDate = "")
                    startActivity(IntentHelper.getVendorActivity(requireContext()).putExtra("data",selectedData))
                }
            }
        }

    }




    fun getAllStateList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().getState(hashMap).enqueue(object : Callback<ModelState?> {
            override fun onResponse(
                call: Call<ModelState?>,
                response: Response<ModelState?>
            ) {
                if (response.body() != null) {

                    for (i in 0 until response.body()!!.data.size) {
                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        stateList.add(model)
                    }

                    binding.spinnerState.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            selectedState =
                                parent!!.getItemAtPosition(position) as ModelCustumSpinner
                            /*getAllCityList()*/
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelState?>, t: Throwable) {

            }
        })
    }

    /*fun getAllCityList() {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["state_id"] = selectedState.id

        RestClient.getInst().getCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {


                        var hint = cityList[0]
                        cityList.clear()
                        cityList.add(hint)

                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }
                        cityAdapter.notifyDataSetChanged()



                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }*/





    fun isValid(): Boolean {

        if (!this::selectedState.isInitialized) {
            Toast.makeText(requireContext(), "Plase Select State", Toast.LENGTH_SHORT).show()
            return false
        }

        /*if (!this::selectedCity.isInitialized) {
            Toast.makeText(context!!, "Plase Select City", Toast.LENGTH_SHORT).show()
            return false
        }*/

        return true
    }

}