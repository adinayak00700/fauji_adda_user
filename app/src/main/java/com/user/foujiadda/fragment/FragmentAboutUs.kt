package com.user.foujiadda.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.user.foujiadda.MainActivity
import com.user.foujiadda.R
import com.user.foujiadda.base.BaseFragment
import com.user.foujiadda.databinding.FragmentAboutUsBinding
import com.user.foujiadda.models.AboutUs
import com.user.foujiadda.models.ModelJob
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentAboutUs : BaseFragment(){

    private lateinit var binding: FragmentAboutUsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getJobDetails()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_about_us, container, false)
        binding = DataBindingUtil.bind<FragmentAboutUsBinding>(view)!!
        getJobDetails()
        return binding.root
    }

    private fun getJobDetails() {

        RestClient.getInst().aboutUs().enqueue(object : Callback<AboutUs?> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(call: Call<AboutUs?>?, response: Response<AboutUs?>) {
                if (response.body()!!.result == "success") {

                    binding.aboutDescription.text = response.body()!!.data.about_us
//                    binding.aboutDescription.text=(Html.fromHtml(response.body()!!.data.about_us, Html.FROM_HTML_MODE_COMPACT))
                    binding.aboutDescription.setText(Html.fromHtml(response.body()!!.data.about_us));
                }
            }

            override fun onFailure(call: Call<AboutUs?>?, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

}