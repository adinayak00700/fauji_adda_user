package com.user.foujiadda.fragment

import CommentAdapter
import FeedAdapter
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.user.foujiadda.R
import com.user.foujiadda.databinding.FragmentFeedsBinding
import com.user.foujiadda.factory.CommentViewModelFactory
import com.user.foujiadda.factory.FeedViewModelFactory
import com.user.foujiadda.models.*
import com.user.foujiadda.models.viewmodel.CommentViewModel
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.ModelSuccessPost
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.IntentHelper
import com.user.foujiadda.utilities.NetworkUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentFeeds : Fragment(), FeedAdapter.Callbackk, CommentAdapter.Callbackk,
    FeedAdapter.OptionsMenuClickListener {
    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""
    private lateinit var binding: FragmentFeedsBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_feeds, container, false)
        binding = DataBindingUtil.bind<FragmentFeedsBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet.bottomSheet)
        initCommentAdapter()
        initFeedAdapter()
        getFeeds()
        iniRefreshListener()
        binding.buttonCreatePost.setOnClickListener {
            startActivityForResult(IntentHelper.getAddPost(requireContext()),111)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode== Activity.RESULT_OK && requestCode == 111){
            getFeeds()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentFeeds().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun iniRefreshListener() {

        binding.refreshaddpost.setOnRefreshListener {

            initCommentAdapter()
            initFeedAdapter()
            getFeeds()

            // You can make your API call here,
            val handler = Handler()
            handler.postDelayed(Runnable {
                if (binding.refreshaddpost.isRefreshing()) {
                    binding.refreshaddpost.setRefreshing(false)
                }
            }, 3000)
        }

    }

    private lateinit var viewModel: FeedViewModel
    private lateinit var feedAdapter: FeedAdapter
    fun initFeedAdapter() {
        binding.rvFeeds.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val factory = FeedViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(FeedViewModel::class.java)
        feedAdapter = FeedAdapter(viewModel, requireContext(), this, this)


        binding.rvFeeds.adapter = feedAdapter
        viewModel.livedatalist.observe(requireActivity(), Observer {
            feedAdapter.submitList(it)
        })
        binding.refreshaddpost.setRefreshing(false)
        binding.bottomSheet.ivSendButton.setOnClickListener {
            addComment(postId = postId.toInt(), binding.bottomSheet.etComment.text.toString())
        }
    }

    private lateinit var commentviewModel: CommentViewModel
    private lateinit var commentAdapter: CommentAdapter
    fun initCommentAdapter() {
        binding.bottomSheet.rvComments.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        val factory = CommentViewModelFactory()
        commentviewModel = ViewModelProviders.of(this, factory).get(CommentViewModel::class.java)
        commentAdapter = CommentAdapter(commentviewModel, requireContext(), this)
        binding.bottomSheet.rvComments.adapter = commentAdapter
        commentviewModel.livedatalist.observe(viewLifecycleOwner, Observer {
            commentAdapter.submitList(it)
        })
    }


    fun isBottomsheetExpanded():Boolean {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            return true;
        }else{
            return  false;
        }
    }


    fun getFeeds() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token

        hashMap["post_id"] = ""


        if (isNetworkAvailable()) {
            RestClient.getInst().getFeeds(hashMap).enqueue(object : Callback<ModelFeeds> {
                override fun onResponse(call: Call<ModelFeeds>, response: Response<ModelFeeds>) {
                    try {
                        if (response.body()!!.result) {
                            Log.d("shdasda", "popopoo")
                            viewModel.add(response.body()!!.data as ArrayList<Feed>)
                            binding.rvFeeds.adapter?.notifyDataSetChanged()
                        }

                    } catch (e: Exception) {

                    }


                }

                override fun onFailure(call: Call<ModelFeeds>, t: Throwable) {

                }
            })
        }else{
            Toast.makeText(requireContext(), "No internet Connection Found", Toast.LENGTH_LONG).show()
        }
    }

    override fun onClickONCommentLayout(postId: Int) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        this.postId = postId.toString()
        getComments()
    }

    override fun onProfileClick(postId: Int) {

    }

    fun isNetworkAvailable() : Boolean{

        return NetworkUtils.isNetworkAvailable()
    }

    fun getComments() {
        commentviewModel.clear()
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["post_id"] = postId
        RestClient.getInst().getComments(hashMap).enqueue(object : Callback<ModelComment> {
            override fun onResponse(call: Call<ModelComment>, response: Response<ModelComment>) {
                if (response.body()!!.result) {
                    commentviewModel.add(response.body()!!.data as ArrayList<Comment>)
                    binding.bottomSheet.rvComments.adapter?.notifyDataSetChanged()
                } else {

                }
            }

            override fun onFailure(call: Call<ModelComment>, t: Throwable) {

            }
        })
    }


    fun addComment(postId: Int, text: String) {
        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["post_id"] = postId.toString()
        hashMap["comment"] = text
        var comment = Comment(
            comment = text,
            post_id = postId.toString(),
            image = PrefManager.getInstance(requireActivity())!!.userDetail.image,
            created_at = "",
            id = "",
            user_name = PrefManager.getInstance(requireActivity())!!.userDetail.first_name,
            is_deleted = "",
            status = "",
            updated_at = "",
            user_id = ""
        )

        RestClient.getInst().addComments(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body()!!.result) {
                    binding.bottomSheet.etComment.setText(" ")

                    var list = ArrayList<Comment>()
                    list.add(comment)
                    commentviewModel.add(list)
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
            }
        })
    }

    override fun onOptionsMenuClicked(position: Int,id : String) {
        performOptionsMenuClick(position, id)
    }

    private fun performOptionsMenuClick(position: Int, id : String) {
        val popupMenu = PopupMenu(context, binding.rvFeeds[position])

        popupMenu.inflate(R.menu.user_block_menu)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            popupMenu.setGravity(Gravity.END)
        }

        popupMenu.setOnMenuItemClickListener { item ->
            when (item?.itemId) {
                R.id.Block -> {

                    val hashMap = HashMap<String, String>()
                    hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token
                    hashMap["block_userID"] = id
                    hashMap["status"] = "1"

                    RestClient.getInst().likeBlockFeed(hashMap).enqueue(object : Callback<ModelSuccessPost> {
                        override fun onResponse(call: Call<ModelSuccessPost>, response: Response<ModelSuccessPost>) {
                            if (response.body()!!.result) {
                                Toast.makeText(requireContext(), "Block User Request Successful", Toast.LENGTH_LONG).show()
                                initFeedAdapter()
                                getFeeds()
                            }
                        }

                        override fun onFailure(call: Call<ModelSuccessPost>, t: Throwable) {
                            Toast.makeText(requireContext(), t.message, Toast.LENGTH_LONG).show()
                        }
                    })

                }

                R.id.Reports -> {
                    val hashMap = HashMap<String, String>()
                    hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token
                    hashMap["post_id"] = id

                    RestClient.getInst().likePostReportFeed(hashMap).enqueue(object : Callback<ModelSuccessPost> {
                        override fun onResponse(call: Call<ModelSuccessPost>, response: Response<ModelSuccessPost>) {
                            if (response.body()!!.result) {
                                Toast.makeText(requireContext(), response.body()!!.message, Toast.LENGTH_LONG).show()
                                initFeedAdapter()
                                getFeeds()
                            }
                        }

                        override fun onFailure(call: Call<ModelSuccessPost>, t: Throwable) {
                            Toast.makeText(requireContext(), t.message, Toast.LENGTH_LONG).show()
                        }
                    })
                }


            }
            false

        }
        popupMenu.show()

    }



}