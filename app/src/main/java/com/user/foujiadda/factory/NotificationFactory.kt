package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.ModelRateAndReview
import com.user.foujiadda.models.viewmodel.NotificationViewModel
import com.user.foujiadda.models.viewmodel.VendorViewModel
import com.user.foujiadda.models.viewmodel.ViewModelRateAndReview

class NotificationFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(NotificationViewModel::class.java)){
            return NotificationViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }
}