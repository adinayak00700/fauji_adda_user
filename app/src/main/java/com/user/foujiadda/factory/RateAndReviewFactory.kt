package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.ModelRateAndReview
import com.user.foujiadda.models.viewmodel.VendorViewModel
import com.user.foujiadda.models.viewmodel.ViewModelRateAndReview

class RateAndReviewFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(ViewModelRateAndReview::class.java)){
            return ViewModelRateAndReview() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }
}