package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.viewmodel.FeedViewModel


class FeedViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(FeedViewModel::class.java)){
            return FeedViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}