package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.JobViewModel


class JobViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(JobViewModel::class.java)){
            return JobViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}