package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.VendorReviewViewModel


class VendorReviewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(VendorReviewViewModel::class.java)){
            return VendorReviewViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}