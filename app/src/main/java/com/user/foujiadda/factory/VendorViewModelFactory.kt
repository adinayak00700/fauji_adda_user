package com.user.foujiadda.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.viewmodel.FeedViewModel
import com.user.foujiadda.models.viewmodel.VendorViewModel


class VendorViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(VendorViewModel::class.java)){
            return VendorViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}