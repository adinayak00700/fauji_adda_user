package com.user.foujiadda.factory



import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.user.foujiadda.models.viewmodel.CommentViewModel


class CommentViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(CommentViewModel::class.java)){
            return CommentViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}