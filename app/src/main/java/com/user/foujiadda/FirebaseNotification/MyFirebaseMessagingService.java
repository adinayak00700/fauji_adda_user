package com.user.foujiadda.FirebaseNotification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.datingapp.utilities.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.user.foujiadda.R;
import com.user.foujiadda.activity.ActivityDashboard;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by Vaibhav
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String CHANNEL_ID = Constants.CHANNEL_ID;
    CharSequence name = "Product";
    String description = "Notifications";
    Intent intent;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.d("notii", s);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("asdasdasdasd", "asdasd");
        Log.d("asdasdasdasd", remoteMessage.getData().toString());

        Intent notificationIntent = new Intent(Constants.FILTER_NOTIFICATION_BROADCAST);
        Map<String, String> nData = remoteMessage.getData();

        if (nData.size() > 0) {

            for (String key : nData.keySet()) {
                notificationIntent.putExtra(key, nData.get(key));
            }
            sendBroadcast(notificationIntent);
        }
        sendNotification(remoteMessage);
    }


    public void sendNotification(RemoteMessage remoteMessage) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(mChannel);
            Intent intent = new Intent(this, ActivityDashboard.class);

            if (remoteMessage.getData().get("service") != null) {
                intent.putExtra("service", remoteMessage.getData().get("service"));
                Log.d("ahcjha", remoteMessage.getData().toString());
            }

            if (remoteMessage.getData().get("link") != null) {
                intent.putExtra("link", remoteMessage.getData().get("link"));
            }


      /*      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
               intent.setFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP);
               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
*/

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_MUTABLE);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setSmallIcon(R.drawable.fauji_adda_logo);
            if (remoteMessage.getNotification() != null) {
                mBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
                mBuilder.setContentText(remoteMessage.getNotification().getBody());
            } else {
                mBuilder.setContentTitle(remoteMessage.getData().get("title"));
                mBuilder.setContentText(remoteMessage.getData().get("body"));
            }
            mBuilder.setColor(getColor(R.color.colorLightGray));
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setCategory(NotificationCompat.CATEGORY_PROMO);
            mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mBuilder.setSound(defaultSoundUri);
            mBuilder.setAutoCancel(true);
            NotificationManagerCompat notificationManagerr = NotificationManagerCompat.from(this);
            notificationManagerr.notify((int) SystemClock.currentThreadTimeMillis(), mBuilder.build());

        } else {

            Intent notificationIntent = new Intent(Constants.FILTER_NOTIFICATION_BROADCAST);
            Map<String, String> nData = remoteMessage.getData();
            if (nData != null && nData.size() > 0) {
                Log.d("Notification", "From: " + nData.toString());
                for (String key : nData.keySet()) {
                    notificationIntent.putExtra(key, nData.get(key));
                }
                Log.d("notiii", "smaller");
                sendBroadcast(notificationIntent);
            }

            // Create an explicit intent for an Activity in your app
            Intent intent = new Intent(this, ActivityDashboard.class);
            intent.putExtra("notification_type", remoteMessage.getData().get("notification_type"));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            if (remoteMessage.getNotification() != null) {
                mBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
                mBuilder.setContentText(remoteMessage.getNotification().getBody());
            } else {
                mBuilder.setContentTitle(remoteMessage.getData().get("title"));
                mBuilder.setContentText(remoteMessage.getData().get("body"));
            }
            mBuilder.setColor(getResources().getColor(R.color.colorLightGray));
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setCategory(NotificationCompat.CATEGORY_PROMO);
            mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mBuilder.setSound(defaultSoundUri);
            mBuilder.setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify((int) SystemClock.currentThreadTimeMillis(), mBuilder.build());
        }


    }


}