package com.user.foujiadda.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

public class SetupFloatWidget{
    private final Context context;
    Callback callback;
    private static final int APP_PERMISSION_REQUEST = 102;
    public SetupFloatWidget(Context context, Callback callback) {
        this.context = context;
        this.callback = callback;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + context.getPackageName()));
            callback.toStartServiceForFloatingWidget(intent,APP_PERMISSION_REQUEST);

        } else {
            context.startService(new Intent(context, FloatWidgetService.class));
            ((Activity)(context)).finish();
        }
    }



    public interface Callback{
        void  toStartServiceForFloatingWidget(Intent intent,int requestCode);
    }
}
