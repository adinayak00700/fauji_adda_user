package com.user.foujiadda.map;

import static com.google.android.gms.maps.model.JointType.ROUND;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;


import com.example.datingapp.application.App;
import com.example.datingapp.utilities.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.gson.Gson;
import com.user.foujiadda.R;
import com.user.foujiadda.callbacks.MapCallbacks;
import com.user.foujiadda.enums.AppMarker;
import com.user.foujiadda.models.Vendor;


import java.util.ArrayList;
import java.util.List;


public class DashboardMapManager implements GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnMarkerClickListener{


    private GoogleMap googleMap;
    private LatLng pickLocation, dropLocation, currentLocation;
    public String pickAddress, dropAddress;
    private AddressManager addressManager;
    private MapCallbacks callbacks;
    private ImageView pickDropMarker;
    private AppMarker currentSelected;
    private boolean isAnimatingToMarker, cameraListenersActive;
    private ArrayMap<String, Marker> driverCarMarkerMap=new ArrayMap<>();
    private List<LatLng> listLatLng = new ArrayList<>();
    private Polyline blackPolyLine,greyPolyLine;
    private ArrayMap<Marker, Long> timeMap = new ArrayMap<>();

    public LatLng getPickLocation() {
        return pickLocation;
    }

    public LatLng getDropLocation() {
        return dropLocation;
    }

    public void registerMap(@NonNull GoogleMap map){
        googleMap = map;
        addressManager = new AddressManager();
    }

    public void setPickDropMarker(ImageView pickDropMarker) {
        this.pickDropMarker = pickDropMarker;
    }

    public void setCallbacks(MapCallbacks callbacks) {
        this.callbacks = callbacks;
        if(addressManager!=null)addressManager.setCallbacks(callbacks);
    }





    public void moveToSelectedAddressType(AppMarker selectedType, LatLng location){
        try{
            if (selectedType.equals(AppMarker.PICK)){
                pickLocation=location;
                    setPickLocation(pickLocation);
            }else if (selectedType.equals(AppMarker.DROP)){
                dropLocation=location;
                setDropLocation(dropLocation);
            }
        }
        catch (Exception e){

        }

    }







/*
    public void setPickLocation(AppPlace place) {
        pickLocation = place.getPosition();
        pickAddress = place.getDescription();
        callbacks.onPickUpAddressFound(place.getDescription());
        moveToPick();
    }

*/





















    private void setPickLocation(LatLng location){
        pickLocation = location;
        moveToPick();
        if(callbacks!=null){
            callbacks.onPickAddressFetchStart();
            addressManager.findAddress(location, true);
            Log.d("adsad","iuybg");
        }
        Log.d("adsad","asdiuybg");
    }

    private void setDropLocation(LatLng location){
        dropLocation = location;
        moveToDrop();
        if(callbacks!=null){
            callbacks.onDropAddressFetchStart();

            addressManager.findAddress(location, false);
        }
    }

    private void moveToPick(){
        isAnimatingToMarker = true;
        currentSelected = AppMarker.PICK;
/*      CameraUpdate pickCamera = CameraUpdateFactory.newLatLngZoom(pickLocation, Constants.MAP_ZOOM_ADDRESS);
        googleMap.moveCamera(pickCamera);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickLocation, 12f));*/
        googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                       new LatLng(pickLocation.latitude, pickLocation.longitude),
                        12f
                )
        );

       // pickDropMarker.setImageDrawable(AppMethods.getDrawable(R.drawable.locationmarker));

        if(!cameraListenersActive){
            googleMap.setOnCameraIdleListener(this);
            googleMap.setOnCameraMoveListener(this);
            googleMap.setOnCameraMoveStartedListener(this);
            googleMap.setOnMarkerClickListener(this);
            cameraListenersActive = true;
            Log.d("adsasd","adsasf");
        }
    }

    private void moveToDrop(){
        isAnimatingToMarker = true;
        currentSelected = AppMarker.DROP;
        CameraUpdate dropCamera = CameraUpdateFactory.newLatLngZoom(dropLocation, Constants.MAP_ZOOM_ADDRESS);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dropLocation, 16f));
      //  pickDropMarker.setImageDrawable(AppMethods.getDrawable(R.drawable.location_gps_icon));
    }

    @Override
    public void onCameraIdle() {
     /*   if(!isAnimatingToMarker){

            if(currentSelected==AppMarker.PICK){
                callbacks.showBottomOptions();
            }else {
                callbacks.showBottomOptions();
            }

            LatLng center = googleMap.getCameraPosition().target;

            if(callbacks!=null){
                if(currentSelected==AppMarker.PICK) {
                    pickLocation = center;
                    Log.d("askdhjg",pickLocation.toString());
                    callbacks.onPickAddressFetchStart();
                    addressManager.findAddress(center, true);
                }else {
                    if(dropLocation==null)return;
                    dropLocation = center;
                    callbacks.onDropAddressFetchStart();
                    addressManager.findAddress(center, false);
                }
            }
        }else {
            final Handler handler  = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    hidePickDropMarker(false);
                    handler.removeCallbacks(this);
                }
            },300);
            isAnimatingToMarker = false;
        }*/
    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int i) {
        if(!isAnimatingToMarker){
         //   callbacks.hideBottomOptions();
        }else {
            hidePickDropMarker(true);
        }
    }

    private void hidePickDropMarker(boolean hide){
        if(hide){
        //    pickDropMarker.setVisibility(View.INVISIBLE);
        }else {
        //    pickDropMarker.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d("asdsadasda","asdasdasdad");
      //  callbacks.onmarkerClick(marker);
        return true;
    }


    public void mapDrivers(ArrayMap<String, LatLng> currentDriversHashMap){
        if(googleMap==null){
            for (String driverId : currentDriversHashMap.keySet()) {
                MarkerOptions driverCarMarker = new MarkerOptions()
                        .position(currentDriversHashMap.get(driverId))
                        .icon(BitmapDescriptorFactory.fromResource(
                                R.drawable.add));
                if(driverCarMarkerMap==null)driverCarMarkerMap = new ArrayMap<>();
                driverCarMarkerMap.put(driverId, googleMap.addMarker(driverCarMarker));
            }
        }else {
            if (currentDriversHashMap == null || currentDriversHashMap.isEmpty()) {
                for (String driverID : driverCarMarkerMap.keySet()) {
                    //driverCarMarkerMap.get(driverID).remove();
                    timeMap.remove(driverCarMarkerMap.get(driverID));
                    animateRemove(driverCarMarkerMap.get(driverID));
                }
                driverCarMarkerMap.clear();
            } else {
                if(currentDriversHashMap.size() > driverCarMarkerMap.size()){
                    for (String currentDriver : currentDriversHashMap.keySet()) {
                        if (driverCarMarkerMap.containsKey(currentDriver)) {
                            animateDriverCar(currentDriver, currentDriversHashMap.get(currentDriver),
                                    driverCarMarkerMap.get(currentDriver));
                        }else {
                            MarkerOptions driverCarMarker = new MarkerOptions()
                                    .position(currentDriversHashMap.get(currentDriver))
                                    .icon(BitmapDescriptorFactory.fromResource(
                                            R.drawable.add));
                            driverCarMarkerMap.put(currentDriver, googleMap.addMarker(driverCarMarker));
                        }
                    }
                }else {
                    List<String> residualDriversId = new ArrayList<>();
                    for (String oldDriver : driverCarMarkerMap.keySet()) {
                        if (currentDriversHashMap.get(oldDriver) == null) {
                            residualDriversId.add(oldDriver);
                            timeMap.remove(driverCarMarkerMap.get(oldDriver));
                            animateRemove(driverCarMarkerMap.get(oldDriver));
                        }
                    }
                    if(!residualDriversId.isEmpty()) driverCarMarkerMap.removeAll(residualDriversId);

                    for (String driverId : currentDriversHashMap.keySet()) {
                        if (driverCarMarkerMap.keySet().contains(driverId)) {
                            //driverCarMarkerMap.get(driverId).setPosition(newDriversHashMap.get(driverId));
                            animateDriverCar(driverId, currentDriversHashMap.get(driverId),
                                    driverCarMarkerMap.get(driverId));

                        } else {
                            MarkerOptions driverCarMarker = new MarkerOptions()
                                    .position(currentDriversHashMap.get(driverId))
                                    .icon(BitmapDescriptorFactory.fromResource(
                                            R.drawable.add));

                            driverCarMarkerMap.put(driverId, googleMap.addMarker(driverCarMarker));
                        }
                    }
                }
            }
        }
     /*   if(pickMarker!=null && dropMarker!=null && driverCarMarkerMap != null && !driverCarMarkerMap.isEmpty()){
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(pickMarker.getPosition());
            builder.include(dropMarker.getPosition());
            for(String k : driverCarMarkerMap.keySet()){
                builder.include(driverCarMarkerMap.get(k).getPosition());
            }
            final LatLngBounds bounds = builder.build();
            try{
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
                googleMap.animateCamera(cu, 175, null);
            }catch (Exception e){
                e.printStackTrace();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,50);
                googleMap.animateCamera(cu, 175, null);
            }
        }*/
    }

   private void animateDriverCar(String driverKey, final LatLng newPosition, final Marker marker) {
        long lastAnimationTime = timeMap.containsKey(marker) ? timeMap.get(marker) : 0;
        long currentTime = System.currentTimeMillis();

        long diff = currentTime - lastAnimationTime;

        if (marker != null && diff >= 1000) {

            final LatLng currentPosition = marker.getPosition();

            Location currentLocation = new Location("");
            currentLocation.setLatitude(currentPosition.latitude);
            currentLocation.setLongitude(currentPosition.longitude);

            Location newLocation = new Location("");
            newLocation.setLatitude(newPosition.latitude);
            newLocation.setLongitude(newPosition.longitude);

            if(newLocation.distanceTo(currentLocation) >= 150){
                //marker.setPosition(newPosition);
                animateRemove(marker);
                MarkerOptions driverCarMarker = new MarkerOptions()
                        .position(newPosition)
                        .icon(BitmapDescriptorFactory.fromResource(
                                R.drawable.add));
                driverCarMarkerMap.put(driverKey, googleMap.addMarker(driverCarMarker));
                timeMap.put(marker, System.currentTimeMillis());
                return;
            }

            //final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng destinationPosition = latLngInterpolator.interpolate(v, currentPosition, newPosition);
                        marker.setPosition(destinationPosition);
                        marker.setRotation(computeRotation(v, marker.getRotation(),
                                (float) bearingBetweenLocations(currentPosition, newPosition)));
                        marker.setAnchor(0.5f,0.5f);
                        marker.setFlat(true);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            valueAnimator.start();
            timeMap.put(marker, System.currentTimeMillis());
        }
    }

    private void animateRemove(final Marker marker) {

        if (marker != null) {
            final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(375);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    marker.setAlpha(1-(float)animation.getAnimatedValue());
                }
            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    marker.remove();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            valueAnimator.start();
        }
    }


    public boolean isPolylinePresent() {
        return blackPolyLine!=null;
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    private static double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    public void drawMarkers(LatLng pick, LatLng drop){
        MarkerOptions pickMarker = new MarkerOptions();
        pickMarker.position(pick);

        pickMarker.zIndex(Float.MAX_VALUE);
        pickMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        googleMap.addMarker(pickMarker);

        MarkerOptions dropMarker = new MarkerOptions();
        dropMarker.position(drop);
        dropMarker.zIndex(Float.MAX_VALUE);
        dropMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        googleMap.addMarker(dropMarker);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(pick);
        builder.include(drop);
        LatLngBounds bounds = builder.build();
        try{
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            googleMap.moveCamera(cu);
        }catch (Exception e){
            e.printStackTrace();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,50);
            googleMap.moveCamera(cu);
        }
    }




    public void drawCurrentLocMarker( int index ,LatLng location){




        MarkerOptions dropMarker = new MarkerOptions();
        dropMarker.position(location);
        dropMarker.zIndex(1);
        dropMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        googleMap.addMarker(dropMarker);

        dropMarker.title("You are Here");
        dropMarker.snippet("Your Location");

        Marker addedMarker = googleMap.addMarker(dropMarker);

        addedMarker.setTag("Your Location");




    }





/*
    public void drawMarker(ModelNearbyVendor.DataBean vendor, Context context, int index ){

    Log.d("asdasdasd",new Gson().toJson(vendor));
        *//*
       googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.latitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pinn_new)));
*//*


        MarkerOptions pickMarker = new MarkerOptions();
        pickMarker.title(vendor.getAddress());
        pickMarker.alpha(0.8f);
        pickMarker.snippet(vendor.getNumber());

        pickMarker.position(new LatLng(vendor.getLat1(),vendor.getLong1()));
        pickMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(vendor)));
        pickMarker.zIndex(index);
        Marker addedMarker = googleMap.addMarker(pickMarker);

        addedMarker.setTag(new Gson().toJson(vendor));




    }






    public static Bitmap createDrawableFromView(ModelNearbyVendor.DataBean vendor) {

            View inflatedFrame = ((LayoutInflater) App.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
            ImageView markerImageView = (ImageView) inflatedFrame.findViewById(R.id.ivImage);
        ImageView ivStar = (ImageView) inflatedFrame.findViewById(R.id.ivStar);
        LottieAnimationView animation_view = (LottieAnimationView) inflatedFrame.findViewById(R.id.animation_view);

                animation_view.setAnimation(R.raw.pink_loading);
        animation_view.playAnimation();
        ivStar.setImageDrawable(ContextCompat.getDrawable(App.getInstance(),R.drawable.star));
        TextView tvTitle = (TextView) inflatedFrame.findViewById(R.id.tvTitle);
        TextView tvRating = (TextView) inflatedFrame.findViewById(R.id.tvRating);
        tvTitle.setText(vendor.getBusiness_name());
        tvRating.setText("5 Stars");
        FrameLayout frameLayout = (FrameLayout) inflatedFrame.findViewById(R.id.screen) ;
        frameLayout.setDrawingCacheEnabled(true);
        frameLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        frameLayout.layout(0, 0, frameLayout.getMeasuredWidth(), frameLayout.getMeasuredHeight());
        frameLayout.buildDrawingCache(true);

            return frameLayout.getDrawingCache();




    }*/
    public void drawPolyline(final List<LatLng> result) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final PolylineOptions lineOptions = new PolylineOptions();
                listLatLng.clear();
                listLatLng.addAll(result);

                lineOptions.width(10);
                lineOptions.color(Color.BLACK);
                lineOptions.startCap(new SquareCap());
                lineOptions.endCap(new SquareCap());
                lineOptions.jointType(ROUND);
                lineOptions.addAll(listLatLng);
                lineOptions.zIndex(1);

                final PolylineOptions greyOptions = new PolylineOptions();
                greyOptions.width(10);
                greyOptions.color(Color.GRAY);
                greyOptions.startCap(new SquareCap());
                greyOptions.endCap(new SquareCap());
                greyOptions.jointType(ROUND);
                lineOptions.zIndex(2);

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        blackPolyLine = googleMap.addPolyline(lineOptions);
                        greyPolyLine = googleMap.addPolyline(greyOptions);
                       // animatePolyLine();
                    }
                });
            }
        }).start();
    }

    void animatePolyLine(){
        ValueAnimator tAnimator = ValueAnimator.ofFloat(0, 1);
        tAnimator.setRepeatCount(ValueAnimator.INFINITE);
        tAnimator.setRepeatMode(ValueAnimator.RESTART);
        tAnimator.setInterpolator(new LinearInterpolator());
        tAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

            }
        });
    }

    public void removeAllMarkers(){
        if (googleMap!=null){
            googleMap.clear();
        }
        if (driverCarMarkerMap.size()>0){
            driverCarMarkerMap.clear();
        }
    }


   /* public void drawPath(LatLng pickLocation, LatLng dropLocation){

        TimeDistanceManager.getPolylineRoute(pickLocation, dropLocation, new LatLng(0,0), new PolylineCallBacks() {
            @Override
            public void onPolylineDataFetched(List<LatLng> points, String distanceInKM, int distanceInMetres, String timeInMin, int timeInSeconds) {
                drawPolyline(points);
                callbacks.onGettinngPolygonData(distanceInKM,distanceInMetres,timeInMin,timeInSeconds);

            }
            @Override
            public void onTaskSuccess(Object responseObj) {

            }

            @Override
            public void onTaskError(String errorMsg) {

            }

            @Override
            public void onPendingRideFound(String ride_id) {

            }

            @Override
            public void onSessionExpire(String message) {

            }

            @Override
            public void onAppNeedUpdate(String message) {

            }

            @Override
            public void onAppNeedForceUpdate(String message) {

            }

            @Override
            public void onInternetNotFound() {

            }

            @Override
            public void delayAppUpdate() {

            }

            @Override
            public void showLoader() {

            }

            @Override
            public void dismissLoader() {

            }

            @Override
            public void onFragmentDetach(String fragmentTag) {

            }



            @Override
            public void onErrorMapp(String message) {

            }

        });
    }*/



    public void drawVendorMarkerOnMap(Vendor vendor){

        Log.d("asdasdasd",new Gson().toJson(vendor));
        /*
       googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.latitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pinn_new)));
*/

        MarkerOptions pickMarker = new MarkerOptions();
        pickMarker.title(vendor.getAddress());
        pickMarker.alpha(0.8f);
        pickMarker.snippet(vendor.getMobile());


        if (!vendor.getLat().isEmpty() || !vendor.getLng().isEmpty()) {

            //   pickMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(vendor)));
            //pickMarker.zIndex(index);
     try {
         pickMarker.position(new LatLng(Double.parseDouble(vendor.getLat()), Double.parseDouble(vendor.getLng())));
         Marker addedMarker = googleMap.addMarker(pickMarker);

         addedMarker.setTag(new Gson().toJson(vendor));
     }catch (Exception e){}

        }


    }


    public static Bitmap createDrawableFromView(Vendor vendor) {

        View inflatedFrame = ((LayoutInflater) App.Companion.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) inflatedFrame.findViewById(R.id.ivImage);
        ImageView ivStar = (ImageView) inflatedFrame.findViewById(R.id.ivStar);
 /*       LottieAnimationView animation_view = (LottieAnimationView) inflatedFrame.findViewById(R.id.animation_view);

        animation_view.setAnimation(R.raw.pink_loading);
        animation_view.playAnimation();*/
      //  ivStar.setImageDrawable(ContextCompat.getDrawable(App.getInstance(),R.drawable.star));
        TextView tvTitle = (TextView) inflatedFrame.findViewById(R.id.tvTitle);
        TextView tvRating = (TextView) inflatedFrame.findViewById(R.id.tvRating);
        tvTitle.setText(vendor.getName());
      //  tvRating.setText("5 Stars");
        FrameLayout frameLayout = (FrameLayout) inflatedFrame.findViewById(R.id.screen) ;
        frameLayout.setDrawingCacheEnabled(true);
        frameLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        frameLayout.layout(0, 0, frameLayout.getMeasuredWidth(), frameLayout.getMeasuredHeight());
        frameLayout.buildDrawingCache(true);

        return frameLayout.getDrawingCache();




    }

}
