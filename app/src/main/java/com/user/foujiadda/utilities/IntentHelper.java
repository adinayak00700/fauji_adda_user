package com.user.foujiadda.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.StrictMode;




import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.user.foujiadda.R;

import com.user.foujiadda.activity.ActivityAddPost;
import com.user.foujiadda.activity.ActivityCSD;
import com.user.foujiadda.activity.ActivityDashboard;
import com.user.foujiadda.activity.ActivityECHS;
import com.user.foujiadda.activity.ActivityFeedDetails;
import com.user.foujiadda.activity.ActivityJob;
import com.user.foujiadda.activity.ActivityJobDetails;
import com.user.foujiadda.activity.ActivityLogin;
import com.user.foujiadda.activity.ActivityMobileNumber;
import com.user.foujiadda.activity.ActivityNews;
import com.user.foujiadda.activity.ActivityOffer;
import com.user.foujiadda.activity.ActivityOtherUserProfile;
import com.user.foujiadda.activity.ActivitySocialApp;
import com.user.foujiadda.activity.ActivitySrttings;
import com.user.foujiadda.activity.ActivityTravel;
import com.user.foujiadda.activity.ActivityVendor;
import com.user.foujiadda.activity.ActivityVendorDescription;
import com.user.foujiadda.activity.NotificationActivity;
import com.user.foujiadda.activity.RateAndReviewActivity;
import com.user.foujiadda.activity.SliderActivity;
import com.user.foujiadda.databinding.ActivitySliderBinding;
//import com.user.techokissan.activity.ActivityDashboard;

public class IntentHelper {

  public static Intent getDashboardActivity(Context context){
        return new Intent(context, ActivityDashboard.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }



    public static Intent getCSDScreen(Context context){
        return new Intent(context, ActivityCSD.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getSettingsActivity(Context context){
        return new Intent(context, ActivitySrttings.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getFeedDetails(Context context){
        return new Intent(context, ActivityFeedDetails.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static Intent getRateANdReviewsActivity(Context context){
        return new Intent(context, RateAndReviewActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getVendorActivity(Context context){
        return new Intent(context, ActivityVendor.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getNewsActivity(Context context){
        return new Intent(context, ActivityNews.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getNumberSvreen(Context context){
        return new Intent(context, ActivityMobileNumber.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getOfferScreen(Context context){
        return new Intent(context, ActivityOffer.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getJobScreen(Context context){
        return new Intent(context, ActivityJob.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getECHSScreen(Context context){
        return new Intent(context, ActivityECHS.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getTravelScreen(Context context){
        return new Intent(context, ActivityTravel.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getVendorDetailsScreen(Context context){
        return new Intent(context, ActivityVendorDescription.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getAddPost(Context context){
        return new Intent(context, ActivityAddPost.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static Intent getOtherUserProfile(Context context){
        return new Intent(context, ActivityOtherUserProfile.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getJobDetailsScreen(Context context){
        return new Intent(context, ActivityJobDetails.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


   public static Intent getLoginScreen(Context context){
        return new Intent(context, ActivityLogin.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getOnboardingScreen(Context context){
        return new Intent(context, SliderActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static Intent getSocialApp(Context context){
        return new Intent(context, ActivitySocialApp.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getNotificationActivity(Context context){
        return new Intent(context, NotificationActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static void shareAPP(Context context, String description) {  // it will share this app
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
            String shareMessage = description+"\n\n";
            //   shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            //  shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.agricoaching.app" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Share Via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void chatWhatsApp(Context context){ // will redirect to WhatsApp to particular contact
        String url = "https://api.whatsapp.com/send?phone="+"+910000000000";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static void sharePostWithPhoto(Context context, String description , Bitmap bitmap) {  // will send a photo to any other application
        File file=new File(context.getExternalCacheDir()+"/"+context.getResources().getString(R.string.app_name)+".png");
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            FileOutputStream fileOutputStream=new FileOutputStream(file);
//            bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            Intent intent=new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/*");
            context.startActivity(Intent.createChooser(intent,"share"));
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void shareAPPDeeplink(Context context, String description ) {  // it will share this app
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));

            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            String shareMessage = description + "\n\n";
            //  shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            //  shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.agricoaching.app" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Share Via"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
