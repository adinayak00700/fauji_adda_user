package com.user.foujiadda.utilities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

public class CustomDatePicker {

    public static void showPicker(Context context, Callbackk callbackk){
        Calendar c=Calendar.getInstance();
       /* Integer month=c.get(Calendar.MONTH);
        Integer day=c.get(Calendar.DAY_OF_MONTH);
        Integer year=c.get(Calendar.YEAR);*/

        DatePickerDialog datePickerDialog =new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int mon=month+1;
                callbackk.onGetDate(dayOfMonth+"-"+mon+"-"+year);
                /*String _year = String.valueOf(year);*/
                String _month = (month+1) < 10 ? "0" + (month+1) : String.valueOf(month+1);
                String _date = dayOfMonth < 10 ? "0" + dayOfMonth : String.valueOf(dayOfMonth);
                String _pickedDate = year + "-" + _month + "-" + _date;
                Log.e("PickedDate: ", "Date: " + _pickedDate);
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public interface  Callbackk{
        void  onGetDate(String date);
    }
}
