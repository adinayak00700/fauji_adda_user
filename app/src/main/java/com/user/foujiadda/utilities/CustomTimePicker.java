package com.user.foujiadda.utilities;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CustomTimePicker {
    private static SimpleDateFormat mFormat;
    private static boolean withAMPM=true;
    static TimePickerDialog mTimePicker;
    public static void showPicker(Context context, Callbackk callbackk){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
              int   hour = selectedHour;
              int   minute = selectedMinute;
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, minute);
                if (mFormat == null)
                    mFormat = new SimpleDateFormat(withAMPM ? "hh:mm a" : "HH:mm", Locale.getDefault());
                     callbackk.onGetTime(mFormat.format(c.getTime()));

            dismiss();
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();


        
    }

    public static void  dismiss(){
        mTimePicker.dismiss();
    }


    public interface  Callbackk{
        void  onGetTime(String time);
    }
}
