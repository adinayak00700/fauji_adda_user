package com.user.foujiadda.utilities;

import android.os.CountDownTimer;

import java.util.concurrent.TimeUnit;

public class MyCount extends CountDownTimer {
    private final Callbackk callbackk;

    public MyCount(long millisInFuture, long countDownInterval, Callbackk callbackk) {
        super(millisInFuture, countDownInterval);
        this.callbackk = callbackk;
    }

    @Override
    public void onFinish() {
        callbackk.onCountEnd();
    }

    @Override
    public void onTick(long millisUntilFinished) {
        long millis = millisUntilFinished;

        int day0=0;
        int day1=0;
        int hours0=0;
        int hours1=0;
        int minute0=0;
        int minute1=0;
        int second0=0;
        int second1=0;
        long days= (TimeUnit.MILLISECONDS.toDays(millis));
        long hours= (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)));
        long minutes= (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)));
        long seconds= (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

            if (String.valueOf(days).length()==2){
                day0=Integer.parseInt(String.valueOf(String.valueOf(days).charAt(0)));
                day1=Integer.parseInt(String.valueOf(String.valueOf(days).charAt(1)));
            }else {
                day1=Integer.parseInt(String.valueOf(String.valueOf(days).charAt(0)));
            }
        if (String.valueOf(hours).length()==2){
            hours0=Integer.parseInt(String.valueOf(String.valueOf(hours).charAt(0)));
            hours1=Integer.parseInt(String.valueOf(String.valueOf(hours).charAt(1)));
        }else {
            hours1=Integer.parseInt(String.valueOf(String.valueOf(hours).charAt(0)));
        }

        if (String.valueOf(minutes).length()==2){
            minute0=Integer.parseInt(String.valueOf(String.valueOf(minutes).charAt(0)));
            minute1=Integer.parseInt(String.valueOf(String.valueOf(minutes).charAt(1)));
        }else {
            minute1=Integer.parseInt(String.valueOf(String.valueOf(minutes).charAt(0)));
        }
        if (String.valueOf(seconds).length()==2){
            second0=Integer.parseInt(String.valueOf(String.valueOf(seconds).charAt(0)));
            second1=Integer.parseInt(String.valueOf(String.valueOf(seconds).charAt(1)));
        }else {
            second1=Integer.parseInt(String.valueOf(String.valueOf(seconds).charAt(0)));
        }
       callbackk.onTimeGet(day0,day1,hours0,hours1,minute0,minute1,second0,second1);
    }

    public interface  Callbackk{
        void  onCountEnd();
        void  onTimeGet(int day0,int day1, int hours0, int hours1, int minute0, int minute1,int second0, int second1);
    }
}