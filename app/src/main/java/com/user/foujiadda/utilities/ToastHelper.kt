package com.user.foujiadda.utilities

import android.R
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.os.Build
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat


class ToastHelper(context: Context) {
    interface OnShowListener {
        fun onShow(toast: ToastHelper?)
    }

    interface OnDismissListener {
        fun onDismiss(toast: ToastHelper?)
    }

    private val context: Context?
    private val windowManager: WindowManager
    private var toastView: View? = null
    var gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
        private set
    var xOffset = 0
        private set
    var yOffset = 0
        private set
    var duration = DEFAULT_DURATION_MILLIS
    private var text: CharSequence = ""
    var horizontalMargin = 0
        private set
    var verticalMargin = 0
        private set
    private var params: WindowManager.LayoutParams? = null
    private var handler: Handler? = null
    var isShowing = false
        private set
    private var leadingInfinite = false
    private var onShowListener: OnShowListener? = null
    private var onDismissListener: OnDismissListener? = null
    private val timer = Runnable { cancel() }
    private fun init() {
        yOffset = context!!.resources.displayMetrics.widthPixels / 5
        params = WindowManager.LayoutParams()
        params!!.height = WindowManager.LayoutParams.WRAP_CONTENT
        params!!.width = WindowManager.LayoutParams.WRAP_CONTENT
        params!!.flags = (WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        params!!.format = PixelFormat.TRANSLUCENT
        params!!.type = WindowManager.LayoutParams.TYPE_TOAST
        params!!.title = "ToastHelper"
        params!!.alpha = 1.0f
        // params.buttonBrightness = 1.0f;
        params!!.packageName = context.packageName
        params!!.windowAnimations = R.style.Animation_Toast
    }

    // textView.setBackgroundColor(Color.BLACK);
    @get:TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private val defaultToastView: View
        private get() {
            val textView = TextView(context)
            textView.text = text
            textView.gravity = Gravity.CENTER_VERTICAL or Gravity.START
            textView.isClickable = false
            textView.isFocusable = false
            textView.isFocusableInTouchMode = false
            textView.setTextColor(Color.WHITE)
            // textView.setBackgroundColor(Color.BLACK);
            val drawable = context!!.resources
                .getDrawable(R.drawable.toast_frame)
            if (Build.VERSION.SDK_INT < 16) {
                textView.setBackgroundDrawable(drawable)
            } else {
                textView.background = drawable
            }
            val wP = getPixFromDip(context, WIDTH_PADDING_IN_DIP)
            val hP = getPixFromDip(context, HEIGHT_PADDING_IN_DIP)
            textView.setPadding(wP, hP, wP, hP)
            return textView
        }

    fun cancel() {
        removeView(true)
    }

    private fun removeView(invokeListener: Boolean) {
        if (toastView != null && toastView!!.parent != null) {
            try {
                Log.i(TAG, "Cancelling Toast...")
                windowManager.removeView(toastView)
                handler!!.removeCallbacks(timer)
            } finally {
                isShowing = false
                if (onDismissListener != null && invokeListener) {
                    onDismissListener!!.onDismiss(this)
                }
            }
        }
    }

    fun show() {
        if (leadingInfinite) {
            throw InfiniteLoopException(
                "Calling show() in OnShowListener leads to infinite loop."
            )
        }
        cancel()
        if (onShowListener != null) {
            leadingInfinite = true
            onShowListener!!.onShow(this)
            leadingInfinite = false
        }
        if (toastView == null) {
            toastView = defaultToastView
        }
        params!!.gravity = GravityCompat
            .getAbsoluteGravity(
                gravity, ViewCompat
                    .getLayoutDirection(toastView!!)
            )
        if (gravity and Gravity.HORIZONTAL_GRAVITY_MASK == Gravity.FILL_HORIZONTAL) {
            params!!.horizontalWeight = 1.0f
        }
        if (gravity and Gravity.VERTICAL_GRAVITY_MASK == Gravity.FILL_VERTICAL) {
            params!!.verticalWeight = 1.0f
        }
        params!!.x = xOffset
        params!!.y = yOffset
        params!!.verticalMargin = verticalMargin.toFloat()
        params!!.horizontalMargin = horizontalMargin.toFloat()
        removeView(false)
        windowManager.addView(toastView, params)
        isShowing = true
        if (handler == null) {
            handler = Handler()
        }
        handler!!.postDelayed(timer, duration)
    }

    fun setText(text: CharSequence) {
        this.text = text
    }

    fun setText(resId: Int) {
        text = context!!.getString(resId)
    }

    fun setGravity(gravity: Int, xOffset: Int, yOffset: Int) {
        this.gravity = gravity
        this.xOffset = xOffset
        this.yOffset = yOffset
    }

    fun setMargin(horizontalMargin: Int, verticalMargin: Int) {
        this.horizontalMargin = horizontalMargin
        this.verticalMargin = verticalMargin
    }

    var view: View?
        get() = toastView
        set(view) {
            removeView(false)
            toastView = view
        }

    fun setOnShowListener(onShowListener: OnShowListener?) {
        this.onShowListener = onShowListener
    }

    fun setOnDismissListener(onDismissListener: OnDismissListener?) {
        this.onDismissListener = onDismissListener
    }

    private class InfiniteLoopException(msg: String) : RuntimeException(msg) {
        companion object {
            private const val serialVersionUID = 6176352792639864360L
        }
    }

    companion object {
        private val TAG = ToastHelper::class.java.name
        private const val WIDTH_PADDING_IN_DIP = 25
        private const val HEIGHT_PADDING_IN_DIP = 15
        private const val DEFAULT_DURATION_MILLIS = 2000L
        private fun getPixFromDip(context: Context?, dip: Int): Int {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip.toFloat(), context!!.resources.displayMetrics
            ).toInt()
        }

        @JvmOverloads
        fun makeText(
            context: Context, text: CharSequence,
            durationMillis: Long = DEFAULT_DURATION_MILLIS
        ): ToastHelper {
            val helper = ToastHelper(context)
            helper.setText(text)
            helper.duration = durationMillis
            return helper
        }

        @JvmOverloads
        fun makeText(
            context: Context, resId: Int,
            durationMillis: Long = DEFAULT_DURATION_MILLIS
        ): ToastHelper {
            val string = context.getString(resId)
            return makeText(context, string, durationMillis)
        }

        fun showToast(context: Context, text: CharSequence) {
            makeText(context, text, DEFAULT_DURATION_MILLIS).show()
        }

        fun showToast(context: Context, resId: Int) {
            makeText(context, resId, DEFAULT_DURATION_MILLIS).show()
        }
    }

    init {
        var mContext = context.applicationContext
        if (mContext == null) {
            mContext = context
        }
        this.context = mContext
        windowManager = mContext
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        init()
    }
}