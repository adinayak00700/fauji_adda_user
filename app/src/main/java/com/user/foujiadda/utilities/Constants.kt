package com.example.datingapp.utilities

object Constants {
    val kDeviceId: String="device_id"
    val kDeviceToken: String="device_token"
    const val USER_TOKEN = "token"
    const val CHANNEL_ID = "channelnew"
    const val FILTER_NOTIFICATION_BROADCAST = "broadcastnew"
    const val SIGNUPAPI = "signup"
    const val getOtheruserProfile = "post_user_profile"
    const val GET_PROFILE = "get_profile"
    const val UPDATE_PROFILE_API = "update_profile"
    const val BANNER_API = "banners"
    const val CATEGORY_API = "service_type"
    const val SUBCATEGORY_API = "subCategory"
    const val PRODUCTS_LIST_API = "products"
    const val JOB_APPLY = "job_apply"
    const val ABOUTUS = "setting"
    const val NEWS_LIST = "get_news"
    const val POST_LIST = "post_list"
    const val JOB_LIST = "job_list"
    const val NOTIFICATION = "notification"
    const val NOTIFICATIONDELETE = "notification_delete"
    const val STATE_LIST = "state"
    const val REGIONAL_LIST = "regional_center"
    const val CITY_LIST = "city"
    const val CITY_LIST_HOSPITAL = "echs_city"
    const val ADD_LEAD_HOSPITAL = "lead_add"
    const val NEWS_CATEGORY = "news_category"
    const val UPDATE_LOCATION = "user_update_location"
    const val CHANGE_PASSWORD = "change_password"
    const val SEND_OTP = "send_otp"
    const val VEHICLE_DETAIL = "vehicle_detail"
    const val BRAND_LIST = "company"
    const val MODEL_LIST = "vehicle_list"
    const val VARIENT_LIST = "variant_list"
    const val DEPOT = "depot"
    const val REGIONAL_CENTER = "regional_center"
    const val JOB_FILTERS = "job_filter"
    const val COMMENTS_LIST = "comment_list"
    const val LIKE_UNLIKE_NEWS_API = "news_like_dislike"
    const val LIKE_UNLIKE_API = "like_dislike"
    const val LIKE_BLOCK_API = "user_block"
    const val LIKE_POST_REPORT_API = "user_post_report"
    const val FOLLOW_UNFOLLOW_USER = "post_user_follow"
    const val COMMENTS_ADD = "comment_add"
    const val VENDOR_LIST = "vendor_list"
    const val USER_RATE_LIST = "user_rating_list"
    const val APP_UPDATE_AVAILABLE = "check_version"
    const val VENDOR_DETAILS = "vendor_detail"
    const val VENDOR_REVIEWS = "vendor_rating_list"
    const val OFFER_LIST = "offer_list"
    const val NEWS_IMAGE = "image"
    const val NEWS_HEADING = "news_heading"
    const val NEWS_DESCRIPTION = "description"

    const val AVTAAR_LIST = "avtaar_list"



    const val MCOLIST = "mco_list"
    const val STATION_LIST = "station_list"
    const val TRAIN_LIST = "train_list"
    const val CHECK_PNR = "check_pnr"


    const val ADD_RATING = "rating_add"
    const val EDIT_RATING = "user_rating_update "
    const val MESSAGE_LOC_API_EMPTY_RESULT = "Pin Location"
    const val MAP_MIN_ZOOM = 1f
    const val MAP_MAX_ZOOM = 16f
    const val MAP_ZOOM_ADDRESS = 16f


}