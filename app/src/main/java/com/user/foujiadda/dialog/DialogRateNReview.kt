package com.user.foujiadda.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R
import com.user.foujiadda.databinding.DialogRateNReviewBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.networking.RestClient
import com.user.foujiadda.utilities.ValidationHelper
import retrofit2.Call
import retrofit2.Response


class DialogRatenReview(private val context: Context, var vendor:Vendor, var callback: Callbackk, var rating:String, var review:String, var isEdit:Boolean) {
    private var dialog: Dialog? = null
    private lateinit var binding: DialogRateNReviewBinding
    var isDialogShowing = false
        private set

    fun show() {
        dialog = Dialog(context, R.style.MyThemeOverlay_Toolbar)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_rate_n_review, null)
        binding= DataBindingUtil.bind(view)!!;
        dialog!!.setContentView(binding.root);
        dialog!!.setCancelable(true)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(null)
            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.setLayout(getWidth(context) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)
            binding.tvTitle.text="Rate & Review"

            dialog!!.window!!.setDimAmount(0.5f)
            //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
        binding.buttonSubmit.setOnClickListener {

            if (/*binding.editText.text.toString().isEmpty() && */ binding.ratingBar.rating.toString() == "0.0"){
                Log.d("bhbcahbca", binding.ratingBar.rating.toString() )
                Toast.makeText(context, "Please Add Your Feedback", Toast.LENGTH_LONG).show()
            }else{

                Log.d("bhbcahbca", binding.ratingBar.rating.toString() )
                if (isEdit){
                    Log.d("bhbcahbca", binding.ratingBar.rating.toString() )
                    ratenReviewEditApi(binding.ratingBar.rating,binding.editText.text.toString() )

                } else {
                    Log.d("bhbcahbca", binding.ratingBar.rating.toString() )
                    ratenReviewApi(binding.ratingBar.rating.toString(),binding.editText.text.toString())

                }


            }

        }
        Glide.with(context).load(vendor.image).circleCrop().into(binding.ivVendorImage)

        binding.tvvendorNae.text=vendor.name


  if (!ValidationHelper.isNull(rating)){
      binding.editText.setText(review)
      binding.ratingBar.rating = rating.toFloat()

  }

      //  ImageLoaderHelperGlide.setGlide(context,binding.ivVendorImage,vendorImage)
        dialog!!.show()
        isDialogShowing = true
    }

    fun dismiss() {
        isDialogShowing = false
        dialog!!.dismiss()
    }

    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }
    }

     interface  Callbackk{
        fun onClickOnAddRateReviewSuccessful()
    }

    private fun ratenReviewApi(rate:String,review:String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
       // hashMap["vendor_id"] = vendor.id
        hashMap["rating"] = rate
        hashMap["review"] = review
        hashMap["type"] = vendor.vendorType
        hashMap["type_id"] = vendor.id
       /* hashMap["rating_id"] = ""*/

        RestClient.getInst().addReview(hashMap).enqueue(object : retrofit2.Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>?, response: Response<ModelSuccess?>) {
                if (response.body()!!.result) {
                    dismiss()
                    Toast.makeText(context,"Feedback Added Successfully",Toast.LENGTH_SHORT).show()
                    callback.onClickOnAddRateReviewSuccessful()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }



        })


    }

    private fun ratenReviewEditApi(rate: Float, review:String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        // hashMap["vendor_id"] = vendor.id
        hashMap["rating"] = rate.toString()
        hashMap["review"] = review
        hashMap["type"] = vendor.vendorType
        hashMap["rating_id"] = vendor.id
        hashMap["type_id"] = ""

        RestClient.getInst().editReview(hashMap).enqueue(object : retrofit2.Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>?, response: Response<ModelSuccess?>) {
                if (response.body()!!.result) {
                    dismiss()
                    Toast.makeText(context,"Feedback Edited Successfully",Toast.LENGTH_SHORT).show()
                    callback.onClickOnAddRateReviewSuccessful()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }



        })


    }
}