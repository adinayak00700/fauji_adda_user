import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import com.user.foujiadda.R
import com.user.foujiadda.databinding.DialogFragmentLoaderBinding


class DialogFragmentLoaderCustom(var fragment: Fragment, var title: String) : DialogFragment() {
    private lateinit var backStateName: String
    private lateinit var binding: DialogFragmentLoaderBinding


    public interface DialogFragmentResultListener {
        fun onGetResult(jsonData: String)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_loader, null)
        val dialog = object : Dialog(requireContext(), R.style.MyCustomDialogTheme) {
            override fun onBackPressed() {

                whenBackPress()

            }
        }

        //   dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.bind(dialogView)!!
        dialog.setContentView(dialogView)

        dialog.setCancelable(true)
        if (dialog.window != null) {

            dialog.window!!.setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.color.colorWhite))
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE));
            dialog.window!!.setLayout(
                    getWidth(
                            requireContext()
                    ) / 100 * 95, getHeight(
                    requireContext()
            ) / 100 * 95
            )
            dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND) // This flag is required to set otherwise the setDimAmount method will not show any effect
            dialog.window!!.setDimAmount(.3f)

            /*    dialog.getWindow()!!.setLayout(
                           LinearLayout.LayoutParams.MATCH_PARENT,
                           LinearLayout.LayoutParams.MATCH_PARENT
                   );*/
        }

        binding.toolbar.tvhead.text = title
        binding.toolbar.topAppBar.navigationIcon = ContextCompat.getDrawable(requireContext(), R.drawable.add)
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            whenBackPress()
        }
        binding.toolbar.topAppBar.menu.getItem(0).isVisible = false
        loadFragment(fragment)

        return dialog
    }


    public fun whenBackPress() {
        Log.d("asdaasdadsas",childFragmentManager.fragments.size.toString())
        for (frag in childFragmentManager.fragments) {
            if (frag.isVisible) {
                val childFm = frag.childFragmentManager
                if (childFm.backStackEntryCount > 1) {
                    childFm.popBackStack()
                } else {
                    dialog!!.dismiss()
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
    }


    public fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val manager: FragmentManager = childFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) { //fragment not in back stack, create it.
            val ft = manager.beginTransaction()
            //    ft.setTransition(Tra));
            ft.add(binding.frame.getId(), fragment, backStateName)
            ft.addToBackStack(backStateName);
            ft.commit()
        }
    }

    override fun onStart() {
        super.onStart()
    }

    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        fun getHeight(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }
    }
}