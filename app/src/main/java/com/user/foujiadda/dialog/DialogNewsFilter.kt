package com.mechanicforyou.user.utilities

import NewsFilterAdapter
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R
import com.user.foujiadda.databinding.DialogNewsFilterBinding
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.NewsFilter
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Response


class DialogNewsFilter(
    private val context: Context,
    var callback: Callbackk,
   var filterList: ArrayList<NewsFilter>
) {
    private var dialog: Dialog? = null
    private lateinit var binding: DialogNewsFilterBinding
    var isDialogShowing = false
        private set

    fun show() {
        dialog = Dialog(context, R.style.MyThemeOverlay_Toolbar)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_news_filter, null)
        binding= DataBindingUtil.bind(view)!!;
        dialog!!.setContentView(binding.root);
        dialog!!.setCancelable(false)
        dialog!!.show()
        isDialogShowing = true



        dialog!!.setCancelable(false)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(null)

            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.setLayout(AvtaarDialog.getWidth(context) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)

            dialog!!.window!!.setDimAmount(0.5f)
            //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }

        binding.closeBtn.setOnClickListener {
            dismiss()
        }

        var newsFilter=NewsFilterAdapter(context,filterList,object :NewsFilterAdapter.Callbackk{
            override fun onCLickONFilter(catId: String) {
                callback.onSelectFilter(catId)
                dismiss()
            }
        })
        binding.rvFilter.adapter=newsFilter
        binding.rvFilter.layoutManager=LinearLayoutManager(context)


    }

    fun dismiss() {
        isDialogShowing = false
        dialog!!.dismiss()
    }



    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }
    }

    public  interface  Callbackk{
        fun onSelectFilter(filter:String)
    }
}