package com.mechanicforyou.user.utilities

import JobFilterAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R

import com.user.foujiadda.databinding.FragmentJobFilterBinding
import com.user.foujiadda.models.*
import com.user.foujiadda.models.spins.ModelCity
import com.user.foujiadda.models.spins.ModelState
import com.user.foujiadda.networking.RestClient
import com.wedguruphotographer.adapter.CustumSpinAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


class FilterDialog(private val context: Context,var callabck:Callbackss) {
    private var dialog: Dialog? = null

    private lateinit var cityAdapter: CustumSpinAdapter
    private lateinit var selectedState: ModelCustumSpinner
    private lateinit var selectedCity: ModelCustumSpinner


    val cityList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()
    val stateList: ArrayList<ModelCustumSpinner> = ArrayList<ModelCustumSpinner>()


    private lateinit var stateAdapter: CustumSpinAdapter
    private lateinit var regionalAdapter: CustumSpinAdapter


    private var param1: String? = null
    private var param2: String? = null
    private var postId = ""

    var education : String = ""
    var salary : String = ""
    var states : String = ""
    var citys : String = ""

   
    
    private lateinit var binding: FragmentJobFilterBinding
    var isDialogShowing = false
        private set

    fun show() {
        dialog = Dialog(context, R.style.MyCustomDialogTheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_job_filter, null)
        binding= DataBindingUtil.bind(view)!!;
        dialog!!.setContentView(binding.root);
        dialog!!.setCancelable(true)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(null)

            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.setLayout(getWidth(context) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)

            dialog!!.window!!.setDimAmount(0.5f)
            //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }


        getAllStateList()
        getFilters()

        var state = ModelCustumSpinner(id = "0", name = "Select State")
        stateList.add(state)
        stateAdapter = CustumSpinAdapter(
            context!!,
            android.R.layout.simple_spinner_item,
            stateList, false
        )
        binding.spinnerState.setAdapter(stateAdapter)


        var city = ModelCustumSpinner(id = "0", name = "Select City")
        cityList.add(city)
        cityAdapter = CustumSpinAdapter(
            context!!,
            android.R.layout.simple_spinner_item,
            cityList, false
        )
        binding.spinnerCity.setAdapter(cityAdapter)


        binding.buttonApply.setOnClickListener {
            callabck.onCLickONApplyButton(states, citys, education , salary)
        dismiss()

    }

        binding.buttonReset.setOnClickListener {
            dismiss()
        }
        //  ImageLoaderHelperGlide.setGlide(context,binding.ivVendorImage,vendorImage)
        dialog!!.show()
        isDialogShowing = true
    }

    fun dismiss() {
        isDialogShowing = false
        dialog!!.dismiss()
    }


public  interface  Callbackss {
    fun onCLickONApplyButton(selecteState: String, selecteCity: String, education : String, salary : String )
}
    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }
    }

    private fun ratenReviewApi(rate:String,review:String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        // hashMap["vendor_id"] = vendor.id
        hashMap["rating"] = rate
        hashMap["review"] = review


        RestClient.getInst().addReview(hashMap).enqueue(object : retrofit2.Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>?, response: Response<ModelSuccess?>) {
                if (response.body()!!.result) {
                    dismiss()
                    Toast.makeText(context,"Feedback Added Successfully",Toast.LENGTH_SHORT).show()
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }



        })


    }


    fun getAllStateList() {
        val hashMap = java.util.HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(context!!)!!.userDetail.token
        RestClient.getInst().getState(hashMap).enqueue(object : Callback<ModelState?> {
            override fun onResponse(
                call: Call<ModelState?>,
                response: Response<ModelState?>
            ) {
                if (response.body() != null) {

                    for (i in 0 until response.body()!!.data.size) {
                        var model = ModelCustumSpinner(
                            id = response.body()!!.data[i].id,
                            name = response.body()!!.data[i].name
                        )
                        stateList.add(model)
                    }





                    binding.spinnerState.setOnItemSelectedListener(object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            selectedState =
                                parent!!.getItemAtPosition(position) as ModelCustumSpinner

                            states = selectedState.id
                            getAllCityList()
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    })
                }
            }

            override fun onFailure(call: Call<ModelState?>, t: Throwable) {

            }
        })
    }

    fun getAllCityList() {
        val hashMap = java.util.HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(context!!)!!.userDetail.token

        hashMap["state_id"] = selectedState.id

        RestClient.getInst().getCity(hashMap).enqueue(object : Callback<ModelCity?> {
            override fun onResponse(
                call: Call<ModelCity?>,
                response: Response<ModelCity?>
            ) {
                if (response.body() != null) {
                    if (response.body()!!.result) {


                        for (i in 0 until response.body()!!.data.size) {
                            var model = ModelCustumSpinner(
                                id = response.body()!!.data[i].id,
                                name = response.body()!!.data[i].name
                            )
                            cityList.add(model)
                        }
                        cityAdapter.notifyDataSetChanged()



                        binding.spinnerCity.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                selectedCity =
                                    parent!!.getItemAtPosition(position) as ModelCustumSpinner

                                citys = selectedCity.id

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                    }
                }
            }

            override fun onFailure(call: Call<ModelCity?>, t: Throwable) {

            }
        })
    }

    fun getFilters() {
        val hashMap = java.util.HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =
            PrefManager.getInstance(context!!)!!.userDetail.token




        RestClient.getInst().getJobFilters(hashMap).enqueue(object : Callback<ModelJobFilter?> {
            override fun onResponse(
                call: Call<ModelJobFilter?>,
                response: Response<ModelJobFilter?>
            ) {
                if (response.body() != null) {
                    var educationFilterList = ArrayList<JobFilterData>()
                    var salaryFilterList = ArrayList<JobFilterData>()
                    for (i in response.body()!!.data.education) {
                        educationFilterList.add(i)
                    }

                    var educationFilterAdapater = JobFilterAdapter(
                        context!!,
                        educationFilterList,
                        "education",
                        object : JobFilterAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {
                               education = catId
                            }
                        })

                    binding.rvEducation.adapter = educationFilterAdapater
                    binding.rvEducation.layoutManager =
                        LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)


                    for (i in response.body()!!.data.salary) {
                        salaryFilterList.add(i)
                    }

                    var salaryFilterAdapter = JobFilterAdapter(
                        context!!,
                        salaryFilterList,
                        "salary",
                        object : JobFilterAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {
                              salary = catId
                            }
                        })

                    binding.rvSalary.adapter = salaryFilterAdapter
                    binding.rvSalary.layoutManager =
                        LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)


                }
            }

            override fun onFailure(call: Call<ModelJobFilter?>, t: Throwable) {

            }
        })
    }





}