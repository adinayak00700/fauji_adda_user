package com.mechanicforyou.user.utilities

import AvtaarAdapter
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.GridLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager

import com.user.foujiadda.R

import com.user.foujiadda.databinding.DialogAvtaarBinding
import com.user.foujiadda.models.Avtaaar
import com.user.foujiadda.models.ModelAvtar
import com.user.foujiadda.models.ModelSuccess
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.networking.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AvtaarDialog(private val context: Context,var callback:Callbackk ) {
    private var dialog: Dialog? = null
    private lateinit var binding: DialogAvtaarBinding
    var isDialogShowing = false
        private set


    public interface Callbackk{
        fun onAvataarChanged();
    }

    fun show() {
        dialog = Dialog(context, R.style.MyThemeOverlay_Toolbar)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_avtaar, null)
        binding= DataBindingUtil.bind(view)!!;
        dialog!!.setContentView(binding.root);
        dialog!!.setCancelable(true)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(null)

            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.setLayout(getWidth(context) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)
            binding!!.tvTitle.text="Select Avtaar"

            dialog!!.window!!.setDimAmount(0.5f)
            //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        getAvtaars()
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
        binding.buttonSubmit.setOnClickListener {
         updateAvtaar("")
            
        }

        dialog!!.show()
        isDialogShowing = true
    }

    fun dismiss() {
        isDialogShowing = false
        dialog!!.dismiss()
    }



    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }
    }

    private fun getAvtaars() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token


        RestClient.getInst().getAvtaars(hashMap).enqueue(object : retrofit2.Callback<ModelAvtar?> {
            override fun onResponse(call: Call<ModelAvtar?>?, response: Response<ModelAvtar?>) {
                if (response.body()!!.result) {

                  var avatarAadpter=  AvtaarAdapter(context,object :AvtaarAdapter.Callbackk{
                      override fun onClickOnAvtaar(avtaar: Avtaaar) {
                          updateAvtaar(avtaar.id)
                      }
                  })

                    avatarAadpter.submitList(response.body()!!.data)


                    binding.recyclerView3.adapter=avatarAadpter
                    binding.recyclerView3.layoutManager=GridLayoutManager(context,3)

                } else {

                }
            }

            override fun onFailure(call: Call<ModelAvtar?>?, t: Throwable) {

            }



        })


    }


    fun updateAvtaar(id:String) {

        RestClient.getInst().etProfileApi(
            PrefManager.getInstance(context)!!.userDetail.token,id,
            object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                       callback.onAvataarChanged()
                        dismiss()
                    }
                }
                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                }
            })
    }
}