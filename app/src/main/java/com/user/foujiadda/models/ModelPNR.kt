package com.user.foujiadda.models

data class ModelPNR(
    val `data`: PNRData,
    val message: String,
    val result: Boolean
)
data class PNRData(
    val arrival_data: ArrivalData,
    val boarding_station: String,
    val chart_status: String,
    val `class`: String,
    val departure_data: DepartureData,
    val passenger: List<Passenger>,
    val quota: String,
    val reservation_upto: String,
    val train_name: String,
    val train_number: String
)

data class ArrivalData(
    val arrival_date: String,
    val arrival_time: String
)

data class DepartureData(
    val departure_date: String,
    val departure_time: String
)

data class Passenger(
    val booking_status: String,
    val current_status: String,
    val name: String
)