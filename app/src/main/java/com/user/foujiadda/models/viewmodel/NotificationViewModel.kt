package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.NotificationData
import com.user.foujiadda.models.Vendor

class NotificationViewModel : ViewModel() {

    var livedatalist = MutableLiveData<ArrayList<NotificationData>>()
    var list = arrayListOf<NotificationData>()
    fun add(feed: ArrayList<NotificationData>){
        list.clear()
        list.addAll(feed)
        livedatalist.value=list
    }

    fun update(feed: NotificationData, position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}