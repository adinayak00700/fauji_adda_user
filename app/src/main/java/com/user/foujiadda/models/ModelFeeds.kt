package com.user.foujiadda.models

data class ModelFeeds(
    val `data`: List<Feed>,
    val message: String,
    val result: Boolean
)

data class Feed(
    val commentCount: String,
    val created_at: String,
    val description: String,
    val id: String,
    val images: List<FeedImaged>,
    val is_deleted: String,
    var already_follow: String,
    val status: String,
    var userProfile:String,
    val title: String,
    val updated_at: String,
    val user_id: String,
    var like:Int,
    var likeCount: String,
    var own_post: String,
    var user_name:String
)

data class FeedImaged(
    val created_at: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val post_id: String,
    val status: String,
    val updated_at: String
)