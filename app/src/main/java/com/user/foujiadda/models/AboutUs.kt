package com.user.foujiadda.models

import java.io.Serializable

data class AboutUs(
    val `data`: AboutData,
    val message: String,
    val result: String
)

data class AboutData(
    val id: String,
    val logo: String,
    val email: String,
    val term_condition: String,
    val about_us: String,

):Serializable