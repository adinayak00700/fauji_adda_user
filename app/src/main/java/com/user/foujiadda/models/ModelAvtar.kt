package com.user.foujiadda.models

data class ModelAvtar(
    val `data`: List<Avtaaar>,
    val message: String,
    val result: Boolean
)

data class Avtaaar(
    val created_at: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val name: String,
    val status: String,
    val updated_at: String
)