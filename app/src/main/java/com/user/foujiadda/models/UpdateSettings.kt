package com.user.foujiadda.models

data class UpdateSettings(
    val `data`: String,
    val message: String,
    val result: Boolean
)
