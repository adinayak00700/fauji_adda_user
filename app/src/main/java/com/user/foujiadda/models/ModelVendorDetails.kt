package com.user.foujiadda.models

data class ModelVendorDetails(
    val `data`: Vendor,
    val message: String,
    val result: Boolean
)

