package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.JobData
import com.user.foujiadda.models.ModelJob
import com.user.foujiadda.models.News

class JobViewModel: ViewModel() {
    var livedatalist = MutableLiveData<ArrayList<JobData>>()
    var list = arrayListOf<JobData>()
    fun add(feed: ArrayList<JobData>){

        list.addAll(feed)
        livedatalist.value=list
    }
    fun update(feed: JobData,position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}