package com.user.foujiadda.models.spins

import com.user.foujiadda.models.BannerData


class ModelModel (val `data`: List<VehicleModel>,
                 val message: String,
                 val result: Boolean)

data class VehicleModel(
    val name: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val status: String,
    val updated_at: String
)
