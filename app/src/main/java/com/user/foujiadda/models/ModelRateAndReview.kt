package com.user.foujiadda.models

data class ModelRateAndReview(
    val `data`: List<Vendor>,
    val message: String,
    val result: Boolean
)
