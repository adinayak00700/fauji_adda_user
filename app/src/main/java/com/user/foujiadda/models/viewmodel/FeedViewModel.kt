package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.News

class FeedViewModel: ViewModel() {
    var livedatalist = MutableLiveData<ArrayList<Feed>>()
    var list = arrayListOf<Feed>()
    fun add(feed: ArrayList<Feed>){
        list.clear()
        list.addAll(feed)
        livedatalist.value=list
    }
    fun update(feed: Feed,position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}