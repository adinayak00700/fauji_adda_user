package com.user.foujiadda.models

import user.faujiadda.models.SubCategory
import java.io.Serializable

data class ModelSelectedJobFilter(
    var state: String,
    var city: String,
    var education: String,
    var salary: String,


    ): Serializable