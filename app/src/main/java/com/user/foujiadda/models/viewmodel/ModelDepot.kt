package com.user.foujiadda.models.viewmodel

data class ModelDepot(
    val `data`: List<Depot>,
    val message: String,
    val result: Boolean
)

data class Depot(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val name: String,
    val status: String,
    val updated_at: String
)