package com.user.foujiadda.models

data class ModelProducts(
    val `data`: List<Product>,
    val message: String,
    val result: Boolean
)

data class Product(
    val category_id: String,
    val cost_price: String,
    val created_at: String,
    val description: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val mrp: String,
    val name: String,
    val status: String,
    val unit: String,
    val unit_value: String,
    val updated_at: String
)