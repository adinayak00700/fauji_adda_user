package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Vendor

class ViewModelRateAndReview : ViewModel()
{
    var livedatalist = MutableLiveData<ArrayList<Vendor>>()
    var list = arrayListOf<Vendor>()
    fun add(feed: ArrayList<Vendor>){
        list.clear()
        list.addAll(feed)
        livedatalist.value=list
    }

    fun update(feed: Vendor, position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }

}