package com.user.foujiadda.models

data class ModelRegionalData(
    val `data`: List<RegionalData>,
    val message: String,
    val result: Boolean
)

data class RegionalData(
    val city_id: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val name: String,
    val state_id: String,
    val status: String,
    val type: String,
    val updated_at: String
)