package com.user.foujiadda.models

data class ModelTrain(
    val `data`: List<Train>,
    val message: String,
    val result: Boolean
)

data class Train(
    val created_at: String,
    val destination: String,
    val id: String,
    val is_deleted: String,
    val mcoContact: String,
    val mcoName: String,
    val mco_id: String,
    val remark: String,
    val seat_available: SeatAvailable,
    val stationName: String,
    val station_id: String,
    val status: String,
    val train_name: String,
    val train_no: String,
    val updated_at: String
)

data class SeatAvailable(
    val AC1: String,
    val AC2: String,
    val AC3: String,
    val CC: String,
    val EC: String,
    val FC: String,
    val SL: String,
    val f2S: String
)