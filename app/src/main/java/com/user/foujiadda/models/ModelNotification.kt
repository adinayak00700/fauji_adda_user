package com.user.foujiadda.models

data class ModelNotification(
    val `data`: List<NotificationData>,
    val message: String,
    val result: Boolean
)

data class NotificationData(
    val created_at: String,
    val id: String,
    val image: Any,
    val is_deleted: String,
    val link: String,
    val message: String,
    val service: String,
    val status: String,
    val title: String,
    val updated_at: String,
    val user_id: String
)