package com.user.foujiadda.models

data class ModelUser(
    val `data`: UserDetails,
    val message: String,
    val result: Boolean
)

data class UserDetails(
    val email: String,
    val image: String,
    val mobile: String,
    val about:String,
    val city:String,
    val token: String,
    val first_name:String,
    val last_name:String,
    val avtaar:String,
    val dob:String,
    val img_permission:String,
    val state_id : String,
    val city_id : String,
    val privacy:String,
    val notification:String

)