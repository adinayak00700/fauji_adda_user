package com.user.foujiadda.models

data class ModelJobFilter(
    val `data`: JobFilters,
    val message: String,
    val result: Boolean
)

data class JobFilters(
    val education: List<JobFilterData>,
    val salary: List<JobFilterData>
)

data class JobFilterData(
    val created_at: String,
    val id: String,
    val amount: String,
    val name: String,
    val status: String,
    val updated_at: String
)

