package user.faujiadda.models

data class ModelSubcategory(
    val `data`: List<SubCategory>,
    val message: String,
    val result: Boolean
)

data class SubCategory(
    val created_at: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val name: String,
    val parent: String,
    val status: String,
    val updated_at: String
)