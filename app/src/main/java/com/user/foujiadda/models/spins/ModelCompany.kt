package com.user.foujiadda.models.spins

import com.user.foujiadda.models.BannerData


class ModelCompany (val `data`: List<Company>,
                 val message: String,
                 val result: Boolean)

data class Company(
    val name: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val status: String,
    val type: String,
)
