package com.user.foujiadda.models

data class ModelMCQ(
    val `data`: List<MCO>,
    val message: String,
    val result: Boolean
)

data class MCO(
    val contact_no: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val name: String,
    val status: String,
    val updated_at: String
)