package com.user.foujiadda.models

data class ModelOtheruserProfile(
    val `data`: OtherUserProfile,
    val message: String,
    val result: Boolean
)

data class OtherUserProfile(
    val about: String,
    val city: String,
    val followers: String,
    val following: String,
    val image: String,
    val is_follow: String,
    val name: String,
    val privacy : String,
    val email : String,
    val dob : String,
    val state : String,
    val mobile : String
)