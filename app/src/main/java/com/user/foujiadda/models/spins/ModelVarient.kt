package com.user.foujiadda.models.spins

data class ModelVarient(
    val `data`: List<VehicleVarient>,
    val message: String,
    val result: Boolean
)

data class VehicleVarient(
    val company_id: String,
    val created_at: String,
    val id: String,
    val name: String,
    val status: String,
    val type: String,
    val v_id: String
)