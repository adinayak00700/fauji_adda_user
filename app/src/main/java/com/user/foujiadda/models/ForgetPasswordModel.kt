package com.user.foujiadda.models

data class ForgetPasswordModel(val `data`: String,
                          val message: String,
                          val result: Boolean)