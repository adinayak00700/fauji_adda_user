package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.News

class NewsViewModel:ViewModel() {

    var livedatalist = MutableLiveData<ArrayList<News>>()
    var list = arrayListOf<News>()

    fun add(categories: ArrayList<News>){
        list.addAll(categories)
        livedatalist.value=list
    }

    fun filterAdd(categories: ArrayList<News>){
        list.clear()
        list.addAll(categories)
        livedatalist.value=list
    }

    fun clear(){
        list.clear()
        livedatalist.value=list
    }

    fun update(news: News, position:Int){
        list.set(position,news)
        livedatalist.value=list
    }
}