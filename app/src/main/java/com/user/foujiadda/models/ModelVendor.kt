package com.user.foujiadda.models

data class ModelVendor(
    val `data`: List<Vendor>,
    val message: String,
    var total_pages: String,
    val result: Boolean
)

data class Vendor(
    val address: String,
    val cityName: String,
    val city_id: String,
    val created_at: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val lat: String,
    val lng: String,
    val mobile: String,
    val vendorType: String,
    val name: String,
    val regional_center_id: String,
    val state_id: String,
    val status: String,
    val rooms: Rooms,
    val total_rating: String,
    val avg_rating: Double,
    val updated_at: String,
    val distance: String,
    val rating: String,
    val review: String,
    val google_map:String,
    val facility:String,


    val type: String,
    val type_id: String,

    val user_id: String
)









data class Rooms(
    val dormitory: String,
    val jcos: String,
    val officers: String,
    val ors: String
)













