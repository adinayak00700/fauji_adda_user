package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.News
import com.user.foujiadda.models.Vendor
import com.user.foujiadda.models.VendorReview

class VendorReviewViewModel: ViewModel() {
    var livedatalist = MutableLiveData<ArrayList<VendorReview>>()
    var list = arrayListOf<VendorReview>()
    fun add(feed: ArrayList<VendorReview>){
        list.clear()
        list.addAll(feed)
        livedatalist.value=list
    }

    fun update(feed: VendorReview,position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}