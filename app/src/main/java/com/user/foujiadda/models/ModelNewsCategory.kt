package com.user.foujiadda.models

data class ModelNewsFilter(
    val `data`: List<NewsFilter>,
    val message: String,
    val result: Boolean
)

data class NewsFilter(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val name: String,
    val status: String,
    val updated_at: String
)