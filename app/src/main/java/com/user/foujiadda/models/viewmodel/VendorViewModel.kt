package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Feed
import com.user.foujiadda.models.News
import com.user.foujiadda.models.Vendor

class VendorViewModel: ViewModel() {
    var livedatalist = MutableLiveData<ArrayList<Vendor>>()
    var list = arrayListOf<Vendor>()
    fun add(feed: ArrayList<Vendor>){

        list.addAll(feed)
        livedatalist.value=list
    }

    fun update(feed: Vendor,position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}