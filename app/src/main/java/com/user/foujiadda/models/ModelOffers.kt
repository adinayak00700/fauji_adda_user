package com.user.foujiadda.models

data class ModelOffers(
    val `data`: List<Offer>,
    val message: String,
    val result: Boolean
)

data class Offer(
    val activate_offer: String,
    val coupon_code: String,
    val created_at: String,
    val description: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val status: String,
    val title: String,
    val updated_at: String,
    val validity: String
)