package com.user.foujiadda.models

import user.faujiadda.models.SubCategory
import java.io.Serializable

data class ModelSelectedFilter(
    var state: String?,
    var city: String?,
    var vehicleType: String?,
    var company_id: String?,
    var type:String?,
    var filterType:String?,
    var vehicle_id: String?,
    var depot_id: String?,
    var appointmentDate: String?,

    ): Serializable