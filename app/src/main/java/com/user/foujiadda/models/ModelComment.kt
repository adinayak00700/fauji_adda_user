package com.user.foujiadda.models

data class ModelComment(
    val `data`: List<Comment>,
    val message: String,
    val result: Boolean
)

data class Comment(
    val comment: String,
    val created_at: String,
    val id: String,
    val image: String,
    val user_name:String,
    val is_deleted: String,
    val post_id: String,
    val status: String,
    val updated_at: String,
    val user_id: String
)