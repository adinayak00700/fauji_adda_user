package com.user.foujiadda.models.spins

data class ModelVehicle(
    val `data`: List<VehicleData>,
    val message: String,
    val result: Boolean
)

data class VehicleData(
    val city_id: String,
    val company_id: String,
    val created_at: String,
    val depot_id: String,
    val id: String,
    val image: String,
    val index_number: String,
    val is_deleted: String,
    val model: String,
    val price: String,
    val state_id: String,
    val status: String,
    val type: String,
    val updated_at: String,
    val vehicle_name: String
)