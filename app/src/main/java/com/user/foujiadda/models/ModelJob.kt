package com.user.foujiadda.models

import java.io.Serializable

data class ModelJob(
    val data: List<JobData>,
    val message: String,
    var total_pages: String,
    val result: Boolean
)

data class JobData(
    val age: String,
    val cityName: String,
    val city_id: String,
    val created_at: String,
    val description: String,
    val eligibilityValue:String,
    val id: String,
    val is_deleted: String,
    val location: String,
    val mobile: String,
    val mode: String,
    val posts: String,
    val qualification: String,
    val job_link:String,
    val salary: String,
    val salary_type: String,
    val stateName: String,
    val state_id: String,
    val status: String,
    val time_type: String,
    val start_date:String,
    val end_date:String,
    val title: String,
    val type: String,
    val updated_at: String,
    val image: String,
    val is_apply: String
):Serializable