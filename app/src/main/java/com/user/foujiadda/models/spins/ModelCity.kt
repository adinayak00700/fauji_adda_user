package com.user.foujiadda.models.spins

import com.user.foujiadda.models.BannerData

class ModelCity (val `data`: List<City>,
                 val message: String,
                 val result: Boolean)


data class City(

    val created_at: String,
    val id: String,
    val name: String,
    val is_deleted: String,
    val updated_at: String
)
