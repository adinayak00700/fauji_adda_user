package com.user.foujiadda.models.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.user.foujiadda.models.Comment


class CommentViewModel: ViewModel() {
    var livedatalist = MutableLiveData<ArrayList<Comment>>()
    var list = arrayListOf<Comment>()
    fun add(feed: ArrayList<Comment>){
        list.addAll(feed)
        livedatalist.value=list
    }
    fun clear(){
        list.clear()
        livedatalist.value=list
    }
    fun update(feed: Comment, position:Int){
        list.set(position,feed)
        livedatalist.value=list
    }
}