package com.user.foujiadda.models

data class ModelCategory(
    val `data`: List<Category>,
    val message: String,
    val result: Boolean
)

data class Category(
    val created_at: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val name: String,
    val parent: String,
    val status: String,
    val updated_at: String
)

