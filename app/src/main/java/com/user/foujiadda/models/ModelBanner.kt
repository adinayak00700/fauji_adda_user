package com.user.foujiadda.models

data class ModelBanner(
    val `data`: List<BannerData>,
    val message: String,
    val result: Boolean
)

data class BannerData(
    val banner_img: String,
    val banner_name: String,
    val business_id: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val status: String,
    val type: String,
    val type_id: String,
    val updated_at: String,
    val service: String,
    val link : String

)



