package com.user.foujiadda.models

data class ModelNews(
    val data: List<News>,
    val message: String,
    val result: Boolean,
    var total_pages: String,
)

data class News(
    val category_id: String,
    val created_at: String,
    val description: String,
    val id: String,
    val image: String,
    val is_deleted: String,
    val status: String,
    val title: String,
    val updated_at: String,
    var is_like:Int,
    var likeCount: String,
    var own_post: String





)