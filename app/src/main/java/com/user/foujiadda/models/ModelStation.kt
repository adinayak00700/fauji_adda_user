package com.user.foujiadda.models

data class ModelStation(
    val `data`: List<Station>,
    val message: String,
    val result: Boolean
)

data class Station(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val mco_id: String,
    val name: String,
    val status: String,
    val updated_at: String
)