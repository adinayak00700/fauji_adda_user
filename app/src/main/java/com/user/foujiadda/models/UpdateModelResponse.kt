package com.user.foujiadda.models

data class UpdateModelResponse(
    val data: Data,
    val message: String,
    val result: Boolean
)

data class Data(
    val android_cur_int: String,
    val android_man_int: String,
    val id: String
)