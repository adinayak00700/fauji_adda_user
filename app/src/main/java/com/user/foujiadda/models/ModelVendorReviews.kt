package com.user.foujiadda.models





data class ModelVendorReviews(
    val `data`: List<VendorReview>,
    val message: String,
    val result: Boolean,
    val userRating: UserRating
)

data class VendorReview(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val rating: String,
    val review: String,
    val status: String,
    val type: String,
    val type_id: String,
    val updated_at: String,
    val userName: String,
    val user_id: String,
    val user_image: String
)

data class UserRating(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val rating: String,
    val review: String,
    val status: String,
    val type: String,
    val type_id: String,
    val updated_at: String,
    val user_id: String
)




