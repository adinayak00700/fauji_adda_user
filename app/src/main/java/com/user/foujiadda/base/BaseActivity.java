package com.user.foujiadda.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.example.mechanicforyoubusiness.utilities.PrefManager;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.google.android.material.snackbar.Snackbar;
import com.user.foujiadda.R;
import com.user.foujiadda.callbacks.BaseCallBacks;
import com.user.foujiadda.utilities.IntentHelper;

import java.util.ArrayList;
import java.util.List;



public abstract class BaseActivity extends AppCompatActivity
        implements View.OnClickListener, BaseCallBacks
{

    public static final int READ_WRITE_STORAGE = 52;
    public static final int MULTIPLE_PERMISSIONS = 10;
    boolean isGPSAvailable = false;
    boolean isInternetAvailable = false;
    private com.user.foujiadda.dialog.ProgressDialog progressDialog;
    private ProgressDialog mProgressDialog;
    private static List<AlertDialog> sessionExpireDialogList = new ArrayList<>();
    private static List<AlertDialog> errorDialogList = new ArrayList<>();

    private boolean isInBAckground = false;
    private Context context;
    public abstract void onClick(int viewId, View view);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new com.user.foujiadda.dialog.ProgressDialog(this);
        context = this;
    }




    public void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void makeLongToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showLogoutDialog(final Activity activity){
        if(activity==null || activity.isFinishing())return;

        new iOSDialogBuilder(activity)
                .setTitle(activity.getString(R.string.app_name))
                .setSubtitle(getString(R.string.txt_are_you_sure))
                .setBoldPositiveLabel(false)
                .setCancelable(false)
                .setPositiveListener(context.getResources().getString(R.string.yes_text), new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        dialog.dismiss();
                        PrefManager.Companion.getInstance(activity).clearPrefs();
                        PrefManager.Companion.getInstance(activity).setKeyIsLoggedIn(false);
                        finishAffinity();
                        startActivity(IntentHelper.getLoginScreen(activity));
                    }
                }).setNegativeListener(context.getResources().getString(R.string.no_text), new iOSDialogClickListener() {
            @Override
            public void onClick(iOSDialog dialog) {
                dialog.dismiss();
            }
        })
                .build().show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // case R.id.toolbar_back: onBackPressed(); break;
            default:
                onClick(v.getId(), v);
                break;
        }
    }

    @Override
    public void showLoader() {
        Log.d("asdsadsd","asdsadsd");
        try {
            if (!isFinishing() && !progressDialog.isShowing()) progressDialog.showDialog(com.user.foujiadda.dialog.ProgressDialog.DIALOG_FULLSCREEN);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void dismissLoader() {
        if (!isFinishing() && progressDialog.isShowing()) progressDialog.dismiss();
    }

    protected void showLoading(@NonNull String message) {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    protected void showSnackbar(@NonNull String message) {
        View view = findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onFragmentDetach(String fragmentTag) {
    }



    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{permission}, READ_WRITE_STORAGE);
        }
        return isGranted;
    }


    public boolean isPermissionGranted(String[] permissions) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE_STORAGE:
                isPermissionGranted(grantResults[0] == PackageManager.PERMISSION_GRANTED, permissions[0]);
                break;
        }
    }

    public void isPermissionGranted(boolean isGranted, String permission) {

    }




}

